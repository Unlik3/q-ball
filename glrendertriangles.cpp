#include "glrendertriangles.h"

GLRenderTriangles::GLRenderTriangles(QObject *parent) :
    QObject(parent)
{
m_count=0;
 m_program=nullptr;
m_firstFlush=true;
}
void GLRenderTriangles::Create(int width,int height,float32 scale,float32 traslx)
    {
  //  QOpenGLBuffer m_vertex;
  m_program = new QOpenGLShaderProgram();
  m_program->addShaderFromSourceCode(QOpenGLShader::Vertex,
                                     "#version 300 es\n"
                                     " in vec2 vtxcoord;\n"
                                     " in vec4 v_color;\n"
                                     "uniform mat4 projectionMatrix;\n"
                                     "uniform vec3 param;\n"
                                     "out vec4 f_color;\n"
                                     "void main() {\n"
                                     "   // colorIn=vec4(dominate,1.0f);\n"
                                     "	f_color = v_color;\n"
                                     "    gl_Position = projectionMatrix*vec4(param.x*vtxcoord.x,param.z+param.y-param.x*vtxcoord.y,-0.4f, 1.0f);\n"

                                     "}\n"                                             );
  m_program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                     "#version 300 es\n"
                                     "precision mediump float;\n"
                                     "out vec4 my_fragcolor;\n"
                                     "in vec4 f_color;\n"
                                     "\n"
                                     "void main() {\n"
                                     "\n"
                                     "my_fragcolor=f_color;\n"

                                     "}\n"
                                     );
  m_program->bindAttributeLocation("vtxcoord",0);
  m_program->bindAttributeLocation("v_color",1);

  m_program->link();


  // Create Buffer (Do not release until VAO is created)
//   m_program->bind();
 //glBindAttribLocation(m_program->programId(), 0, "vtxcoord");
 // glBindAttribLocation(m_program->programId(), 1, "v_color");
//   m_program->release();
  m_vertexBuffer.create();
  if(m_vertexBuffer.bind())
  {
  m_vertexBuffer.setUsagePattern(QOpenGLBuffer::DynamicDraw);
  m_vertexBuffer.allocate(m_vertices, sizeof(m_vertices));
  m_vertexBuffer.release();
   }
  m_colorBuffer.create();
  if(m_colorBuffer.bind())
  {
   m_colorBuffer.setUsagePattern(QOpenGLBuffer::DynamicDraw);
   m_colorBuffer.allocate(m_colors, sizeof(m_colors));
   m_colorBuffer.release();
   qDebug()<< "m_colorBuffer OK"<< "size:"<<sizeof(m_colors);
}

  m_projection_matrix.setToIdentity();


  m_param.setX(scale);
  m_param.setY(height);
  m_param.setZ(0);

  m_projection_matrix.ortho(0, width, height, 0.0f, -1.0f, 1.0f);
  m_projection_matrix.translate(traslx/scale+width/2.0f,0,0.0);

  m_count = 0;


  qDebug() << "Program1 link result:" << m_program->log();
}

void GLRenderTriangles::Vertex(const b2Vec2& v, const b2Color& c)
{

    if (m_count == e_maxVertices)
    {
        qDebug() << "### GLRenderTriangles e_maxVertices###"<<m_count;
        return;
        //qDebug() << "@@@";
         //   Flush();
    }

        m_vertices[m_count] =v;


        m_colors[m_count] = c;
        ++m_count;
}
void GLRenderTriangles::Flush(float32 offsetY)
{

    if (m_count == 0)
        return;
    if (m_firstFlush)
    {
        qDebug() << "GLRenderTriangles::Tot vertex:"<<m_count;
        m_firstFlush=false;
    }
    if ( m_program->bind())
    {



       if (m_vertexBuffer.bind())
        {

        m_program->enableAttributeArray("vtxcoord");
        m_program->setAttributeBuffer(0, GL_FLOAT, 0,2);
        m_vertexBuffer.write(0,m_vertices, m_count * sizeof(b2Vec2));
        m_vertexBuffer.release();
        }

        if(m_colorBuffer.bind())
        {

        m_program->enableAttributeArray("v_color");
        m_program->setAttributeBuffer(1, GL_FLOAT, 0,4);
        m_colorBuffer.write(0,m_colors, m_count * sizeof(b2Color));


        m_colorBuffer.release();

        }



        m_program->setUniformValue("projectionMatrix", m_projection_matrix);

        m_param.setZ(offsetY);
        m_program->setUniformValue("param", m_param);


       glDrawArrays(GL_TRIANGLES, 0, m_count);




        m_program->release();
    }
    m_count = 0;
}
GLRenderTriangles::~GLRenderTriangles()
{
    if (m_program!=nullptr)
    {
    qDebug()<<"Destroy triangle";

m_vertexBuffer.destroy();
m_colorBuffer.destroy();
delete m_program;
    }
}

