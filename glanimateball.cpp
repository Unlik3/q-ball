#include "glanimateball.h"

GLAnimateBall::GLAnimateBall(QObject *parent) : QObject(parent)
{
 m_program=nullptr;
    m_status=normal;
}
void GLAnimateBall::Create(const float32 radiusWorld,const int width,const int height,const QUrl dataUrl,float32 scale,float32 traslx)
{

    m_rateWorldScreen=scale;
    qDebug() << "create animate ball";
    QImage image;
    image=QImage(512,512,QImage::Format_RGB32);
    // QUrl url;
    // url= SailfishApp::pathTo("data");
    if (image.load(dataUrl.path().append("/images/").append("fireball.png")))
        qDebug() << "Image OK";

    m_status=normal;

    fireOff=false;
    m_currentFrame=0;
    m_TotFrame=26;
    m_heightScreen=height;
    GLfloat radius=2*m_rateWorldScreen*radiusWorld;



    const GLfloat g_vertex_buffer_data[] = {
        // X        Y         S     T
        -radius*0.5f, radius,   0.0f, 1.0f,
        radius*0.5f,    radius,    1.0f, 1.0f,
        radius*0.5f,    -radius, 1.0f,0.0f,
        -radius*0.5f ,-radius,0.0f,0.0f,

    };


    /*
        if  (window->openglContext()->hasExtension(QByteArrayLiteral("GL_OES_vertex_array_object")))
            qDebug() << "VAO presente!!!!";
        else
             qFatal( "GL_OES_vertex_array_object is not supported" );

    */

    m_program = new QOpenGLShaderProgram();
    //  printVersionInformation();
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex,
                                       "#version 300 es\n"
                                       "in vec4 vtxcoord;\n"
                                       "out vec2 texCoord;\n"
                                       "out vec4  framecalcFS;\n"
                                       "uniform vec4  framecalc;\n"
                                       "uniform mat4 projectionMatrix;\n"
                                       "uniform mat4 modelMatrix;\n"
                                       "//uniform mat4 rotationMatrix;\n"
                                       "void main() {\n"
                                       "  framecalcFS=framecalc;\n"
                                       "  texCoord=vtxcoord.zw;\n"
                                       "  gl_Position = projectionMatrix*modelMatrix*vec4(vtxcoord.x,vtxcoord.y,-0.5f, 1.0f);\n"
                                       "}\n"
                                       );
    qDebug() << "addShaderFromSourceCode Vertex" << m_program->log();
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       "#version 300 es\n"

                                       "precision mediump float;\n"
                                       "out vec4 my_fragcolor;\n"
                                       "in vec2 texCoord;\n"
                                       "in vec4 framecalcFS;\n"

                                       "uniform sampler2D tex;\n"
                                       "\n"
                                       "void main() {\n"
                                       "\n"

                                       "my_fragcolor=texture(tex, vec2(texCoord.x*framecalcFS.x+framecalcFS.z,texCoord.y*framecalcFS.y+framecalcFS.w));\n"
                                       "my_fragcolor.w=0.5;\n"
                                       "if (my_fragcolor.r<0.5f && my_fragcolor.g<0.5f && my_fragcolor.b<0.5f)"
                                       "discard;\n"
                                       "}\n"
                                       );
    qDebug() << "addShaderFromSourceCode Fragment" << m_program->log();
    m_projection_matrix.setToIdentity();
    m_projection_matrix.ortho(0.0,  width, height, 0.0f, -1.0f, 1.0f);
    m_projection_matrix.translate(traslx/scale+width/2.0f,0.1,0.0);


    m_program->bindAttributeLocation("vtxcoord",0);
    if (m_program->link())
        qDebug()<< "Link OK";

    if(m_VAOEXT.initializeOpenGLFunctions())
        qDebug()<< "initializeOpenGL OK";
    m_VAOEXT.glGenVertexArraysOES(1,  &VertexArrayID);
    m_VAOEXT.glBindVertexArrayOES(VertexArrayID);
    // genera il VBO
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
    glVertexAttribPointer(
                0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                4,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );

    glEnableVertexAttribArray(0);

    m_texture  =  new QOpenGLTexture(image);

    m_texture->setMinificationFilter(QOpenGLTexture::Nearest);
    m_texture->setMagnificationFilter(QOpenGLTexture::Nearest);

    // set the wrap mode
    m_texture->setWrapMode(QOpenGLTexture::Repeat);


    glBindBuffer(GL_ARRAY_BUFFER, 0);
    m_VAOEXT.glBindVertexArrayOES(0);

    if (m_program->bind())
    {

        m_program->setUniformValue("projectionMatrix", m_projection_matrix);
    }
    int maxSize;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE,&maxSize);
    qDebug() << "Texture maxSize:" << maxSize;
}
void GLAnimateBall::Flush(const float32 coordX,const float32 coordY,float32 offsetY)
{
    if (m_program->bind())
    {


        if (!fireOff)
        {
            QVector4D framecalc;

            m_modelMatrix.setToIdentity();
            m_modelMatrix.translate(coordX*m_rateWorldScreen,offsetY+m_heightScreen-coordY*m_rateWorldScreen-m_rateWorldScreen,-.4);

            m_program->setUniformValue("modelMatrix", m_modelMatrix);
            int calcFrame=m_currentFrame;



            const short ncolonne=8;
            const short nrighe=4;

            float scaleX=1.0f/ncolonne;
            float scaleY=1.0f/nrighe;
            float stepX,stepY;
            //"int istepY;\n"

            stepX=calcFrame%ncolonne;
            stepY=calcFrame/ncolonne;
            framecalc.setX(scaleX);
            framecalc.setY(scaleY);
            framecalc.setZ(scaleX*stepX);
            framecalc.setW(scaleY*stepY);



            m_program->setUniformValue("framecalc", framecalc);


            m_texture->bind();

        }

        m_VAOEXT.glBindVertexArrayOES(VertexArrayID);

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

        /*
        m_modelMatrix.setToIdentity();
        m_modelMatrix.translate(coordX*m_rateWorldScreen,m_heightScreen-coordY*m_rateWorldScreen-50);

        m_program->setUniformValue("modelMatrix", m_modelMatrix);

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

*/


        if (!fireOff)
            m_texture->release();
        m_VAOEXT.glBindVertexArrayOES(0);










        m_program->release();

        //        qDebug() << "current Frame:"<<calcFrame << framecalc;

        m_currentFrame = (m_currentFrame+1)%m_TotFrame;

    }
}

GLAnimateBall::~GLAnimateBall()
{
    // delete m_texture;
    if (m_program!=nullptr)
    {
    delete m_program;
    glDeleteBuffers(1, &vertexbuffer);
    m_VAOEXT.glDeleteVertexArraysOES(1, &VertexArrayID);
}
    qDebug() << "Anmate ball delete";
}
