#include "mycontactlistener.h"
#include "typeglobal.h"
#include "renderitemraw.h"
#include "Common/b2Math.h"
#include <QDebug>
#include <QThread>
#include "clipper.hpp"
/*
===========================================================================
                       Public Function Prototypes gpc
===========================================================================
*/

using namespace ClipperLib;


MyContactListener::MyContactListener( const QUrl url, ViewGL* mainWin)
{
    m_mainWin=mainWin;
    srand((unsigned)time(NULL));
    m_sound[0].setSource(QUrl::fromLocalFile(url.path().append("/sounds/iron-bar.wav")));
    m_sound[1].setSource(QUrl::fromLocalFile(url.path().append("/sounds/wall.wav")));
    m_sound[2].setSource(QUrl::fromLocalFile(url.path().append("/sounds/glass-bottle.wav")));
    m_sound[3].setSource(QUrl::fromLocalFile(url.path().append("/sounds/impact_1.wav")));
    m_sound[4].setSource(QUrl::fromLocalFile(url.path().append("/sounds/impact_2.wav")));
    m_sound[5].setSource(QUrl::fromLocalFile(url.path().append("/sounds/impact_3.wav")));
    m_sound[6].setSource(QUrl::fromLocalFile(url.path().append("/sounds/wall.wav")));

    m_sound[0].setVolume(0.5);
    m_sound[1].setVolume(0.5);
    m_sound[2].setVolume(1);
    m_sound[3].setVolume(1);
    m_sound[4].setVolume(1);
    m_sound[5].setVolume(1);
    initHapticEffect();
}



void addPoint(int x, int y, Path *path)
{
    IntPoint ip;
    ip.X = x;
    ip.Y = y;
    path->push_back(ip);
}
void convertLoopToPath(const b2Fixture* chain,Path* subj)
{
    int32 i;
    int32 nTotEdge= chain->GetShape()->GetChildCount();
    b2EdgeShape edge;
    b2Vec2 point;
    const b2Transform& xf = chain->GetBody()->GetTransform();



    for ( i = 0; i <nTotEdge; i++) {


        ((b2ChainShape*)chain->GetShape())->GetChildEdge(&edge, i);
        //qDebug() << "point" << edge.m_vertex1.x << edge.m_vertex1.y;
        point= b2Mul(xf, edge.m_vertex1);

        //  qDebug() << "pointT" << point.x << point.y;

        addPoint(1000*point.x,1000*point.y, subj);


    }
    //  subj->push_back(p);
}


bool convertPathToLoop(Path* subj,  b2ChainShape* chain ,  const b2Transform& xf)
{
    if (subj->size()<4)
        return false;

    bool isok=true;
    unsigned int i,ind=1;

    int nTotEdge;
    nTotEdge=subj->size()-1;
    b2Vec2 point,pointmul;
    b2Vec2* vertices= (b2Vec2*) malloc((nTotEdge+1)*sizeof(b2Vec2));
    for ( i = 0; i <subj->size()-1; i++)
    {
        IntPoint ip=subj->at(i);
        point.Set(ip.X/1000.,ip.Y/1000.);
        //point.Set(polygon->contour[0].vertex[i].x,polygon->contour[0].vertex[i].y);
        pointmul=b2MulT(xf,point);
        if (i==0)
            vertices[0]=pointmul;
        else
        {
            if (b2DistanceSquared(pointmul,vertices[ind-1]) >2000.*b2_linearSlop * b2_linearSlop)
            {
                vertices[ind]=pointmul;
                ind++;
            }

            else
            {
                //    isok=false;
                nTotEdge--;
                // vertices[i-1]=pointmul;
                //  pointmul+=b2Vec2(.3,.3);
                // vertices[i]=pointmul;
                // qDebug() << "##b2Assert##";
                //  qDebug() << "pointmul:"<< pointmul.x << "-"<< pointmul.y;
                //  qDebug() << "Prev:"<< vertices[i-1].x << "-"<<vertices[i-1].y;
            }
        }


    }

    IntPoint ip1=subj->at(subj->size()-1);
    //point.Set(polygon->contour[0].vertex[polygon->contour[0].num_vertices-1].x,polygon->contour[0].vertex[polygon->contour[0].num_vertices-1].y);
    point.Set(ip1.X/1000.,ip1.Y/1000.);
    pointmul=b2MulT(xf,point);

    if (b2DistanceSquared(pointmul,vertices[0]) >b2_linearSlop * b2_linearSlop)
        vertices[nTotEdge]=pointmul;
    else
    {
        //    isok=false;
        nTotEdge--;
        // pointmul+=b2Vec2(.3,.3);
        // vertices[i]=pointmul;
        qDebug() << "b2Assert last-first";
    }


    //    if (isok)
    chain->CreateLoop(vertices,nTotEdge+1);

    free(vertices);
    return isok;
}


void logChian(b2ChainShape* chain)
{
    int i;
    b2EdgeShape edge;


    for ( i = 0; i <chain->GetChildCount(); i++)
    {
        chain->GetChildEdge(&edge, i);
    //    qDebug() << "segment:" << "("<<edge.m_vertex1.x <<","<< edge.m_vertex1.y<<")--("<<edge.m_vertex2.x <<","<< edge.m_vertex2.y<<")";

    }

}
void logPaths(const Paths p)
{
   // qDebug() << "***";
    for (unsigned int i=0; i<p.size(); i++)
    {
        Path p3 = p.at(i);

        for (unsigned j=0; j<p3.size(); j++)
        {
            IntPoint ip = p3.at(j);
        //    qDebug() << j<<ip.X/1000.<<ip.Y/1000.;

        }

    }
//    qDebug() << "***";
}
int MyContactListener::vibra_evdev_file_search(bool *supportsRumble, bool *supportsPeriodic){
    int i = 0;
    int fp = 1;
    char device_file_name[24];
    unsigned long features[4];
    /* fail safe stop at 256 devices */
    while (fp && i < 256) {
        sprintf(device_file_name, "/dev/input/event%d", i);
        fp = open(device_file_name, O_RDWR);
        if (fp == -1) {
            qDebug() << "Unable to open input file";
            break;
        }
        if (ioctl(fp, EVIOCGBIT(EV_FF, sizeof(unsigned long) * 4), features) < 0) {
            qDebug() << "Ioctl query failed";
            close(fp);
            i++;
            continue;
        }
        *supportsRumble = false;
        *supportsPeriodic = false;
        if (test_bit(FF_RUMBLE, features))
            *supportsRumble = true;
        if (test_bit(FF_PERIODIC, features))
            *supportsPeriodic = true;
        if (*supportsRumble || *supportsPeriodic)
            return fp;
        close(fp);
        i++;
    }
    return -1;
}
void MyContactListener::playHapticEffect(const unsigned int intensity,const unsigned int duration)
{
    struct input_event event;
    event.type=EV_FF;
    m_effect.u.rumble.strong_magnitude = intensity;
    //  effect.u.rumble.weak_magnitude = intensity*0.5;
    m_effect.replay.length=duration;
    // qDebug() << "Upload rumble effect... ";
    // fflush(stdout);

    int err = ioctl(m_fd, EVIOCSFF, &m_effect);
    if (err == -1) {
        printf("failed\n");
        qDebug() << QString("ioctl error: %1").arg(errno);
        return ;
    }
    //  qDebug() <<QString("id=%1").arg(m_effect.id);


    event.code = m_effect.id;
    event.value = 1;

    m_effect.direction=0;

    err = write(m_fd, (const void*) &event, sizeof(event));
    if (err == -1) {
        qDebug() << QString("failed to start rumble effect (err=%1)").arg(errno);
        return;

    }
}

void MyContactListener::initHapticEffect()
{
    bool supportsRumble = false, supportsPeriodic = false;







    m_effect.type = FF_RUMBLE;
    m_effect.id = -1;
    m_effect.u.rumble.strong_magnitude = 0x0000;
    m_effect.u.rumble.weak_magnitude = 0x0000;
    m_effect.direction= 0x0000 ;
    //effect.replay.length = 5000;
    m_effect.replay.delay = 0;

    // err;




    //   intensity = 0xffff;// 0x0001-0xffff
    //   duration=100;//  0x0001-32767 ms (0x7fff)

    m_fd  = vibra_evdev_file_search(&supportsRumble, &supportsPeriodic);
    qDebug() << "Supports Rumble: " << supportsRumble;
    qDebug() << "Supports Periodic: " << supportsPeriodic;









}

void MyContactListener::ballIron(b2Contact* contact,Resource* bodyUserData1)
{

    const b2Manifold* manifold = contact->GetManifold();
    fixtureToModify bodyMod;
    int numPointsContact = manifold->pointCount; // numero contatti
    //qDebug() <<"num contatti:"<< numPointsContact;
    if (numPointsContact == 0 || numPointsContact>1)
    {
        return;
    }

    // deformo iron_border
    //trovo la velocita' della palla
    b2Fixture* ballfixture;
    b2Fixture* edgefixture;
    b2Body* ballbody;

    if (bodyUserData1->type==iron_border)
    {
        ballfixture = contact->GetFixtureB();
        edgefixture = contact->GetFixtureA();

    }
    else
    {
        ballfixture = contact->GetFixtureA();
        edgefixture = contact->GetFixtureB();
    }
    ballbody=ballfixture->GetBody();
    b2WorldManifold worldManifold;
    contact->GetWorldManifold( &worldManifold );
    float32 vef=b2Dot(worldManifold.normal,ballbody->GetLinearVelocity());
    float32 mass= sphereMass(ballfixture);
    float32 momentum=vef*mass;
 //   qDebug() << "fabs(momentum)" << momentum << 100;
    //    if (!pMainWin->isNeedModifyOn())
    if (fabs(momentum)>60) //  value calculated experimentally
    {
        if(edgefixture->GetShape()->GetChildCount()>3) // if edge of fixture are > 3
        {


        //    qDebug() << "play";
            if (!m_sound[0].isPlaying())
                m_sound[0].play();

            b2Vec2 pointContact=ballfixture->GetBody()->GetWorldCenter() ;//worldManifold.points[0];
            b2Vec2 pointContactDeep;

            Paths path1,path2,result;


            float32 radius= ballfixture->GetShape()->m_radius;
            //  float32 dist=((float32)rand()/(float32)RAND_MAX);



            float32 dist=qMin(fabs(momentum*radius*radius)/400.,1.0*radius);//(fabs(momentum)+2000)/2000.;

       //     qDebug() << "#####"<<fabs(momentum*radius*radius)/400.<<"diam:"<<2.0*radius;


            // qDebug() << "dist" <<dist;
            float32 gamma=atan2(ballbody->GetLinearVelocity().x,ballbody->GetLinearVelocity().y);
            //energia cinetica sfera che rotola lungo un piano inclinato:
            // K=7/10*M*v^2
            float32 density=ballfixture->GetDensity();
            float32 volume=4/3.*M_PI*radius*radius*radius;
            float32 sphereMass=density*volume;
            float32 modulev=ballbody->GetLinearVelocity().Length();
            float32 Kinetic=7/10.*sphereMass*modulev*modulev;
            b2Vec2 shapeBullet[7];

            pointContactDeep.Set(pointContact.x+dist*sin(gamma),pointContact.y+dist*cos(gamma));

            // qDebug() << "gamma:"<<gamma*180/b2_pi << "Point Contact:("<<pointContact.x<<","<<pointContact.y<<")"<< "pointContactDeep:("<<pointContactDeep.x<<","<<pointContactDeep.y<<")"<< "dist:"<<dist << "Normal: x"<< worldManifold.normal.x<<"y"<< worldManifold.normal.y;


            //   qDebug() << "Velocity x=" << ballbody->GetLinearVelocity().x<<"y:"<<ballbody->GetLinearVelocity().y;
            //   qDebug() << "Velocity normal:" << b2Dot(worldManifold.normal,ballbody->GetLinearVelocity());
            // qDebug() << "Kinetic" << Kinetic<< radius;

            //int intensity=Kinetic*65535

            playHapticEffect(map(Kinetic,0,10000,0,65535),50);

            radius+=0.1;

            shapeBullet[0].Set(pointContact.x+radius*cos(-gamma),pointContact.y+radius*sin(-gamma));
            shapeBullet[1].Set(pointContactDeep.x+radius*cos(-gamma),pointContactDeep.y+radius*sin(-gamma));

            shapeBullet[2].Set(pointContactDeep.x+radius*cos(b2_pi/4.-gamma),pointContactDeep.y+radius*sin(b2_pi/4.-gamma));

            shapeBullet[3].Set(pointContactDeep.x+radius*cos(b2_pi/2.-gamma),pointContactDeep.y+radius*sin(b2_pi/2.-gamma));

            shapeBullet[4].Set(pointContactDeep.x+radius*cos(3.0*b2_pi/4.-gamma),pointContactDeep.y+radius*sin(3.0*b2_pi/4.-gamma));


            shapeBullet[5].Set(pointContactDeep.x+radius*cos(b2_pi-gamma),pointContactDeep.y+radius*sin(b2_pi-gamma));
            shapeBullet[6].Set(pointContact.x+radius*cos(b2_pi-gamma),pointContact.y+radius*sin(b2_pi-gamma));


            Path bullet;

            addPoint(1000*shapeBullet[0].x,1000*shapeBullet[0].y,&bullet);
            addPoint(1000*shapeBullet[1].x,1000*shapeBullet[1].y,&bullet);
            addPoint(1000*shapeBullet[2].x,1000*shapeBullet[2].y,&bullet);
            addPoint(1000*shapeBullet[3].x,1000*shapeBullet[3].y,&bullet);
            addPoint(1000*shapeBullet[4].x,1000*shapeBullet[4].y,&bullet);
            addPoint(1000*shapeBullet[5].x,1000*shapeBullet[5].y,&bullet);
            addPoint(1000*shapeBullet[6].x,1000*shapeBullet[6].y,&bullet);
            path1.push_back(bullet);
            //    logPaths(path1);





            //qDebug() << "prima:";
            //  logChian((b2ChainShape*) edgefixture->GetShape());

            //   convertLoopToPolygon(edgefixture, &polygon);

            Path pa;
            convertLoopToPath(edgefixture, &pa);


            path2.push_back(pa);


            //  logPaths(path2);


            //   logChian(chainShape);
            //  qDebug() <<"count polygon:" <<polygon1.contour[0].num_vertices;
            //  Path p4 = subj.at(0);
            //   convertPathToLoop(&p4, chainShape,edgefixture->GetBody()->GetTransform());
            //  logChian(chainShape);

            //  gpc_log_polygon(&polygon);


            // gpc_polygon_clip (GPC_DIFF,&polygon,&polygon1,&result);

            Clipper c;

            c.AddPaths(path1,ptClip,true);
            c.AddPaths(path2,ptSubject,true);
            c.Execute(ctDifference,result,pftEvenOdd,pftEvenOdd);
            unsigned  numCont=result.size();
            //  logPaths(result);
            //  b2Transform iden;
            //iden.SetIdentity();

            //    gpc_log_polygon(&result);

            qDebug()<< "num of contour:"<<result.size();
            //   bool nothing[2];
            //   nothing[0]=false;
            //   nothing[1]=false;

            //b2ChainShape *chainShape;
            b2ChainShape *chainShape1=NULL;
            b2ChainShape *chainShape2=NULL;
            bool isok=true;
            if (numCont>=3 || numCont<=0)
                isok=false;
            else
            {
                if (numCont>0)
                {
                    chainShape1=new b2ChainShape();
                    Path poly=result.at(0);
                    double surface=Area(poly)*0.000001;
               //     qDebug()<<"area 1"<<surface;
                    if (!convertPathToLoop(&result.at(0),chainShape1,edgefixture->GetBody()->GetTransform())|| surface<0.01)
                    {
                        isok=false;
                        qDebug() << "**nothing** 1";
                        delete chainShape1;
                        chainShape1=NULL;

                    }
                }
                if (numCont>1)
                {

                    chainShape2=new b2ChainShape();
                    Path poly=result.at(1);
                    double surface=Area(poly)*0.000001;
                 //   qDebug()<<"area 2"<<surface;
                    if (!convertPathToLoop(&result.at(1),chainShape2,edgefixture->GetBody()->GetTransform())||surface<0.01)
                    {
                        isok=false;
                        qDebug() << "**nothing**  2";
                        delete chainShape2;
                        chainShape2=NULL;

                    }
                }

            }




            if(!isok)
            {
             //   qDebug() << "problemi non fare nulla:";
                return;
            }
            //   b2Fixture *myFixture;
            b2FixtureDef myFixtureDef1;
            b2FixtureDef myFixtureDef2;


            //  memset(myFixtureDef,0,3*sizeof(b2FixtureDef));

            // tot point of chain









            /*
        for (unsigned  j = 0; j <2; j++)
        {
            myFixtureDef[j].shape=NULL;
            bodyMod.newFixtureDef[j]=NULL;

        }
        */
            // for (unsigned  j = 0; j <2; j++)
            // {



            myFixtureDef1.shape=NULL;
            //bodyMod.newFixtureDef[0]=NULL;


            bodyMod.oldFixture=edgefixture;

            if (chainShape1!=NULL)
            {
                myFixtureDef1.density=edgefixture->GetDensity();
                myFixtureDef1.filter=edgefixture->GetFilterData();
                myFixtureDef1.friction=edgefixture->GetFriction();
                myFixtureDef1.isSensor=edgefixture->IsSensor();
                myFixtureDef1.restitution=edgefixture->GetRestitution();
                myFixtureDef1.userData=edgefixture->GetUserData();

                myFixtureDef1.shape = chainShape1;
                bodyMod.newFixtureDef[0]=myFixtureDef1;




            }


            myFixtureDef2.shape=NULL;
            //bodyMod.newFixtureDef[1]=NULL;


            if (chainShape2!=NULL)
            {
                myFixtureDef2.density=edgefixture->GetDensity();
                myFixtureDef2.filter=edgefixture->GetFilterData();
                myFixtureDef2.friction=edgefixture->GetFriction();
                myFixtureDef2.isSensor=edgefixture->IsSensor();
                myFixtureDef2.restitution=edgefixture->GetRestitution();
                myFixtureDef2.userData=edgefixture->GetUserData();


                myFixtureDef2.shape = chainShape2;
                bodyMod.newFixtureDef[1]=myFixtureDef2;



            }




            bodyMod.ballBody=ballbody;









            m_mainWin->addFixModModify(bodyMod);

          //  if (bodyMod.newFixtureDef[0].shape!=NULL)
            //    qDebug() << "n vert contour 1"<< bodyMod.newFixtureDef[0].shape->GetChildCount();
          //  if (bodyMod.newFixtureDef[1].shape!=NULL)
            //    qDebug() << "n vert contour 2"<< bodyMod.newFixtureDef[1].shape->GetChildCount();


        //    qDebug() << "tumb!!!!";
            short index=((Resource*)edgefixture->GetBody()->GetUserData())->index_iron;
            m_mainWin->setNeedModifyOn(index-1);
            //    pMainWin->setNeedTriangulate(true);

        }
    }
    else
    {
       // qDebug() << "Momentum:"<< momentum<< ballbody->GetLinearVelocity().x<<"-" <<ballbody->GetLinearVelocity().y;
        if (fabs(momentum)>10)
            if (!m_sound[5].isPlaying())
                m_sound[5].play();
    }

}
void MyContactListener:: BeginContact(b2Contact* contact)
{


    Resource* bodyUserData1 = (Resource*) contact->GetFixtureA()->GetBody()->GetUserData();
    Resource* bodyUserData2= (Resource*) contact->GetFixtureB()->GetBody()->GetUserData();
    if (bodyUserData1!=NULL && bodyUserData2!=NULL)
    {
        //     qDebug() << bodyUserData1->type;
        //    qDebug() << bodyUserData2->type;

    }
    else //undefined body vs ?
    {
        bool isBall=false;

        b2WorldManifold worldManifold;
        contact->GetWorldManifold( &worldManifold );
      //  float32 momentum=0;
         float32 v1=0;
        if (bodyUserData1!=NULL)
        {
          //  v1=contact->GetFixtureA()->GetBody()->GetLinearVelocity().LengthSquared();


           // float32 mass= sphereMass(ballfixture);
           // float32 momentum=vef*mass;

            if(bodyUserData1->type==ball)
            {

                v1=b2Dot(worldManifold.normal,contact->GetFixtureA()->GetBody()->GetLinearVelocity());
                //momentum=v1*sphereMass(contact->GetFixtureA());
                isBall=true;
            }


        }
        else if (bodyUserData2!=NULL)
        {

//            v1=contact->GetFixtureB()->GetBody()->GetLinearVelocity().LengthSquared();

            if(bodyUserData2->type==ball)
            {
                isBall=true;
                v1=b2Dot(worldManifold.normal,contact->GetFixtureB()->GetBody()->GetLinearVelocity());
            //    momentum=v1*sphereMass(contact->GetFixtureB());
            }
        }


         //  qDebug() << "BALL !!"<<v1;
        if (fabs(v1)>15)
        {

            if (isBall) //undefined body vs ball
            {
            //    qDebug() << "BALL 5!!";
                if (!m_sound[5].isPlaying())
                    m_sound[5].play();
                else
                    m_sound[1].play();
            }
            else
            {
           //     qDebug() << "BALL 3!!";
                if (!m_sound[3].isPlaying())
                    m_sound[3].play();
            }
        }

    }
    if(!m_mainWin->isDrawing())
    {
        if (bodyUserData1!=NULL && bodyUserData2!=NULL)

        {

            if ((bodyUserData1->type==ball && bodyUserData2->type==iron_border) ||
                    (bodyUserData1->type==iron_border && bodyUserData2->type==ball)  )
            {

            }

            // ball vs iron Border
            if ((bodyUserData1->type==ball && bodyUserData2->type==iron_border) ||
                    (bodyUserData1->type==iron_border && bodyUserData2->type==ball)  )

            {
                ballIron(contact, bodyUserData1);
                return;
            }

            // ball vs ball
            if ((bodyUserData1->type==ball && bodyUserData2->type==ball) ||
                    (bodyUserData1->type==ball && bodyUserData2->type==ball)  )
            {
                if (contact->GetFixtureA()->GetBody()->GetLinearVelocity().LengthSquared()+ contact->GetFixtureB()->GetBody()->GetLinearVelocity().LengthSquared()>100)
                {
                    if (!m_sound[1].isPlaying())
                        m_sound[1].play();
                }
                return;
            }

        }
        if (!m_mainWin->getIsDestroying())
        {
            if (bodyUserData1!=nullptr)
            {
                if (bodyUserData1->type==glass_dynamic_body ) // body1 glass

                {
                    b2MassData mdata;
                    b2Body* bbreak=contact->GetFixtureA()->GetBody();
                    bbreak->GetMassData(&mdata);



                    double eTot=0.5*mdata.mass*bbreak->GetLinearVelocity().LengthSquared()+0.5*0.08*mdata.mass*bbreak->GetInertia()*bbreak->GetAngularVelocity()*bbreak->GetAngularVelocity();

                    // qDebug() << "break!!!!"<<mdata.mass<< eTot;
                    //  qDebug() << eTot;
                    if (eTot>400)
                    {
                        qDebug() << "break1!!!!"<<mdata.mass<< eTot;
                        if (!m_sound[2].isPlaying())
                            m_sound[2].play();
                        m_mainWin->addBodyBreak(bbreak);
                        return;
                    }
                    else
                    {
                        if (!m_sound[4].isPlaying())
                            m_sound[4].play();
                    }
                }
            }
            if (bodyUserData2!=nullptr) {

                if (bodyUserData2->type==glass_dynamic_body ) // body2 glass

                {
                    b2Body* bbreak=contact->GetFixtureB()->GetBody();
                    b2MassData mdata;
                    bbreak->GetMassData(&mdata);
                    //  qDebug() << "break2!!!!"<<mdata.mass;
                    double eTot=0.5*mdata.mass*bbreak->GetLinearVelocity().LengthSquared()+0.5*0.08*mdata.mass*bbreak->GetInertia()*bbreak->GetAngularVelocity()*bbreak->GetAngularVelocity();

                    if (eTot>400)
                    {
                   //    qDebug() << "break3!!!!"<<mdata.mass<< eTot;

                        if (!m_sound[2].isPlaying())
                            m_sound[2].play();
                        m_mainWin->addBodyBreak(bbreak);
                        return;
                    }
                    else
                    {
                        if (!m_sound[4].isPlaying())
                            m_sound[4].play();
                    }


                }
            }
        }
    }
    //

}
void MyContactListener:: EndContact(b2Contact* contact)
{
    Resource* bodyUserData1 = (Resource*) contact->GetFixtureA()->GetBody()->GetUserData();
    Resource* bodyUserData2= (Resource*) contact->GetFixtureB()->GetBody()->GetUserData();
    if (bodyUserData1!=NULL && bodyUserData2!=NULL)
    {
        //    qDebug() << bodyUserData1->type;
        //  qDebug() << bodyUserData2->type;

    }
}
