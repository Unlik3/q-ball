#include <QDebug>

#include <Box2D/Collision/Shapes/b2Shape.h>

#include <Box2D/Dynamics/b2Fixture.h>
#ifndef TYPEGLOBAL
#define TYPEGLOBAL
enum BodyType{
    ball=0,
    fireball=1,
    iron_border=2,
    glass_dynamic_body=3,
    glass_dynamic_body_unbreakable=4,
    wood_dynamic_body=5,


    test=99
};
typedef struct{
    BodyType type;
    float32 *data;
    short index_iron=0;
}Resource;

typedef struct {
    b2Body* ballBody; // body to modify
    b2FixtureDef newFixtureDef[2]; // new FixtureDef
    b2Fixture* oldFixture; // old Fixture to delete

} fixtureToModify;
/*
enum SEGMENT_INTERSEC{
   SEGMENT_INTERSEC_NONE=0,
   SEGMENT_INTERSEC_EXTREMITY_P1=1,
   SEGMENT_INTERSEC_EXTREMITY_P2=2,
   SEGMENT_INTERSEC_CROSS=3,
   SEGMENT_IS_POINT=4

};
*/
#define BITS_PER_LONG (sizeof(long) * 8)
#define LONG(x) ((x)/BITS_PER_LONG)
#define OFF(x)  ((x)%BITS_PER_LONG)
#define test_bit(bit, array)    ((array[LONG(bit)] >> OFF(bit)) & 1)

inline long map(long x, long in_min, long in_max, long out_min, long out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
//energia cinetica sfera che rotola lungo un piano inclinato:
// K=7/10*M*v^2
inline float32 sphereKineticEnergy(b2Fixture* ballfixture) {

    float32 radius = ballfixture->GetShape()->m_radius;
    float32 density=ballfixture->GetDensity();
    float32 volume=4/3.*M_PI*radius*radius*radius;
    float32 sphereMass=density*volume;
    float32 modulev=ballfixture->GetBody()->GetLinearVelocity().Length();
    float32 Kinetic=7/10.*sphereMass*modulev*modulev;
    return Kinetic;
}
inline float32 sphereMass(b2Fixture* ballfixture) {

    float32 radius = ballfixture->GetShape()->m_radius;
    float32 density=ballfixture->GetDensity();

    float32 volume=4/3.*M_PI*radius*radius*radius;
    float32 sphereMass=density*volume;

    return sphereMass;
}

#endif // TYPEGLOBAL


