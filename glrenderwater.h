#ifndef GLRENDERWATER_H
#define GLRENDERWATER_H

#include <QObject>
#include <QtGui/QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QVector>
#include <Box2D/Box2D.h>
#include <QUrl>
#include <QImage>
#include <QOpenGLTexture>
#define  NUM_BACKGROUND_WAVES  7

class GLRenderWater : public QObject
{
    Q_OBJECT
public:
    explicit GLRenderWater(b2Vec2 seg1p1,b2Vec2 seg1p2,b2Vec2 seg2p1,b2Vec2 seg2p2 , QObject *parent = 0);
    ~GLRenderWater();
    int32 m_count;
    inline int getNumVertex(){return m_count;}
    enum { e_maxVertices = 256 };
    b2Vec2 m_vertices[e_maxVertices];
    b2Color m_colors[e_maxVertices];
    b2Vec2 m_uvmap[e_maxVertices];
    bool m_firstFlush;
    QOpenGLShaderProgram *m_program;
    QMatrix4x4                  m_projection_matrix;
    QVector3D                   m_param;
    QOpenGLBuffer m_vertexBuffer;
    QOpenGLBuffer m_colorBuffer;
    QOpenGLBuffer m_uvBuffer;
    QOpenGLTexture *m_texture;

    void Create(int width,int height,float32 scale,const QUrl dataUrl,float32 traslx);
    void Vertex(const b2Vec2 &v,const  b2Color &c);
    void Flush(float32 offsetY);
    void Splash(float32 xPosition, float32 velocity);
    float32 getLevelWater(float32 xPosition);
protected:
    struct waterColumn
    {
        float32 x;
        float32 y;
        b2Vec2 spd;
        float32 mass;
    };
    QVector <waterColumn> wavePoints;
    float32 offset;

    // Amounts by which a particular sine is offset
    float32 sineOffsets[NUM_BACKGROUND_WAVES];

    // Amounts by which a particular sine is amplified
    float32 sineAmplitudes[NUM_BACKGROUND_WAVES] ;
    // Amounts by which a particular sine is stretched
    float32 sineStretches[NUM_BACKGROUND_WAVES] ;
    // Amounts by which a particular sine's offset is multiplied
    float32 offsetStretches[NUM_BACKGROUND_WAVES] ;
    float32 overlapSines(float32 x);
    // Resolution of simulation
    int NUM_POINTS ;
    // Width of simulation
    float32 WIDTH ;
    // start of sim
    float32 X_OFFSET;
    // start of sim
    float32 Y_START;


    // Spring constant for forces applied by adjacent points
    float32 SPRING_CONSTANT;
    // Sprint constant for force applied to baseline
    float32 SPRING_CONSTANT_BASELINE;
    // Vertical draw offset of simulation
    float32 Y_OFFSET ;
    // Damping to apply to speed changes
    float32  DAMPING ;
    // Number of iterations of point-influences-point to do on wave per step
    // (this makes the waves animate faster)
    float32 ITERATIONS ;
    float32 BACKGROUND_WAVE_MAX_HEIGHT ;
    float32 BACKGROUND_WAVE_COMPRESSION ;
    bool m_firstTime;


    void updateWavePoints();
    char get_line_intersection(float32 p0_x, float32 p0_y, float32 p1_x, float32 p1_y, float32 p2_x, float32 p2_y, float32 p3_x, float32 p3_y, float32 *i_x, float32 *i_y);
signals:

public slots:
};

#endif // GLRENDERWATER_H
