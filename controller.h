#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include "Box2D.h"
#include "renderitemraw.h"
class ViewGL;
class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = 0);

   Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)
//    Q_PROPERTY(QString ball2 READ ball2 WRITE setBall2 NOTIFY ball2Changed)
//    Q_PROPERTY(QString ball3 READ ball3 WRITE setBall3 NOTIFY ball3Changed)

    Q_PROPERTY(int play1 READ play1 WRITE setPlay1 NOTIFY play1Changed)
    Q_PROPERTY(int play2 READ play2 WRITE setPlay2 NOTIFY play2Changed)
    Q_PROPERTY(int play3 READ play3 WRITE setPlay3 NOTIFY play3Changed)


    Q_PROPERTY(int tilt READ tilt WRITE setTilt NOTIFY tiltChanged)
    Q_PROPERTY(bool canpush READ canpush WRITE setCanpush NOTIFY canpushChanged)


    bool canpush() const {
        return m_canpush;
    }
    void  setCanpush(const bool canpush)  {
        if (canpush != m_canpush) {
            qDebug() << "emit canpush";
            m_canpush = canpush;
            emit canpushChanged();
        }
    }

    int play1() const {
        return m_play1;
    }
    void  setPlay1(const int& play)  {
        if (play != m_play1) {
            m_play1 = play;
            emit play1Changed();
        }
    }

    int play2() const {
        return m_play2;
    }
    void  setPlay2(const int& play)  {
        if (play != m_play2) {
            m_play2 = play;
            emit play2Changed();
        }
    }


    int play3() const {
        return m_play3;
    }
    void  setPlay3(const int& play)  {
        if (play != m_play3) {
            m_play3 = play;
            emit play3Changed();
        }
    }
    QString status() const {
        return m_status;
    }
    void  setStatus(const QString& status)  {
        if (status != m_status) {
            m_status = status;
            emit statusChanged();
        }
    }



    void setTilt(const int &t) {
        if (t != m_tilt) {
            m_tilt = t;
            emit tiltChanged();
        }
    }
    int tilt() const {
        return m_tilt;
    }

    Q_INVOKABLE void pressRight();
    Q_INVOKABLE void pressLeft();
    Q_INVOKABLE void releaseRight();
    Q_INVOKABLE void releaseLeft();
    Q_INVOKABLE void grabScreen();
    Q_INVOKABLE void restart();
    Q_INVOKABLE void openPinfoldCtrl();
    Q_INVOKABLE void closePinfoldCtrl();
    Q_INVOKABLE void pause();

    Q_INVOKABLE void setAcc(const float accx, const float accy);
    Q_INVOKABLE void preTilt();
    Q_INVOKABLE void statusTilt();
    Q_INVOKABLE void setVisibleOpenGL(bool vis);
    Q_INVOKABLE void setOffsetY(int vis);
    Q_INVOKABLE void startGame(const QString nameScheme);
    Q_INVOKABLE void stopGame();
    Q_INVOKABLE void createTeamCtrl(int indball1, int indball2, int indball3);
    Q_INVOKABLE void checkTeamCTRL( );

    Q_INVOKABLE int countBallCtrl();
    Q_INVOKABLE void impossiblePush();
    Q_INVOKABLE void randomBreak();


    void setPaintGL(ViewGL *viewGL);



    void somethingHappened(QString text);

public slots:
    void updateCaption();
    void canPushing();

protected:
private:
    int m_tilt;

    ViewGL *m_viewGL;

    int m_play1,m_play2,m_play3;
    QString m_status;
    bool m_canpush;


signals:
    void sendToQml(int count);
     void tiltChanged();
    void play1Changed();
    void play2Changed();
    void play3Changed();
    void statusChanged();
    void canpushChanged();
public slots:

};

#endif // CONTROLLER_H
