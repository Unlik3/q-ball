#include "glrendertexturetriangles.h"
#include <QDebug>


GLRenderTextureTriangles::GLRenderTextureTriangles(QObject *parent) :
    QObject(parent)
{
    m_count=0;
     m_program=nullptr;
    m_firstFlush=true;
}
void GLRenderTextureTriangles::Create(int width,int height,float32 scale,float32 traslx,const QString pathImage,const float32 transp)
{

    //  QRect rect(0, 0, 540, 960);
    QImage image;

    qDebug() << "triangle texture :" <<sizeof(m_vertices)+sizeof(m_uv);
    //  imageLoad=QImage();

    // QUrl url;
    // url= SailfishApp::pathTo("data");
    if (image.load(pathImage))
        qDebug() << pathImage <<"Image texture OK";
    //image.scaled(0,-1);
    image=image.mirrored(false,true);
    qDebug() << "texture width:" <<image.width()/scale;
    qDebug() << "texture height:" <<image.height()/scale;
    //image=imageLoad.copy(rect);

    m_program = new QOpenGLShaderProgram();
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex,
                                       "#version 300 es\n"
                                       "in vec2 vtxcoord;\n"
                                       "in vec2 vertexuv;\n"
                                       "out vec2 uv;\n"
                                       "out float transp;\n"
                                       "uniform mat4 projectionMatrix;\n"
                                       "uniform vec4 param;\n"
                                       "void main() {\n"
                                       "uv = vertexuv;\n"
                                       "transp=param.z;\n"
                                       "gl_Position = projectionMatrix*vec4(param.x*vtxcoord.x,param.w+param.y-param.x*vtxcoord.y,-0.4f, 1.0f);\n"

                                       "}\n"
                                       );
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       "#version 300 es\n"
                                       "precision mediump float;\n"
                                       "in vec2 uv;\n"
                                       "in float transp;\n"
                                       "out vec4 my_fragcolor;\n"
                                       "uniform sampler2D tex;\n"
                                       "\n"
                                       "void main() {\n"
                                       "\n"
                                       "my_fragcolor=texture(tex,vec2(uv.x,uv.y));\n"
                                       "//my_fragcolor=vec4(1.0f,1.0f,0.0f,-0.5f);\n"
                                       "my_fragcolor=transp*my_fragcolor;\n"
                                       "}\n"
                                       );
    m_program->bindAttributeLocation("vtxcoord",0);
    m_program->bindAttributeLocation("vertexuv",1);

    m_program->link();



    m_vertexBuffer.create();
    if(m_vertexBuffer.bind())
    {
        m_vertexBuffer.setUsagePattern(QOpenGLBuffer::DynamicDraw);
        m_vertexBuffer.allocate(m_vertices, sizeof(m_vertices));
        m_vertexBuffer.release();
    }


    m_uvBuffer.create();
    if(m_uvBuffer.bind())
    {
        m_uvBuffer.setUsagePattern(QOpenGLBuffer::DynamicDraw);
        m_uvBuffer.allocate(m_uv, sizeof(m_uv));
        m_uvBuffer.release();
    }




    m_texture  =  new QOpenGLTexture(image);


    m_texture->setMinificationFilter(QOpenGLTexture::Nearest);
    m_texture->setMagnificationFilter(QOpenGLTexture::Nearest);

    // set the wrap mode
    m_texture->setWrapMode(QOpenGLTexture::Repeat);




    m_projection_matrix.setToIdentity();


    m_param.setX(scale);
    m_param.setY(height);
    m_param.setZ(transp);
    m_param.setW(0);
    qDebug() << "width:"<< width<< "height:"<< height << "trasl:"<< traslx/scale+width/2.0f << "scala:"<< scale<<"trasparenza"<<transp;

    m_projection_matrix.ortho(0, width, height, 0.0f, -1.0f, 1.0f);
    m_projection_matrix.translate(traslx/scale+width/2.0f,0.0,0.0);

    m_count = 0;






    qDebug() << "Program1 link result:" << m_program->log();



}

void GLRenderTextureTriangles::Vertex(const b2Vec2& v, const b2Vec2& uv)
{

    if (m_count == e_maxVertices)
    {
        //  Flush();
        qDebug() << "### GLRenderTextureTriangles e_maxVertices###"<<m_count;
        return;
    }



    m_vertices[m_count] =v;


    m_uv[m_count] = uv;

    ++m_count;

}
void GLRenderTextureTriangles::Flush(float32 offestY)
{

    if (m_count == 0)
        return;
    if (m_firstFlush)
    {
        qDebug() << "GLRenderTextureTriangles::Tot vertex:"<<m_count;
        m_firstFlush=false;
    }
    if ( m_program->bind())
    {

        m_program->setUniformValue("projectionMatrix", m_projection_matrix);

        m_param.setW(offestY);
        m_program->setUniformValue("param", m_param);


        if (m_vertexBuffer.bind())
        {

            m_program->enableAttributeArray("vtxcoord");
            m_program->setAttributeBuffer(0, GL_FLOAT, 0,2);
            m_vertexBuffer.write(0,m_vertices, m_count * sizeof(b2Vec2));
            m_vertexBuffer.release();
        }


        if (m_uvBuffer.bind())
        {

            m_texture->bind();


                m_program->enableAttributeArray("vertexuv");
                m_program->setAttributeBuffer(1, GL_FLOAT, 0,2);
                m_uvBuffer.write(0,m_uv, m_count * sizeof(b2Vec2));







        }



        glDrawArrays(GL_TRIANGLES, 0, m_count);



        m_program->release();
    }

    m_count = 0;

}
void GLRenderTextureTriangles::destroyTexture(QOpenGLContext * context)
{

    qDebug() << "----2";
    m_texture->destroy();
    qDebug() << "----3";
    delete m_texture;
    qDebug() << "----4";
    m_texture = nullptr;
    context->doneCurrent();

}

GLRenderTextureTriangles::~GLRenderTextureTriangles()
{
    qDebug()<<"Destroy GLRenderTextureTriangles";

    //  m_texture->release();

    // delete m_texture;
    //   m_texture->destroy();
    if (m_program!=nullptr)
    {
    m_vertexBuffer.destroy();
    m_uvBuffer.destroy();


    delete m_program;
    }
}

