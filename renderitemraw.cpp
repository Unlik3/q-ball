#include "renderitemraw.h"
#include <QQuickView>
#include <QtQuick/qsgnode.h>
#include <QSGSimpleRectNode>
#include <QSGSimpleTextureNode>

//#include <QtFeedback/QFeedbackHapticsEffect>
//QFeedbackHapticsEffect

#include "qball.h"
#include "renderitemraw.h"
#include "tesselation.h"
#include <QOpenGLFunctions>
#include <QStandardPaths>
#include <QFileInfo>
#include "b2dJson/b2dJson.h"
#include <sailfishapp.h>
#include <Box2D/Box2D.h>
#include "src/libtess2/Include/tesselator.h"
#include <QCoreApplication>
#include <QTimer>
#include <QCoreApplication>
#include <QVariant>
#include <QQmlContext>
ViewGL::ViewGL(QQuickWindow *window,Controller *controller)
    : QObject()

    ,m_horizonthal_light(0)
    ,m_vertical_light(0)

{
     srand(time(nullptr));
    m_controller=controller;

    m_window=window;
    m_controller->setPaintGL(this);
    m_isDrawing=false;
    m_isDestroying=false;

    bodyToBreak=nullptr;


    m_world=nullptr;
    m_isStop=false;
    m_wasStop=false;
    m_needModScene[0]=false;
    m_needModScene[1]=false;
    m_needModScene[2]=false;
    needTriangulate[0]=false;
    needTriangulate[1]=false;
    needTriangulate[2]=false;
    bodySphere1=nullptr;
    bodySphere2=nullptr;
    bodySphere3=nullptr;

    leftFlipper=nullptr;
    rightFlipper=nullptr;
    m_controller->setPlay1(0);
    m_controller->setPlay2(1);
    m_controller->setPlay3(2);
    m_needRecreate1=false;
    m_praticing=false;
    noWater=false;
    m_lines=nullptr;
    m_triangles=nullptr;
    m_textureTriangles[0]=nullptr;
    m_textureTriangles[1]=nullptr;
    m_textureTriangles[2]=nullptr;
    m_textureStaticTriangles=nullptr;
    m_textureDynamicTriangles[0]=nullptr;
    m_textureDynamicTriangles[1]=nullptr;
    bodySphere1=nullptr;
    bodySphere2=nullptr;
    bodySphere3=nullptr;
    fireBall1=nullptr;
    m_water=nullptr;
    m_sphere1=nullptr;
    m_sphere2=nullptr;
    m_sphere3=nullptr;
    m_lines=nullptr;
    m_fireball1=nullptr;

    dataUrl= SailfishApp::pathTo("data");



    m_dataList.append(new QBall("Mokomaze", "RUS","Aluminium",1.1,2.70,0,"mokomaze","09/01/2016","mokomaze.png"));
    m_dataList.append(new QBall("z0b", "FIN","Iron",1.2,7.96,0,"jarmo.png","16/08/2018","jarmo.png"));
    m_dataList.append(new QBall("Nick 401", "ITA","Polyethylene",1.0,0.96,0.3,"alex","15/10/2016","alex.png"));
    m_dataList.append(new QBall("Miroslavova", "BGR","Polyethylene",1.05,0.96,0.2,"eye.jpg","07/8/2017","miroslavova.png"));
    m_dataList.append(new QBall("Sebastián", "MEX","Leather",1,1,0,"arambula.jpg","03/06/2016","arambula.png"));
    m_dataList.append(new QBall("Tajōmaru", "JPN","Manifold",1,0.7,0.5,"tajomaru.jpg","03/06/2016","tajomaru.png"));
    m_dataList.append(new QBall("Biko", "ZAF","Wood",1,1.07,0,"biko.jpg","15/08/2018","biko.png"));
    m_dataList.append(new QBall("Mihaela", "ROU","Nickel",0.95,8.8,0,"mihaela.png","08/04/2018","mihaela.png"));
    m_dataList.append(new QBall("Scheip 63 c", "ITA","Brass",1.05,8.44,0,"scheip63c.jpg","08/05/2018","scheip63c.png"));
    m_dataList.append(new QBall("B.B.", "FRA","Cork",0.85,0.26,0,"bb.png","26/09/2016","bb.png"));
    m_dataList.append(new QBall("0573", "ITA","Bakelite",1.2,1.3,0,"0573.gif","22/8/2018","0573.png"));
    m_dataList.append(new QBall("Paul", "AUS","Iron",1,7.96,0,"paul.png","21/08/2018","paul.png"));
    m_dataList.append(new QBall("Danilovic", "SRB","Glass",1.2,2.6,0,"globe.png","10/09/2018","globe.png"));

    //    reinterpret_cast<QBall *>(m_dataList.at(0))->setPickedtoplay(true);
    //    reinterpret_cast<QBall *>(m_dataList.at(1))->setPickedtoplay(true);
    //    reinterpret_cast<QBall *>(m_dataList.at(2))->setPickedtoplay(true);

    QQmlContext *ctxt = ((QQuickView*)  m_window)->rootContext();


    ctxt->setContextProperty(QString("myModel"), QVariant::fromValue(m_dataList));
    ctxt->setContextProperty(QString("dataURL"), QVariant::fromValue(dataUrl));


    //   ctxt->setContextProperty(QString("chooseNameBall1"), QVariant::fromValue("?"));

    //   ctxt->setContextProperty(QString("chooseNameBall2"), QVariant::fromValue("??"));

    //   ctxt->setContextProperty(QString("chooseNameBall3"), QVariant::fromValue("???"));

    sound1.setSource(QUrl::fromLocalFile(dataUrl.path().append("/sounds/flip.wav")));
    sound2.setSource(QUrl::fromLocalFile(dataUrl.path().append("/sounds/tick.wav")));
    sound3.setSource(QUrl::fromLocalFile(dataUrl.path().append("/sounds/bumper.wav")));
    sound4.setSource(QUrl::fromLocalFile(dataUrl.path().append("/sounds/wall.wav")));
    sound5.setSource(QUrl::fromLocalFile(dataUrl.path().append("/sounds/door-open.wav")));
    sound6.setSource(QUrl::fromLocalFile(dataUrl.path().append("/sounds/water-splash.wav")));
    sound7.setSource(QUrl::fromLocalFile(dataUrl.path().append("/sounds/fuse.wav")));

    sound8.setSource(QUrl::fromLocalFile(dataUrl.path().append("/sounds/endlevel.wav")));
    sound9.setSource(QUrl::fromLocalFile(dataUrl.path().append("/sounds/tilt.wav")));
    sound10.setSource(QUrl::fromLocalFile(dataUrl.path().append("/sounds/dead.wav")));

    sound1.setVolume(0.5);
    sound2.setVolume(0.5);
    sound3.setVolume(0.5);
    sound4.setVolume(0.5);
    sound5.setVolume(0.5);
    sound6.setVolume(0.5);
    sound7.setVolume(0.5);
    sound8.setVolume(0.8);
    sound9.setVolume(1.0);
    sound10.setVolume(0.9);



    m_offsetY=0;
    qDebug() << "connect";
    // myContactListenerInstance = new MyContactListener(dataUrl,this);

    //   Initialize();
    // pause();

    /*

        QFeedbackHapticsEffect *effect = new QFeedbackHapticsEffect();
        effect->setDuration(1000);
        effect->setPeriod(500);
        effect->setIntensity(1);
        effect->start();
*/

    // connect(m_window, SIGNAL(closing(QQuickCloseEvent*)),
    //         this, SLOT(closeWindow(QQuickCloseEvent*)));
    m_triangles = new GLRenderTriangles();
    m_textureTriangles[0] = new GLRenderTextureTriangles();
    m_textureTriangles[1] = new GLRenderTextureTriangles();
    m_textureTriangles[2] = new GLRenderTextureTriangles();
    m_textureStaticTriangles = new GLRenderTextureTriangles();
    m_textureDynamicTriangles[0] = new GLRenderTextureTriangles();
    m_textureDynamicTriangles[1] = new GLRenderTextureTriangles();

    m_lines     = new GLRenderLines();
    m_points    = new GLRenderPoints();
    m_sphere1   = new GLRenderSphere(this);
    m_sphere2   = new GLRenderSphere(this);
    m_sphere3   = new GLRenderSphere(this);


    m_fireball1 = new GLAnimateBall(this);

    // if (poly1!=nullptr)
    m_water     = new GLRenderWater(b2Vec2(-2.4185,2.02297),
                                    b2Vec2(-7.2101, 5.27517),
                                    b2Vec2( 2.4185 ,2.02297),
                                    b2Vec2(7.2101, 5.27517));


    m_sphere1Alfa=1.0f;
    m_sphere2Alfa=1.0f;
    m_sphere3Alfa=1.0f;
    m_timeliveFireBall=1.0f;
    myContactListenerInstance = new MyContactListener(dataUrl,this);
}

/*
void ViewGL::deleteContext()
{


qDebug() << "context:" <<m_context;

}
*/

void ViewGL::start(const QString nameScheme)
{





    connect(m_window, SIGNAL(afterRendering()),
            this, SLOT(renderGL()),
            Qt::DirectConnection);


    qDebug() << "START";
    m_isDrawing=false;
    m_isDestroying=false;


    if(Initialize(nameScheme))
    {
        m_sphere1Alfa=1.0;
        m_sphere2Alfa=1.0;

        m_sphere3Alfa=1.0;
        m_timeliveFireBall=1.0f;
        m_fireball1->m_status=GLAnimateBall::normal;
        m_fireball1->fireOff=false;

        bodySphere1=addSphere(-10,numRand(70,90)  ,reinterpret_cast<QBall *>(m_dataList.at(m_controller->play1()))->radius(),reinterpret_cast<QBall *>(m_dataList.at(m_controller->play1()))->density());
        bodySphere2=addSphere(4,numRand(90,110),reinterpret_cast<QBall *>(m_dataList.at(m_controller->play2()))->radius(),reinterpret_cast<QBall *>(m_dataList.at(m_controller->play2()))->density());
        bodySphere3=addSphere(8,numRand(110,130),reinterpret_cast<QBall *>(m_dataList.at(m_controller->play3()))->radius(),reinterpret_cast<QBall *>(m_dataList.at(m_controller->play3()))->density());


        //*****


        //    delete m_sphere2;
        //  m_sphere2->Flush(userDataSphere2,bodySphere2->GetWorldCenter().x,bodySphere2->GetWorldCenter().y,lightPos,offsetY);

        //****

      // m_world->SetGravity(b2Vec2(0,0));

        m_praticing=false;
        noWater=false;
        if (nameScheme=="Pratice.json")
        {

            m_praticing=true;
            noWater=true;
            /*
          //  m_world->SetGravity(b2Vec2(0,0));
             getWorldAABB(m_world_dimension);
            double scalaX=m_window->width()/(m_world_dimension.upperBound.x-m_world_dimension.lowerBound.x);
            double scalaY=m_window->height()/(m_world_dimension.upperBound.y-m_world_dimension.lowerBound.y);


            m_rateWorldScreen=qMin(scalaX,scalaY);
          //  b2Vec2 pinsM=b2Vec2(230,640+m_offsetY);
          // convertPixelToWorld(pinsM);
          //  b2Vec2 pinsM=b2Vec2(-5,30);
          //  bodySphere1=addSphere(pinsM.x,pinsM.y,1,1);
           for (int32 i = 0; i < 10; i++)
                addChooseBox(-13,48-i*13.3);
             //bodySphere1=addSphere(-10,45+m_offsetY*m_sca,1.0,5.0);
            //addCage(0, -30);
            //    addCage(4, 50);

            */

        }


        /*
            b2dJson json;
            qDebug() << QDir::currentPath();
            if (json.writeToFile(m_world, "myfile.json"))
                qDebug() << "Export OK!";
            else
                qDebug() << "Export fails!";
                */

        qDebug() << "Start timer"<<mTimer.isActive()<<mTimer.timerId();
        mTimer.start(1000.f / 60.0f , this);
        qDebug() << "ID timer"<<mTimer.timerId();
        m_isStop=false;
        // m_offsetY=0;
        openGLVisible=true;
    }
}
void ViewGL::stop()
{
    qDebug() << "Disconnect";
    disconnect(m_window, SIGNAL(afterRendering()),this,SLOT(renderGL()));
    qDebug() << "Stop timer"<<mTimer.isActive()<<mTimer.timerId();
    mTimer.stop();
    m_isStop=true;
    openGLVisible=false;
    // cleanUp();
}
void ViewGL::onApplicationStateChanged(Qt::ApplicationState state)
{

    qDebug() << "void ViewGL::onApplicationStateChanged(Qt::ApplicationState state =" << state << ")";
    if (Qt::ApplicationActive == state)
    {
        if (!m_wasStop)
        {
            qDebug() << "Start timer"<<mTimer.isActive()<<mTimer.timerId();
            mTimer.start(1000.f / 60.0f , this);
            m_isStop=false;
            // m_offsetY=0;
            openGLVisible=true;
        }
    }
    else
    {
        if (!m_isStop)
        {
            qDebug() << "Stop timer"<<mTimer.isActive()<<mTimer.timerId();
            mTimer.stop();
            m_isStop=true;
            m_wasStop=false;
            openGLVisible=false;
        }



    }

    //   writeSettings();
}



void ViewGL::cleanUp()
{

    openGLVisible=false;


    if(m_triangles!=nullptr)
    {
        delete m_triangles;
        m_triangles=nullptr;
    }

    if(m_textureTriangles[0]!=nullptr)
    {




        // m_textureTriangles[0]->destroyTexture(m_window->openglContext());



        delete m_textureTriangles[0];



        m_textureTriangles[0]=nullptr;

    }

    if(m_textureTriangles[1]!=nullptr)
    {
        delete m_textureTriangles[1];
        m_textureTriangles[1]=nullptr;
    }
    if(m_textureTriangles[2]!=nullptr)
    {
        delete m_textureTriangles[2];
        m_textureTriangles[2]=nullptr;
    }

    if(m_textureStaticTriangles!=nullptr)
    {
        delete m_textureStaticTriangles;
        m_textureStaticTriangles=nullptr;
    }




    if(m_textureDynamicTriangles[0]!=nullptr)
    {
        delete m_textureDynamicTriangles[0];
        m_textureDynamicTriangles[0]=nullptr;
    }

    if(m_textureDynamicTriangles[1]!=nullptr)
    {
        delete m_textureDynamicTriangles[1];
        m_textureDynamicTriangles[1]=nullptr;
    }

    if(m_lines!=nullptr)
    {
        delete m_lines;
        m_lines=nullptr;
    }
    if(m_sphere1!=nullptr)
    {
        delete m_sphere1;
        m_sphere1=nullptr;
    }

    if(m_sphere2!=nullptr)
    {
        delete m_sphere2;
        m_sphere2=nullptr;
    }


    if(m_sphere3!=nullptr)
    {
        delete m_sphere3;
        m_sphere3=nullptr;
    }

    if (m_fireball1!=nullptr)
    {
        delete m_fireball1;
        m_fireball1=nullptr;
    }

    if(m_water!=nullptr)
    {
        delete m_water;
        m_water=nullptr;
    }

    if (myContactListenerInstance!=nullptr)
    {
        delete myContactListenerInstance;
        myContactListenerInstance=nullptr;
    }

    if (m_world!=nullptr)
    {
        delete m_world;
        m_world=nullptr;
    }

    qDebug() << "END";
}


void   ViewGL::getBodyAABB(b2AABB& aabb,b2Body* b,bool transformed)
{
    aabb.lowerBound = b2Vec2(FLT_MAX,FLT_MAX);
    aabb.upperBound = b2Vec2(-FLT_MAX,-FLT_MAX);

    b2Fixture* fixture = b->GetFixtureList();
    b2Transform t;

    if (transformed)
        t = b->GetTransform();
    else
        t.SetIdentity();


    while (fixture != nullptr)
    {


        const b2Shape *shape = fixture->GetShape();
        const int childCount = shape->GetChildCount();
        for (int32 child = 0; child < childCount; ++child) {

            if (shape->GetType()!=b2Shape::e_circle)
            {
                b2AABB shapeAABB;
                //const b2Transform& t = b->GetTransform();



                shape->ComputeAABB(&shapeAABB, t, child);
                // shapeAABB.lowerBound = shapeAABB.lowerBound + r;
                //  shapeAABB.upperBound = shapeAABB.upperBound - r;
                //qDebug() << "shapeAABB:"<<shapeAABB.lowerBound.x<<shapeAABB.lowerBound.y<<"--"<<shapeAABB.upperBound.x<<shapeAABB.upperBound.y;


                aabb.Combine(shapeAABB);
            }
        }
        fixture = fixture->GetNext();
    }






}
void   ViewGL::getWorldAABB(b2AABB& aabbWorld)
{
    if (m_world!=nullptr)
    {


        aabbWorld.lowerBound = b2Vec2(FLT_MAX,FLT_MAX);
        aabbWorld.upperBound = b2Vec2(-FLT_MAX,-FLT_MAX);


        b2AABB aabb;

        b2Body* m_bodyList=m_world->GetBodyList();

        for (b2Body* b = m_bodyList; b; b = b->GetNext())
        {



            getBodyAABB(aabb,b,true);

            aabbWorld.Combine(aabb);
        }

    }
    else qDebug() << "World is null!!!!!!!";



}



bool ViewGL::getOpenGLVisible() const
{
    return openGLVisible;
}

void ViewGL::setOpenGLVisible(bool value)
{
    openGLVisible = value;
}
// triangulate a b2ChainShape
void ViewGL::triangulate(b2ChainShape* chain, const b2Transform* xf, short indexTexture)
{
    // if (fix->GetType()==b2Shape::e_chain)
    // {


    // qDebug() << "Triangulation start!!";
    /*
    int ms=1000;

            struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
               nanosleep(&ts, NULL);
*/

    //  b2ChainShape* chain = (b2ChainShape*)fix->GetShape();
    int32 count = chain->m_count;
    const b2Vec2* vertices = chain->m_vertices;

    float* listVertex=new float[2*count];
    int32 i=0,c=0;
    // const b2Transform& xf = fix->GetBody()->GetTransform();
    while (i<count)
    {
        b2Vec2 verttr =b2Mul(*xf, vertices[i]);
        listVertex[c]=verttr.x;
        listVertex[c+1]=verttr.y;
        i++;
        c+=2;
    }


    //*******************

    int j;
    TESSalloc ma;
    TESStesselator* tess = 0;
    const int nvp = 2*count;
    unsigned char* vflags = 0;
    //      int nvflags = 0;




    int allocated = 0;
    memset(&ma, 0, sizeof(ma));
    ma.memalloc = stdAlloc;
    ma.memfree = stdFree;
    ma.userData = (void*)&allocated;
    ma.extraVertices = 256; // realloc not provided, allow 256 extra vertices.

    tess = tessNewTess(&ma);
    if (tess)
    {
        tessAddContour(tess, 2, listVertex, sizeof(float)*2, count);
        //  tessAddContour(tess, 2, win1, sizeof(float)*2, 3);
        if (tessTesselate(tess, TESS_WINDING_POSITIVE, TESS_POLYGONS, nvp, 2, 0))
        {
            // qDebug()<<"Memory used for tesselation: %.1f kB\n"<< allocated/1024.0f;


            // Draw tesselated pieces.
            if (tess)
            {
                const float* verts = tessGetVertices(tess);
                //  const int* vinds = tessGetVertexIndices(tess);
                const int* elems = tessGetElements(tess);
                // const int nverts = tessGetVertexCount(tess);
                const int nelems = tessGetElementCount(tess);
                int start=0;
                // Draw polygons.
                // glColor4ub(255,255,255,128);
                //    qDebug() << "Numero triangoli fan:" <<  nelems;

                for (i = 0; i < nelems; ++i)
                {

                    const int* p = &elems[i*nvp];

                    //   qDebug() << "GL_TRIANGLE_FAN";
                    //      qDebug() << "Fan 0" <<verts[p[start]*2] << verts[p[start]*2+1];


                    for (j = 1; j < nvp && p[j+1] != TESS_UNDEF; ++j)
                    {

                        //  qDebug() << "Fan:" << verts[p[j]*2] << verts[p[j]*2+1];



                        m_verticesTexture[m_nVertTexture[indexTexture]++][indexTexture]=b2Vec2(verts[p[start]*2],verts[p[start]*2+1]);
                        //   qDebug() << "T1:" << m_verticesTexture[m_nVertTexture-1].x<< m_verticesTexture[m_nVertTexture-1].y;
                        m_verticesTexture[m_nVertTexture[indexTexture]++][indexTexture]=b2Vec2(verts[p[j]*2],verts[p[j]*2+1]);
                        // qDebug() << "T2:" << m_verticesTexture[m_nVertTexture-1].x <<m_verticesTexture[m_nVertTexture-1].y;  ;
                        m_verticesTexture[m_nVertTexture[indexTexture]++][indexTexture]=b2Vec2(verts[p[j+1]*2],verts[p[j+1]*2+1]);
                        // qDebug() << "T3:" << m_verticesTexture[m_nVertTexture-1].x<<m_verticesTexture[m_nVertTexture-1].y;;


                    }


                    start=0;

                    //glVertex2f(verts[p[j]*2], verts[p[j]*2+1]);
                    //  glEnd();
                }

                //   qDebug() << "vertici totali: "<<m_nVertTexture[indexTexture];

                /*
                    // glColor4ub(0,0,0,16);
                    for (i = 0; i < nelems; ++i)
                    {
                        const int* p = &elems[i*nvp];
                        // glBegin(GL_LINE_LOOP);
                        qDebug() << "GL_LINE_LOOP";
                        for (j = 0; j < nvp && p[j] != TESS_UNDEF; ++j)
                            qDebug() << verts[p[j]*2]<< verts[p[j]*2+1];
                        //  glVertex2f(verts[p[j]*2], verts[p[j]*2+1]);
                        //glEnd();
                    }

                    //  glColor4ub(0,0,0,128);
                    //  glPointSize(3.0f);
                    //  glBegin(GL_POINTS);
                    qDebug() << "GL_POINTS";
                    for (i = 0; i < nverts; ++i)
                    {
                        if (vflags && vflags[vinds[i]])
                            //  glColor4ub(255,0,0,192);
                            qDebug() <<"**" << verts[i*2]<< verts[i*2+1];
                        else
                            qDebug() <<"##" << verts[i*2]<<verts[i*2+1];
                        //glColor4ub(0,0,0,128);
                        //glVertex2f(verts[i*2], verts[i*2+1]);
                    }
                    */
                //glEnd();
                // glPointSize(1.0f);
            }

        }
        if (tess) tessDeleteTess(tess);
        if (vflags)
            free(vflags);
    }




    //*******************
    delete[] listVertex;
    //  qDebug() << "Triangulation end!!";
    //   QThread::msleep(1000);
    // nanosleep(&ts, NULL);

    //  }
}

bool  ViewGL::Initialize(QString nameScheme)
{

    m_breakBody=false;
    m_isPinLeftDestroyed=false;
    m_isPinRightDestroyed=false;
    isStatusTilt=false;

    /*
    QQuickView view;
    view.setSource(QUrl::fromLocalFile(url.path().append("/sounds/iron-bar.wav")));
    //view.setSource(QUrl::fromLocalFile("qml/pages/FirstPage.qml"));
    view.show();
    QObject *object = view.rootObject();
    object->setProperty("isTilt", false);
*/


    // QUrl url;
    b2dJson json;

    std::string errorMsg;


    if (m_world!=nullptr)
        delete m_world;

    m_world = json.readFromFile(dataUrl.path().append("/").append(nameScheme).toUtf8(), errorMsg );

    if ( m_world ) {
        qDebug() << "#GO! GO! GO! GO! GO! GO! GO! GO! ";
    }
    else
    {
        qDebug() <<  QString::fromUtf8(errorMsg.c_str());
        return false;
    }
    m_countPinLeft=0;
    m_countPinRight=0;



    //    //in FooTest constructor
    //    if (myContactListenerInstance!=nullptr)
    //    {
    //        delete myContactListenerInstance;
    //        myContactListenerInstance=nullptr;
    //    }


    m_world->SetContactListener(myContactListenerInstance);
    /*
    bodySphere1=addSphere(17,80,1.0);
    bodySphere2=addSphere(10,70,1.0);
    bodySphere3=addSphere(13,75,1.0);
*/
    // bodySphere1=addSphere(-8,45,1.0,5.0);


    //    bodySphere1->GetFixtureList()->SetDensity(5.0);
    //    bodySphere2->GetFixtureList()->SetDensity(0.5);
    //    bodySphere3->GetFixtureList()->SetDensity(1.0);

    //   addRect(300,500,1.1,1.1,true);
    std::vector<b2Joint*> f;

    json.getJointsByName("flip_left", f);

    leftFlipper=nullptr;
    if (f.size()>0)
    {
        leftFlipper = (b2RevoluteJoint*)f[0];
        leftFlipper->SetMotorSpeed(-9.8);
        f.clear();

    }

    // if (f.size()>0)
    //{

    json.getJointsByName("flip_right", f);
    rightFlipper=nullptr;
    if (f.size()>0)
    {
        rightFlipper = (b2RevoluteJoint*)f[0];
        rightFlipper->SetMotorSpeed(9.8);

        f.clear();

    }

    // if (f.size()>0)
    //  {

    json.getJointsByName("pinFold", f);
    m_pinFold=nullptr;
    if (f.size()>0)
    {
        m_pinFold = (b2RevoluteJoint*) f[0];

        f.clear();
    }
    //}
    fireBall1=json.getBodyByName("fireball1");

    // set deformable body
    m_nVertTexture[0]=0;
    m_nVertTexture[1]=0;
    m_nVertTexture[2]=0;
    b2Body *ironleft=nullptr;
    b2Body *ironright=nullptr;
    b2Body *test=nullptr;


    //   leftFlipper  = json.getFixtureByName("leftFlipperButton");
    //  rightFlipper = json.getFixtureByName("rightFlipperButton");
    // b2PolygonShape* poly1=NULL,*poly2=NULL;
    // if(leftFli!=NULL)
    //   poly1 = (b2PolygonShape*)leftFli->GetShape();
    // if(rightFli!=NULL)
    //   poly2 = (b2PolygonShape*)rightFli->GetShape();

    //   fireBall1=json.getBodyByName("body_iron_left");

    ironleft=json.getBodyByName("body_iron_left");
    if (ironleft!=nullptr)
    {
        Resource* resBall1 = new Resource;
        resBall1->type=iron_border;
        resBall1->data=nullptr;
        resBall1->index_iron=1;
        ironleft->SetUserData(resBall1);
        if (ironleft->GetFixtureList()[0].GetType()==b2Shape::e_chain) // not need
        {

            triangulate((b2ChainShape*)ironleft->GetFixtureList()[0].GetShape(),&ironleft->GetTransform(),0);
        }
    }


    ironright=json.getBodyByName("body_iron_right");
    if (ironright!=nullptr)
    {

        Resource* resBall2 = new Resource;

        resBall2->type=iron_border;
        resBall2->data=nullptr;
        resBall2->index_iron=2;

        ironright->SetUserData(resBall2);
        if (ironright->GetFixtureList()[0].GetType()==b2Shape::e_chain) // not need
            triangulate((b2ChainShape*)ironright->GetFixtureList()[0].GetShape(),&ironright->GetTransform(),1);
    }










    test=json.getBodyByName("iron_rust");

    if (test!=nullptr)
    {

        Resource* resBall3 = new Resource;





        resBall3->type=iron_border;
        resBall3->data=nullptr;
        resBall3->index_iron=3;

        test->SetUserData(resBall3);
        b2Fixture* fixtureList=test->GetFixtureList();

        for (b2Fixture* f= fixtureList; f; f=f->GetNext())
        {

            triangulate((b2ChainShape*) f->GetShape(),&test->GetTransform(),2);

        }
        //        b2AABB box=test->GetFixtureList()->GetAABB(0);
        //      qDebug() << "pixel box:"<< box.lowerBound.x<<box.lowerBound.y<<box.upperBound.x<<box.upperBound.y;
    }

    //  short index1=((Resource*)ironleft->GetUserData())->index_iron;
    //   short index2=((Resource*)ironright->GetUserData())->index_iron;
    //   short index3=((Resource*)test->GetUserData())->index_iron;

    //   qDebug() << index1<<index2<<index3;


   // m_world->SetGravity(b2Vec2(0,0));

    //  b2BodyDef edgedef;
    //  edgedef.position.Set(0.f,10.f);
    //b2Body* edge = m_world->CreateBody(&edgedef);

    //   b2Vec2 vertices[2];
    //   vertices[0].Set(0.0f, 0.0f);
    //  vertices[1].Set(10.0f, 0.0f);
    //int32 count = 2;



    //    b2Body *body=nullptr;
    QString nameglass;
    QString namewood;
    b2Body *body_unbreak=nullptr;

    for (int un=1;un<=10;un++)
    {

        nameglass=QString("dynamic_glass_%1").arg(un);
        namewood=QString("dynamic_wood_%1").arg(un);
        f.clear();
        BodyType typemat=glass_dynamic_body;
        body_unbreak=json.getBodyByName(nameglass.toStdString());
        if(body_unbreak==nullptr)
        {
            body_unbreak=json.getBodyByName(namewood.toStdString());
            typemat=wood_dynamic_body;
        }
        qDebug() << nameglass <<namewood <<typemat<<un;
        if(body_unbreak!=nullptr)
        {
            b2AABB aabb;

            getBodyAABB(aabb,body_unbreak,false);



            qDebug() <<"Lower Bound:" << aabb.lowerBound.x<<aabb.lowerBound.y;
            b2Vec2 *uvRef=new b2Vec2(aabb.lowerBound.x, aabb.lowerBound.y);
            Resource* resDynamic = new Resource;
            resDynamic->data=(float32*)uvRef;
            resDynamic->index_iron=0;

            resDynamic->type=typemat;


            b2MassData massData;
            body_unbreak->GetMassData(&massData);


            qDebug() << "mass"<<massData.mass<<resDynamic->type;


            body_unbreak->SetUserData(resDynamic);


        }
    }

    /*
for (b2Fixture* f = test->GetFixtureList(); f; f = f->GetNext())
{
    if (f->GetType()== b2Shape::e_chain)
    {
        GLint len=f->GetShape()->GetChildCount();
        qDebug()<<"len="<< len;
        GLdouble* data= new GLdouble[3*len*sizeof(GLdouble)];
        GLint* data_out= new GLint[len*sizeof(GLint)];

        convertLoopToPolygon(f, data,len);
        GLint len_out=0;
        int i;
        for ( i = 0; i<len; i++)
        {
            qDebug() << "("<< data[i*3]<<","<<data[1+i*3]<<","<<data[2+i*3]<<")";
        }

        TesselatorTest(data, len,data_out,&len_out);



        qDebug() << "OUT";

        for ( i = 0; i<len_out; i++)
        {
            qDebug() << "("<< data_out[i*3]<<","<<data_out[1+i*3]<<","<<data_out[2+i*3]<<")";
        }

        //DrawSolidPolygon(vertices, vertexCount, color);
        delete data_out;
        delete data;
    }
}

*/


    /*
    int i;
    b2Vec2 vertice;
    for (i=0;i<poly1->GetVertexCount();i++)


    {
        vertice = b2Mul(leftFli->GetBody()->GetTransform(), poly1->m_vertices[i]);
        qDebug() <<  vertice.x << vertice.y;
    }
*/
    /*
poly = (b2PolygonShape*)rightFli->GetShape();

for (i=0;i<poly->GetVertexCount();i++)
{
    vertice = b2Mul(rightFli->GetBody()->GetTransform(), poly->m_vertices[i]);
     qDebug() <<  vertice.x << vertice.y;
}

  */
    angle=0;






    openGLVisible=false;



    return true;
}

ViewGL::~ViewGL()
{
    qDebug()<<"Bye bye!! cruel world";
    //m_object.destroy();

    //   delete m_programMoveSphere;
    // qDebug() <<m_window->openglContext();
    //m_window->openglContext()->doneCurrent();



    cleanUp();


}

void
ViewGL::timerEvent(QTimerEvent *event)
{

    // Ideally you should stop the timer whenever the window is
    // minimized / hidden or the screen is turned off.
    //  world->Step(1.0f/60.0f,8,3);
    //    qDebug() << QDateTime::currentDateTime().toString();


    m_world->Step(1.0/60.0,8,3);


    if (m_needModScene[0])
        scheduledForModify(0);

    if (m_needModScene[1])
        scheduledForModify(1);

    if (m_needModScene[2])
        scheduledForModify(2);




    if (bodyToBreak!=nullptr)
    {


        ((Resource*) bodyToBreak->GetUserData())->type=glass_dynamic_body_unbreakable;

        breakBody(bodyToBreak);

        m_world->DestroyBody(bodyToBreak);
        bodyToBreak=nullptr;
        m_isDestroying=false;
    }




    if (leftFlipper!=nullptr)
        if (m_isPinLeftDestroyed)
        {
            b2Body* body1=leftFlipper->GetBodyB();


            m_world->DestroyJoint( leftFlipper );
            leftFlipper=nullptr;

            if (!isStatusTilt)
            {
            body1->SetLinearVelocity(b2Vec2(numRand(0,10),numRand(0,100)));
            body1->SetAngularVelocity(numRand(0,20));
            }

        }
    if (rightFlipper!=nullptr)
        if (m_isPinRightDestroyed)
        {
            b2Body* body1=rightFlipper->GetBodyB();

            m_world->DestroyJoint( rightFlipper );
            rightFlipper=nullptr;
            if (!isStatusTilt)
            {
            body1->SetLinearVelocity(b2Vec2(-1*numRand(0,10),numRand(0,100)));
            body1->SetAngularVelocity(numRand(0,20));
            }

        }


    /*
    if (m_breakBody)
{
    if (m_body_test!=nullptr)
    {
    breakBody(m_body_test);
    m_world->DestroyBody(m_body_test);
    m_body_test=NULL;
    }
 }
 */
    //b2Body *test=NULL;
    if (!m_praticing)
    {
        if (timerdead.elapsed()/1000)
        {

            if (m_sphere1->m_status==GLRenderSphere::underwater)
            {
                if (bodySphere2==nullptr && bodySphere3==nullptr) // is last qball

                    m_sphere1Alfa-=0.1;
                else
                    m_sphere1Alfa-=0.01;

                if (m_sphere1Alfa<=0)
                {
                    m_world->DestroyBody( bodySphere1 );
                    bodySphere1=nullptr;
                    m_sphere1Alfa=1;
                    m_sphere1->m_status=GLRenderSphere::normal;
                    sound10.play();
                }


            }
            if (m_sphere2->m_status==GLRenderSphere::underwater)
            {
                if (bodySphere1==nullptr && bodySphere3==nullptr) // is last qball

                    m_sphere2Alfa-=0.1;
                else
                    m_sphere2Alfa-=0.01;

                if (m_sphere2Alfa<=0)
                {
                    m_world->DestroyBody( bodySphere2 );
                    bodySphere2=nullptr;
                    m_sphere2Alfa=1;
                    m_sphere2->m_status=GLRenderSphere::normal;
                    sound10.play();
                }

            }
            if (m_sphere3->m_status==GLRenderSphere::underwater)
            {
                if (bodySphere1==nullptr && bodySphere2==nullptr) // is last qball

                    m_sphere3Alfa-=0.1;
                else
                    m_sphere3Alfa-=0.01;

                if (m_sphere3Alfa<=0)
                {
                    m_world->DestroyBody( bodySphere3 );
                    bodySphere3=nullptr;
                    m_sphere3Alfa=1;
                    m_sphere3->m_status=GLRenderSphere::normal;
                    sound10.play();
                }

            }
        }
    }
    else // I've been practicing ball restart
    {

        b2Vec2 pos;
        if (bodySphere1!=nullptr)
        {
            pos=bodySphere1->GetPosition();
            if (pos.y<-2)
            {
                qDebug() << "time1!!";
                m_sphere1->m_status=GLRenderSphere::normal;

                bodySphere1->SetTransform(b2Vec2(5,70), 0);
                bodySphere1->SetLinearVelocity(b2Vec2(0,0));

            }
        }
        if (bodySphere3!=nullptr)
        {
            pos=bodySphere2->GetPosition();
            if (pos.y<-2)
            {
                qDebug() << "time2!!";
                m_sphere2->m_status=GLRenderSphere::normal;

                bodySphere2->SetTransform(b2Vec2(-5,70), 0);
                bodySphere2->SetLinearVelocity(b2Vec2(0,0));
            }
        }
        if (bodySphere3!=nullptr)
        {
            pos=bodySphere3->GetPosition();
            if (pos.y<-2.0)

            {
                qDebug() << "time3!!";
                m_sphere3->m_status=GLRenderSphere::normal;

                bodySphere3->SetTransform(b2Vec2(7,70), 0);
                bodySphere3->SetLinearVelocity(b2Vec2(0,0));
            }


        }
    }

    if (m_fireball1->m_status==GLAnimateBall::underwater)
    {
        m_timeliveFireBall-=0.01;
        if (m_timeliveFireBall<=0 && m_controller->status()!="tilt")
        {
            sound8.play();
            m_controller->setStatus("levelok");
            openGLVisible=false;

            pause();
        }

    }

    if (bodySphere1==nullptr && bodySphere2==nullptr && bodySphere3==nullptr)
    {

        openGLVisible=false;
        m_controller->setStatus("failed");
        pause();


    }

    m_window->update();

}

void
ViewGL::renderGL()
{

    /*
            QUrl qmlpath=SailfishApp::pathTo("qml");

            QUrl strurl=qmlpath.path().append("/harbour-qball.qml");


            qDebug() << strurl;
            QQmlEngine engine;
            QQmlComponent component(&engine, strurl);
           m_object = component.create();

           qDebug() << m_object->objectName();

         //  qDebug() << "Property value Y:" << QQmlProperty::read(m_object, "myvalue").toInt();
          if (m_object!=nullptr)
          {
              qDebug() << m_object->objectName();
            qDebug() << "Property value:" << m_object->property("myvalue").toInt();
        }
        */

    //  qDebug()<< "vers::::"<<glGetString(GL_SHADING_LANGUAGE_VERSION);
    //   printf("OpenGL version supported by this platform (%s): \n", glGetString(GL_VERSION));
    //if (m_triangles->m_program)

    if (openGLVisible)
    {
        float32 offsetY=m_offsetY;
        // start at top
        /*
                if (bodySphere1->GetWorldCenter().y<-15)
                {
                    m_sphere1->m_status=GLRenderSphere::normal;

                    bodySphere1->SetTransform(b2Vec2(5,70), 0);
                    bodySphere1->SetLinearVelocity(b2Vec2(0,0));
                }
                if (bodySphere2->GetWorldCenter().y<-15)
                {
                    m_sphere2->m_status=GLRenderSphere::normal;

                    bodySphere2->SetTransform(b2Vec2(-5,70), 0);
                    bodySphere2->SetLinearVelocity(b2Vec2(0,0));
                }
                if (bodySphere3->GetWorldCenter().y<-15)
                {
                    m_sphere3->m_status=GLRenderSphere::normal;

                    bodySphere3->SetTransform(b2Vec2(7,70), 0);
                    bodySphere3->SetLinearVelocity(b2Vec2(0,0));
                }
                */

        //   qDebug() << "step1";

        /*
                if (fireBall1!=NULL)
                    if (fireBall1->GetWorldCenter().y<-5)
                    {
                        // you win!!!!!!!!!

                        sound8.play();
                        fireBall1->SetTransform(b2Vec2(19,70), 0);
                        openGLVisible=false;
                        endLivel=true;
                        pause();


                    }
        */

        if (m_sphere1->m_status==GLRenderSphere::splash)
        {
            sound6.play();
            m_water->Splash(bodySphere1->GetWorldCenter().x,-bodySphere1->GetLinearVelocity().y*0.01f);
            // bodySphere1->SetGravityScale(0.1);
            // bodySphere1->SetLinearVelocity(b2Vec2(0,-bodySphere1->GetLinearVelocity().y*0.1f));
            m_sphere1->m_status=GLRenderSphere::underwater;
            timerdead.start();


        }
        if (m_sphere2->m_status==GLRenderSphere::splash)
        {
            sound6.play();
            //    m_water->Splash(bodySphere2->GetWorldCenter().x,-10.0f);
            m_water->Splash(bodySphere2->GetWorldCenter().x,-bodySphere2->GetLinearVelocity().y*0.01f);
            //  bodySphere2->SetGravityScale(0.1);
            //  bodySphere2->SetLinearVelocity(b2Vec2(0,-bodySphere2->GetLinearVelocity().y*0.1f));
            m_sphere2->m_status=GLRenderSphere::underwater;

            timerdead.start();
        }
        if (m_sphere3->m_status==GLRenderSphere::splash)
        {
            sound6.play();
            // m_water->Splash(bodySphere3->GetWorldCenter().x,-10.0f);
            m_water->Splash(bodySphere3->GetWorldCenter().x,-bodySphere3->GetLinearVelocity().y*0.01f);
            // bodySphere3->SetGravityScale(0.1);
            //    bodySphere3->SetLinearVelocity(b2Vec2(0,-bodySphere3->GetLinearVelocity().y*0.1f));
            m_sphere3->m_status=GLRenderSphere::underwater;
            timerdead.start();
        }



        if (m_fireball1!=nullptr)
        {
            if (m_fireball1->m_status==GLAnimateBall::splash)
            {
                sound7.play();
                qDebug() << "Fireball spenta!!!";
                fireBall1->SetGravityScale(1.0f);
                // m_water->Splash(bodySphere3->GetWorldCenter().x,-10.0f);
                m_water->Splash(fireBall1->GetWorldCenter().x,-fireBall1->GetLinearVelocity().y*0.01f);
                // bodySphere3->SetGravityScale(0.1);
                fireBall1->SetLinearVelocity(b2Vec2(0,-fireBall1->GetLinearVelocity().y*0.1f));
                m_fireball1->m_status=GLAnimateBall::underwater;

            }
        }


        float32 userDataSphere1[7];
        float32 userDataSphere2[7];
        float32 userDataSphere3[7];


        if (!m_triangles->getShaderProgram()) {


            printVersionInformation();

            //           m_context=m_window->openglContext();
            //           connect(m_context, SIGNAL(aboutToBeDestroyed()),
            //                   this, SLOT(deleteContext()));

            qDebug() << "width:"<<  m_window->width() << "height:" <<  m_window->height();
            // get  boundig box of world
            // b2AABB aabb;
            getWorldAABB(m_world_dimension);

            qDebug() << "World dimension:"<<m_world_dimension.lowerBound.x<<m_world_dimension.lowerBound.y<<"--"<<m_world_dimension.upperBound.x<<m_world_dimension.upperBound.y;
            double scalaX=m_window->width()/(m_world_dimension.upperBound.x-m_world_dimension.lowerBound.x);
            double scalaY=m_window->height()/(m_world_dimension.upperBound.y-m_world_dimension.lowerBound.y);
            qDebug() << "scalaX:"<< scalaX;
            qDebug() << "scalaY:"<< scalaY;
            m_rateWorldScreen=qMin(scalaX,scalaY);
            m_ratex=m_rateWorldScreen/m_window->width();
            m_ratey=m_rateWorldScreen/m_window->height();



            QString img1=dataUrl.path().append("/images/").append("Iron_rust1.jpg");
            m_triangles->Create(m_window->width(),m_window->height(),m_rateWorldScreen,m_world_dimension.upperBound.x);
            m_lines->Create(m_window->width()+2,m_window->height(),m_rateWorldScreen,m_world_dimension.upperBound.x);
            m_water->Create(m_window->width(),m_window->height(),m_rateWorldScreen,dataUrl,m_world_dimension.upperBound.x);

            m_textureTriangles[0]->Create(m_window->width(),m_window->height(),m_rateWorldScreen,m_world_dimension.upperBound.x,img1,1.0);
            m_textureTriangles[1]->Create(m_window->width(),m_window->height(),m_rateWorldScreen,m_world_dimension.upperBound.x,img1,1.0);

            m_textureTriangles[2]->Create(m_window->width(),m_window->height(),m_rateWorldScreen,m_world_dimension.upperBound.x,img1,1.0);



            QString img4=dataUrl.path().append("/images/").append("iron.jpg");
            m_textureStaticTriangles->Create(m_window->width(),m_window->height(),m_rateWorldScreen,m_world_dimension.upperBound.x,img4,1.0);
            QString img2=dataUrl.path().append("/images/").append("glass1.jpg");

            m_textureDynamicTriangles[0]->Create(m_window->width(),m_window->height(),m_rateWorldScreen,m_world_dimension.upperBound.x,img2,0.6);
            img2=dataUrl.path().append("/images/").append("wood.jpg");
            m_textureDynamicTriangles[1]->Create(m_window->width(),m_window->height(),m_rateWorldScreen,m_world_dimension.upperBound.x,img2,1);


            m_points->Create(m_window->width()+2,m_window->height(),m_rateWorldScreen,m_world_dimension.upperBound.x,dataUrl);

            //   m_points->Create(m_window->width(),m_window->height(),m_rateWorldScreen);
            if (fireBall1!=nullptr)
            {




                //           ((b2CircleShape*) fireBall1->GetFixtureList()->GetShape())->m_radius=2;


                m_fireball1->Create(((b2CircleShape*) fireBall1->GetFixtureList()->GetShape())->m_radius+0.3,m_window->width(),m_window->height(),dataUrl,m_rateWorldScreen,m_world_dimension.upperBound.x);
            }

            lightPos.setX(sin(m_horizonthal_light) * cos(m_vertical_light));
            lightPos.setY(sin(m_vertical_light));
            lightPos.setZ(cos(m_horizonthal_light) * cos(m_vertical_light));
            lightPos.setW(0.);

            m_sphere1->Create(m_window->width(),m_window->height(),getImageName(reinterpret_cast<QBall *>(m_dataList.at(m_controller->play1()))->texture()),m_rateWorldScreen,m_world_dimension.upperBound.x);

            m_sphere2->Create(m_window->width(),m_window->height(),getImageName(reinterpret_cast<QBall *>(m_dataList.at(m_controller->play2()))->texture()),m_rateWorldScreen,m_world_dimension.upperBound.x);

            m_sphere3->Create(m_window->width(),m_window->height(),getImageName(reinterpret_cast<QBall *>(m_dataList.at(m_controller->play3()))->texture()),m_rateWorldScreen,m_world_dimension.upperBound.x);

        }


        if (m_needRecreate1)
        {
            QOpenGLTexture * tex1=m_sphere1->texture();
            QString str =reinterpret_cast<QBall *>(m_dataList.at(m_controller->play1()))->texture();
            QImage image1=getImageName(str).mirrored(false,true);
            delete tex1;
            tex1  =  new QOpenGLTexture(image1);
            if (tex1!=nullptr)
                qDebug() << "Texture1 creata"<<reinterpret_cast<QBall *>(m_dataList.at((m_controller->play1())));
            else
                qDebug() << "Texture1 fails";


            QOpenGLTexture * tex2=m_sphere2->texture();
            str =reinterpret_cast<QBall *>(m_dataList.at(m_controller->play2()))->texture();
            QImage image2=getImageName(str).mirrored(false,true);
            delete tex2;
            tex2  =  new QOpenGLTexture(image2);

            QOpenGLTexture * tex3=m_sphere3->texture();
            str =reinterpret_cast<QBall *>(m_dataList.at(m_controller->play3()))->texture();
            QImage image3=getImageName(str).mirrored(false,true);
            delete tex3;
            tex3  =  new QOpenGLTexture(image3);
            m_needRecreate1=false;

        }

        DrawData(userDataSphere1,userDataSphere2,userDataSphere3);



        // m_points->Flush();


        if (bodySphere1!=nullptr)
            m_sphere1->Flush(userDataSphere1,bodySphere1->GetWorldCenter().x,bodySphere1->GetWorldCenter().y,reinterpret_cast<QBall *>(m_dataList.at(m_controller->play1()))->radius(),lightPos,m_sphere1Alfa,offsetY);
        if (bodySphere2!=nullptr)
            m_sphere2->Flush(userDataSphere2,bodySphere2->GetWorldCenter().x,bodySphere2->GetWorldCenter().y,reinterpret_cast<QBall *>(m_dataList.at(m_controller->play2()))->radius(),lightPos,m_sphere2Alfa,offsetY);
        if (bodySphere3!=nullptr)
            m_sphere3->Flush(userDataSphere3,bodySphere3->GetWorldCenter().x,bodySphere3->GetWorldCenter().y,reinterpret_cast<QBall *>(m_dataList.at(m_controller->play3()))->radius(),lightPos,m_sphere3Alfa,offsetY);


        if (fireBall1!=nullptr)
            m_fireball1->Flush(fireBall1->GetWorldCenter().x,fireBall1->GetWorldCenter().y,offsetY);
        m_points->Flush(offsetY);


        m_triangles->Flush(offsetY);
        m_lines->Flush(offsetY);




        m_textureTriangles[0]->Flush(offsetY);
        m_textureTriangles[1]->Flush(offsetY);
        m_textureTriangles[2]->Flush(offsetY);

        m_textureStaticTriangles->Flush(offsetY);

        m_textureDynamicTriangles[0]->Flush(offsetY);
        m_textureDynamicTriangles[1]->Flush(offsetY);

        if (!noWater)
            m_water->Flush(offsetY);

        /*
                   if(bodySphere1!=nullptr)
                     if (userData1!=nullptr)
                       flushSphere(userData1,texture1,m_program1);
        */





    }


}
QString ViewGL::pathCache() const
{
    return m_pathCache;
}

void ViewGL::setPathCache(const QString &pathCache)
{
    m_pathCache = pathCache;
}


void ViewGL::printVersionInformation()
{

    QString glVersion;
    QString glExtension;
    QString glProfile;

    // Get Version Information
    glVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));


    // qPrintable() will print our QString w/o quotes around it.
    qDebug() << qPrintable(glVersion) << "(" << qPrintable(glProfile) << ")";


    glExtension = reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
    qDebug() << "glExtension:" << qPrintable(glExtension) ;
}
// check if a body is make with IronRast
//
short ViewGL::isIronRust(b2Body* b)
{
    short retval=0;
    if (b->GetUserData()!=nullptr)
    {
        if (((Resource*)b->GetUserData())->type==iron_border)
        {

            retval = ((Resource*) b->GetUserData())->index_iron;
        }
    }
    return retval;
}

short ViewGL::bodyType(b2Body* b)
{
    short retval=0;
    if (b->GetUserData()!=nullptr)
    {


        retval = ((Resource*) b->GetUserData())->type;

    }
    return retval;
}


void ViewGL::DrawData(float32* userMyData1,float32* userMyData2,float32* userMyData3)
{
    b2Body* m_bodyList;
    Resource *userData;

    m_isDrawing=true;
    //  b2AABB aabbWorld;

    // getWorldAABB(aabbWorld);


    // qDebug() << "Start";


    //m_points->Vertex(b2Vec2(0,12),b2Color(1.0,0,0),2*m_rateWorldScreen);
    //m_points->Vertex(b2Vec2(10,12),b2Color(1.0,0,0));

    //m_points->Vertex(b2Vec2(10,12),b2Color(1.0,0,0));
    //m_points->Vertex(b2Vec2(-5,15),b2Color(1.0,1.0,0),m_rateWorldScreen*5);
    /*
        m_textureStaticTriangles->Vertex(b2Vec2(-10.4497, 38.6732),b2Vec2(m_ratex*-10.4497+0.5,m_ratey*38.6732));
        m_textureStaticTriangles->Vertex(b2Vec2(3.28364, 36.1318),b2Vec2(m_ratex*3.28364+0.5,m_ratey*36.1318));
        m_textureStaticTriangles->Vertex(b2Vec2(-4.48024, 27.811),b2Vec2(m_ratex*-4.48024+0.5,m_ratey*27.811));
        */
    //    qDebug() << "End";

    //     float32 lar=23.27,alt=23.27;
    /*
                    float val=1.;
                    // lar Real/larPixtexture*scala
                    m_textureStaticTriangles->Vertex(b2Vec2(0,12),b2Vec2(0,val));
                    m_textureStaticTriangles->Vertex(b2Vec2(10,12),b2Vec2(val,val));

                    m_textureStaticTriangles->Vertex(b2Vec2(0,0),b2Vec2(0,0 ));


                    m_textureStaticTriangles->Vertex(b2Vec2(0,0),b2Vec2(0,0));
                    m_textureStaticTriangles->Vertex(b2Vec2(10,12),b2Vec2(val,val));

                    m_textureStaticTriangles->Vertex(b2Vec2(10,0),b2Vec2(val,0));
        */
    /*
        if (m_body_test!=nullptr)
            if(m_body_test->GetPosition().y<9)
            {
                m_breakBody=true;

            }
        */
    // qDebug() << sizeof (Resource);
    //  qDebug() << "GetBodyCount!" << world->GetBodyCount();
    m_bodyList=m_world->GetBodyList();
    for (b2Body* b = m_bodyList; b; b = b->GetNext())
    {
        // if (b->GetUserData()!=NULL)
        // qDebug() << "tipo:" << ((Resource*)b->GetUserData())->type;



        short indiron=isIronRust(b)-1;
        if (indiron>=0)
        {



            if (needTriangulate[indiron])
            {

                b2Fixture* fixtureList=b->GetFixtureList();

                for (b2Fixture* f= fixtureList; f; f=f->GetNext())
                {
                    triangulate((b2ChainShape*) f->GetShape(),&b->GetTransform(),indiron);
                }
                qDebug() << "triangulate" << indiron;
                needTriangulate[indiron]=false;
            }


            //    qDebug() << indiron <<m_nVertTexture[indiron];
            for (int32 t = 0; t < m_nVertTexture[indiron]; ++t)
            {
                //m_triangles->Vertex(m_verticesTexture[t],b2Color(1.,t/(float)m_nVertTexture,t/(float)m_nVertTexture,0.3));


                //   b2Vec2 sc(m_world_dimension.upperBound.x,m_world_dimension.lowerBound.y);
                //qDebug() <<"min max" <<m_world_dimension.upperBound.y<<m_world_dimension.lowerBound.y;
                //  convertWorldToUVMap(sc);
                //  qDebug() <<"-16.4,-1.03" <<sc.x<<sc.y;

                // sc.Set(16.4,54.76);
                // convertWorldToUVMap( sc);
                // qDebug() <<"16.4,54.76:" <<sc.x<<sc.y;



                // b2Vec2 mapuv(m_verticesTexture[t].x,m_verticesTexture[t].y);

                //convert
                //  convertWorldToUVMap( );
                //   qDebug() << mapuv.x<<mapuv.y;

                m_textureTriangles[indiron]->Vertex(m_verticesTexture[t][indiron],b2Vec2(m_ratex*m_verticesTexture[t][indiron].x+0.5,m_ratey*m_verticesTexture[t][indiron].y));

                //   qDebug() << t<< "*T:"<< m_verticesTexture[t].x<<m_verticesTexture[t].y;

            }
            //qDebug() << "NumVertex:"<< m_nVertTexture;
        }


        if (bodySphere1==b)

        {

            b2CircleShape* c=(b2CircleShape*)b->GetFixtureList()->GetShape();
            userData=(Resource*)b->GetUserData();


            if(m_sphere1->m_status==GLRenderSphere::normal)
                drawSphere(b->GetWorldCenter() ,b->GetAngle(),c->m_radius,userData);


            userData->data[0]=b->GetWorldCenter().x;
            userData->data[1]=b->GetWorldCenter().y;
            userData->data[2]=b->GetAngle();

            memcpy(&userMyData1[0],((Resource*)b->GetUserData())->data,sizeof(float32)*7);

            if (!noWater)
            {
                if (b->GetWorldCenter().y<=m_water->getLevelWater(b->GetWorldCenter().x)+c->m_radius )
                {

                    if(m_sphere1->m_status==GLRenderSphere::normal)
                    {
                        m_sphere1->m_status=GLRenderSphere::splash;
                        qDebug() << "Splash Data sphere 1"<< b->GetWorldCenter().y;
                    }


                }
                else if (b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x)+2*c->m_radius &&
                         b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x-c->m_radius)+2*c->m_radius &&
                         b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x+c->m_radius)+2*c->m_radius)
                    m_sphere1->m_status=GLRenderSphere::normal;

                if (m_sphere1->m_status==GLRenderSphere::underwater)
                {
                    float displacedMass =  -c->m_radius*c->m_radius*M_PI;
                    b2Vec2 buoyancyForce = displacedMass*0.5 * m_world->GetGravity();
                    bodySphere1->ApplyForce( buoyancyForce, b->GetWorldCenter(),true );

                    // if (bodySphere1->GetLinearVelocity().y>0)
                    bodySphere1->SetLinearVelocity(b2Vec2(0,0));
                }
            }

        }
        else if (bodySphere2==b)

        {

            b2CircleShape* c=(b2CircleShape*)b->GetFixtureList()->GetShape();
            userData=(Resource*)b->GetUserData();
            if(m_sphere2->m_status==GLRenderSphere::normal)
                drawSphere(b->GetWorldCenter() ,b->GetAngle(),c->m_radius,userData);


            userData->data[0]=b->GetWorldCenter().x;
            userData->data[1]=b->GetWorldCenter().y;
            userData->data[2]=b->GetAngle();
            memcpy(&userMyData2[0],((Resource*)b->GetUserData())->data,sizeof(float32)*7);
            if (!noWater)
            {
                if (b->GetWorldCenter().y<=m_water->getLevelWater(b->GetWorldCenter().x)+c->m_radius )
                {
                    if(m_sphere2->m_status==GLRenderSphere::normal)
                    {
                        m_sphere2->m_status=GLRenderSphere::splash;
                        qDebug() << "Splash Data sphere 2"<< b->GetWorldCenter().y;
                    }


                }
                else if (b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x)+2*c->m_radius &&
                         b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x-c->m_radius)+2*c->m_radius &&
                         b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x+c->m_radius)+2*c->m_radius)
                    m_sphere2->m_status=GLRenderSphere::normal;

                if (m_sphere2->m_status==GLRenderSphere::underwater)
                {
                    float displacedMass =-c->m_radius*c->m_radius*M_PI;
                    b2Vec2 buoyancyForce = displacedMass *0.5* m_world->GetGravity();
                    bodySphere2->ApplyForce( buoyancyForce, b->GetWorldCenter(),true );
                    if (bodySphere2->GetLinearVelocity().y>0)
                        bodySphere2->SetLinearVelocity(b2Vec2(0,0));
                }
            }
        }
        else if (bodySphere3==b)

        {

            b2CircleShape* c=(b2CircleShape*)b->GetFixtureList()->GetShape();
            userData=(Resource*)b->GetUserData();
            if(m_sphere3->m_status==GLRenderSphere::normal)

                drawSphere(b->GetWorldCenter() ,b->GetAngle(),c->m_radius,userData);


            userData->data[0]=b->GetWorldCenter().x;
            userData->data[1]=b->GetWorldCenter().y;
            userData->data[2]=b->GetAngle();
            memcpy(&userMyData3[0],((Resource*)b->GetUserData())->data,sizeof(float32)*7);
            if (!noWater)
            {
                if (b->GetWorldCenter().y<=m_water->getLevelWater(b->GetWorldCenter().x) +c->m_radius)
                {

                    if(m_sphere3->m_status==GLRenderSphere::normal)
                    {
                        m_sphere3->m_status=GLRenderSphere::splash;
                        qDebug() << "Splash Data sphere 3"<< b->GetWorldCenter().y;
                    }

                }
                else if (b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x)+2*c->m_radius &&
                         b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x-c->m_radius)+2*c->m_radius &&
                         b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x+c->m_radius)+2*c->m_radius)
                {


                    m_sphere3->m_status=GLRenderSphere::normal;
                }
                if (m_sphere3->m_status==GLRenderSphere::underwater)
                {

                    float displacedMass =  - c->m_radius*c->m_radius*M_PI;
                    b2Vec2 buoyancyForce = displacedMass*0.5 * m_world->GetGravity();
                    bodySphere3->ApplyForce( buoyancyForce, b->GetWorldCenter(),true );

                    if (bodySphere3->GetLinearVelocity().y>0)
                        bodySphere3->SetLinearVelocity(b2Vec2(0,0));


                }
            }
        }
        //else if (fireBall1==b)
        //{
        // }
        else  if (!isIronRust(b))

        {
            const b2Transform& xf = b->GetTransform();
            for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext())
            {

                if (b->IsActive() == false)
                {
                    DrawShape(f, xf, b2Color(0.0f, 1.0f, 0.0f),true,false);
                }
                else if (b->GetType() == b2_staticBody)
                {

                    DrawShape(f, xf, b2Color(0.0f, 1.0f, 0.0f),true,false);//static
                    /*
                            bool isDeform=false;
                            if (b->GetUserData()!=NULL)
                            {
                                Resource *res=(Resource*)b->GetUserData();
                                // qDebug() << (*res).type;
                                if ((*res).type==iron_border)
                                    isDeform=true;
                            }
                            if (isDeform)
                            {
                                DrawShape(f, xf, b2Color(0.0f, 1.0f, 0.0f));

                            }
                            else
                                DrawShape(f, xf, b2Color(1.0f, 0.0f, 0.0f));
                                */

                }
                else if (b->GetType() == b2_kinematicBody)
                {
                    DrawShape(f, xf, b2Color(0.0f, 0.5f, 0.9f),true,false);
                }
                else if (b->IsAwake() == false)
                {
                    DrawShape(f, xf, b2Color(0.0f, 0.0f, 1.0f),true,false);
                }
                else
                {
                    if (fireBall1==b)
                    {
                        b2CircleShape* c=(b2CircleShape*)b->GetFixtureList()->GetShape();
                        if (b->GetWorldCenter().y<=m_water->getLevelWater(b->GetWorldCenter().x)+c->m_radius )
                        {

                            if(m_fireball1->m_status==GLAnimateBall::normal)
                            {
                                m_fireball1->m_status=GLAnimateBall::splash;
                                m_fireball1->fireOff=true;
                                qDebug() << "Splash Data sphere 1"<< b->GetWorldCenter().y;
                            }


                        }
                        else if (b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x)+2*c->m_radius &&
                                 b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x-c->m_radius)+2*c->m_radius &&
                                 b->GetWorldCenter().y> m_water->getLevelWater(b->GetWorldCenter().x+c->m_radius)+2*c->m_radius)
                            m_fireball1->m_status=GLAnimateBall::normal;

                        if (m_fireball1->m_status==GLAnimateBall::underwater)
                        {
                            float displacedMass = -b->GetFixtureList()->GetDensity() * c->m_radius*c->m_radius*M_PI;
                            b2Vec2 buoyancyForce = displacedMass * m_world->GetGravity();
                            fireBall1->ApplyForce( buoyancyForce, b->GetWorldCenter(),true );
                        }

                        if (m_fireball1->m_status==GLAnimateBall::underwater)
                            DrawShape(f, xf, b2Color(0.3f, 0.3f, 0.3f,0.5),true,true);
                        else
                            DrawShape(f, xf, b2Color(.3f, 0.3f, 0.3f,0.8),true,true);
                    }
                    else if (bodyType(b)==glass_dynamic_body || bodyType(b)==glass_dynamic_body_unbreakable)
                    {

                        b2PolygonShape* poly = (b2PolygonShape*)f->GetShape();
                        int32 vertexCount = poly->m_count;

                        b2Assert(vertexCount <= b2_maxPolygonVertices);
                        b2Vec2 vertices[b2_maxPolygonVertices];


                        for (int32 i = 0; i < vertexCount; ++i)
                        {
                            vertices[i] = b2Mul(xf, poly->m_vertices[i]);
                        }


                        for (int32 i = 1; i < vertexCount - 1; ++i)
                        {

                            b2Vec2 *tran=(b2Vec2*)((Resource*) b->GetUserData())->data;
                            float32 sc=0.1;

                            m_textureDynamicTriangles[0]->Vertex(vertices[0],  b2Vec2((poly->m_vertices[0].x-tran->x)*sc+0.0,(poly->m_vertices[0].y-tran->y)*sc));
                            m_textureDynamicTriangles[0]->Vertex(vertices[i],  b2Vec2((poly->m_vertices[i].x-tran->x)*sc+0.0,(poly->m_vertices[i].y-tran->y)*sc));
                            m_textureDynamicTriangles[0]->Vertex(vertices[i+1],b2Vec2((poly->m_vertices[i+1].x-tran->x)*sc+0.0,(poly->m_vertices[i+1].y-tran->y)*sc));

                        }



                    }
                    else if (bodyType(b)==wood_dynamic_body)
                    {
                        if (f->GetShape()->GetType()==b2Shape::e_polygon)
                        {
                            b2PolygonShape* poly = (b2PolygonShape*)f->GetShape();
                            int32 vertexCount = poly->m_count;

                            b2Assert(vertexCount <= b2_maxPolygonVertices);
                            b2Vec2 vertices[b2_maxPolygonVertices];


                            for (int32 i = 0; i < vertexCount; ++i)
                            {
                                vertices[i] = b2Mul(xf, poly->m_vertices[i]);
                            }


                            for (int32 i = 1; i < vertexCount - 1; ++i)
                            {

                                b2Vec2 *tran=(b2Vec2*)((Resource*) b->GetUserData())->data;
                                float32 sc=0.1;

                                m_textureDynamicTriangles[1]->Vertex(vertices[0],  b2Vec2((poly->m_vertices[0].x-tran->x)*sc+0.0,(poly->m_vertices[0].y-tran->y)*sc));
                                m_textureDynamicTriangles[1]->Vertex(vertices[i],  b2Vec2((poly->m_vertices[i].x-tran->x)*sc+0.0,(poly->m_vertices[i].y-tran->y)*sc));
                                m_textureDynamicTriangles[1]->Vertex(vertices[i+1],b2Vec2((poly->m_vertices[i+1].x-tran->x)*sc+0.0,(poly->m_vertices[i+1].y-tran->y)*sc));

                            }
                        }
                        else if (f->GetShape()->GetType()==b2Shape::e_circle)
                        {

                            b2CircleShape* circle = (b2CircleShape*)f->GetShape();


                            b2Vec2 center = b2Mul(xf, circle->m_p);
                            //                           if (isfire)
                            //                           {
                            //                               float32 radius = circle->m_radius;
                            //                               b2Vec2 axis = b2Mul(xf.q, b2Vec2(1.0f, 0.0f));

                            //                               DrawSolidCircle(center, radius, axis, color);
                            //                           }
                            //                           else
                            // qDebug() << center.x<<center.y << circle->m_radius;
                            m_points->Vertex(center,b2Color(0.7f, 0.7f, 0.7f,0.5f),b2Vec2(circle->m_radius*m_rateWorldScreen*2,-1.0*f->GetBody()->GetAngle()));

                        }
                    }
                    else
                    {
                        DrawShape(f, xf, b2Color(0.7f, 0.7f, 0.7f,0.5f),false,true);
                    }
                }
            }
        }
    }
    m_isDrawing=false;
}
void ViewGL::drawSphere(b2Vec2 center,float32 angle,float32 radius,Resource* resball)
{




    //if (angle!=0)
    //  qDebug()<<"angle:"<<angle;

    float32 deltaX,deltaY;

    deltaX=center.x-resball->data[0];
    deltaY=center.y-resball->data[1];


    //  qDebug()<<"DeltaX:"<<deltaX<<"DeltaY:"<<deltaY;

    float32 theta=atan2(deltaY,deltaX);

    float32 rotazionegradi=1./radius*sqrt(deltaX*deltaX+deltaY*deltaY)*180/M_PI;
    //float32 thetagradi=theta*180/M_PI;

    QQuaternion rot2,rot1,rotSum;
    rot1=QQuaternion ( cos((-angle+resball->data[2])/2.), 0, 0, sin((-angle+resball->data[2])/2.));


    QVector3D  axe(sin(theta),cos(theta),0);

    rot2=QQuaternion::fromAxisAndAngle(axe,rotazionegradi);
    rotSum=QQuaternion(resball->data[3],resball->data[4],resball->data[5],resball->data[6]);

    //  qDebug()<<"axe:"<<axe;
    // rotSum*=rot1*rot2;
    //  rotSum*=rot1;
    rotSum=rot1*rot2*rotSum;

    resball->data[3]=rotSum.scalar();
    resball->data[4]=rotSum.vector().x();
    resball->data[5]=rotSum.vector().y();
    resball->data[6]=rotSum.vector().z();

}

void ViewGL::DrawShape(b2Fixture* fixture, const b2Transform& xf, const b2Color& color,bool isstatic,bool isfire)
{
    switch (fixture->GetType())
    {
    case b2Shape::e_circle:
    {

        // qDebug()<< "e_circle";


        b2CircleShape* circle = (b2CircleShape*)fixture->GetShape();


        b2Vec2 center = b2Mul(xf, circle->m_p);
        if (isfire)
        {
            float32 radius = circle->m_radius;
            b2Vec2 axis = b2Mul(xf.q, b2Vec2(1.0f, 0.0f));

            DrawSolidCircle(center, radius, axis, color);
        }
        else
            // qDebug() << center.x<<center.y << circle->m_radius;
            m_points->Vertex(center,color,b2Vec2(circle->m_radius*m_rateWorldScreen*2,-1.0*fixture->GetBody()->GetAngle()));
    }
        break;

    case b2Shape::e_edge:
    {
        //qDebug()<< "e_edge";
        b2EdgeShape* edge = (b2EdgeShape*)fixture->GetShape();
        b2Vec2 v1 = b2Mul(xf, edge->m_vertex1);
        b2Vec2 v2 = b2Mul(xf, edge->m_vertex2);
        DrawSegment(v1, v2, color);
    }
        break;

    case b2Shape::e_chain:
    {
        //   qDebug()<< "e_chain";
        b2ChainShape* chain = (b2ChainShape*)fixture->GetShape();
        int32 count = chain->m_count;
        const b2Vec2* vertices = chain->m_vertices;

        b2Vec2 v1 = b2Mul(xf, vertices[0]);
        for (int32 i = 1; i < count; ++i)
        {
            b2Vec2 v2 = b2Mul(xf, vertices[i]);
            DrawSegment(v1, v2, color);
            DrawCircle(v1, 0.1f, color);
            v1 = v2;
        }
    }
        break;

    case b2Shape::e_polygon:
    {
        //qDebug()<< "e_polygon";
        b2PolygonShape* poly = (b2PolygonShape*)fixture->GetShape();
        int32 vertexCount = poly->m_count;
        b2Assert(vertexCount <= b2_maxPolygonVertices);
        b2Vec2 vertices[b2_maxPolygonVertices];

        for (int32 i = 0; i < vertexCount; ++i)
        {
            vertices[i] = b2Mul(xf, poly->m_vertices[i]);
        }
        if (isstatic)
            DrawStaticSolidPolygon(vertices, vertexCount);
        else
            DrawSolidPolygon(vertices, vertexCount,color);
    }
        break;

    default:
        break;
    }
}
void ViewGL::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
    b2Vec2 p1 = vertices[vertexCount - 1];
    for (int32 i = 0; i < vertexCount; ++i)
    {
        b2Vec2 p2 = vertices[i];
        m_lines->Vertex(p1, color);
        m_lines->Vertex(p2, color);
        p1 = p2;
    }
}
void ViewGL::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
    b2Color fillColor(0.5f * color.r, 0.5f * color.g, 0.5f * color.b,1.0);

    for (int32 i = 1; i < vertexCount - 1; ++i)
    {

        m_triangles->Vertex(vertices[0], fillColor);
        m_triangles->Vertex(vertices[i], fillColor);
        m_triangles->Vertex(vertices[i+1], fillColor);
    }

    b2Vec2 p1 = vertices[vertexCount - 1];
    for (int32 i = 0; i < vertexCount; ++i)
    {
        b2Vec2 p2 = vertices[i];
        m_lines->Vertex(p1, color);
        m_lines->Vertex(p2, color);
        p1 = p2;
    }

}

//
void ViewGL::DrawStaticSolidPolygon(const b2Vec2* vertices, int32 vertexCount)
{

    for (int32 i = 1; i < vertexCount - 1; ++i)
    {

        m_textureStaticTriangles->Vertex(vertices[0],b2Vec2(m_ratex*vertices[0].x+0.5,m_ratey*vertices[0].y));
        m_textureStaticTriangles->Vertex(vertices[i],b2Vec2(m_ratex*vertices[i].x+0.5,m_ratey*vertices[i].y));
        m_textureStaticTriangles->Vertex(vertices[i+1],b2Vec2(m_ratex*vertices[i+1].x+0.5,m_ratey*vertices[i+1].y));
        //  m_triangles->Vertex(vertices[0], fillColor);
        //  m_triangles->Vertex(vertices[i], fillColor);
        //  m_triangles->Vertex(vertices[i+1], fillColor);
    }


}

//
void ViewGL::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{
    const float32 k_segments = 16.0f;
    const float32 k_increment = 2.0f * b2_pi / k_segments;
    float32 sinInc = sinf(k_increment);
    float32 cosInc = cosf(k_increment);
    b2Vec2 r1(1.0f, 0.0f);
    b2Vec2 v1 = center + radius * r1;
    for (int32 i = 0; i < k_segments; ++i)
    {
        // Perform rotation to avoid additional trigonometry.
        b2Vec2 r2;
        r2.x = cosInc * r1.x - sinInc * r1.y;
        r2.y = sinInc * r1.x + cosInc * r1.y;
        b2Vec2 v2 = center + radius * r2;
        m_lines->Vertex(v1, color);
        m_lines->Vertex(v2, color);
        r1 = r2;
        v1 = v2;
    }
}

//
void ViewGL::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
{


    const float32 k_segments = 16.0f;
    const float32 k_increment = 2.0f * b2_pi / k_segments;
    float32 sinInc = sinf(k_increment);
    float32 cosInc = cosf(k_increment);
    b2Vec2 v0 = center;
    b2Vec2 r1(cosInc, sinInc);
    b2Vec2 v1 = center + radius * r1;
    b2Color fillColor(0.5f * color.r, 0.5f * color.g, 0.5f * color.b,color.a);
    for (int32 i = 0; i < k_segments; ++i)
    {
        // Perform rotation to avoid additional trigonometry.
        b2Vec2 r2;
        r2.x = cosInc * r1.x - sinInc * r1.y;
        r2.y = sinInc * r1.x + cosInc * r1.y;
        b2Vec2 v2 = center + radius * r2;
        m_triangles->Vertex(v0, fillColor);
        m_triangles->Vertex(v1, fillColor);
        m_triangles->Vertex(v2, fillColor);
        r1 = r2;
        v1 = v2;
    }

    r1.Set(1.0f, 0.0f);
    v1 = center + radius * r1;
    for (int32 i = 0; i < k_segments; ++i)
    {
        b2Vec2 r2;
        r2.x = cosInc * r1.x - sinInc * r1.y;
        r2.y = sinInc * r1.x + cosInc * r1.y;
        b2Vec2 v2 = center + radius * r2;
        m_lines->Vertex(v1, color);
        m_lines->Vertex(v2, color);
        r1 = r2;
        v1 = v2;
    }

    // Draw a line fixed in the circle to animate rotation.
    b2Vec2 p = center + radius * axis;
    m_lines->Vertex(center, color);
    m_lines->Vertex(p, color);
}

//
void ViewGL::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
{
    m_lines->Vertex(p1, color);
    m_lines->Vertex(p2, color);
}

//
void ViewGL::DrawTransform(const b2Transform& xf)
{
    const float32 k_axisScale = 0.4f;
    b2Color red(1.0f, 0.0f, 0.0f);
    b2Color green(0.0f, 1.0f, 0.0f);
    b2Vec2 p1 = xf.p, p2;

    m_lines->Vertex(p1, red);
    p2 = p1 + k_axisScale * xf.q.GetXAxis();
    m_lines->Vertex(p2, red);

    m_lines->Vertex(p1, green);
    p2 = p1 + k_axisScale * xf.q.GetYAxis();
    m_lines->Vertex(p2, green);
}


void ViewGL::invertGravity()
{

    b2Vec2 grav;
    grav=m_world->GetGravity();
    grav.y*=-1;
    m_world->SetGravity(grav);

    //  openGLVisible=!openGLVisible;


}

b2Body * ViewGL::addSphere(float32 x,float32 y,float32 r,float32 density)
{


    Resource* resBall = new Resource;
    float32 *dataUser= new float32[7];
    resBall->type=ball;
    resBall->data=dataUser;


    qDebug()<<"hello from boat";
    qDebug() << "Sfera messa in:"<< x<<","<<y;
    b2Body *bodySphere ;

    b2BodyDef bodydef;
    bodydef.angle=0;
    bodydef.angularVelocity=0.0f;
    bodydef.angularDamping = 1.3f;
    bodydef.linearDamping = 0.0f;
    //   bodydef.linearVelocity = b2Vec2(0.0f,-17.0f);
    bodydef.linearVelocity=b2Vec2(0.0f,0.0f);
    bodydef.bullet=true;
    dataUser[0]=x; // oldCenter x
    dataUser[1]=y; //oldCenter y
    dataUser[2]=0; // old ang z

    // current quaternion
    dataUser[3]=1; // scalar
    dataUser[4]=0;
    dataUser[5]=0;
    dataUser[6]=0;



    //oldCenter.Set(x,y);
    //oldangz=0;


    bodydef.position.Set(x,y);

    bodydef.type=b2_dynamicBody;
    bodySphere=m_world->CreateBody(&bodydef);

    b2CircleShape shape;
    shape.m_radius=r;
    shape.m_p.Set(0,0);

    b2FixtureDef fixturedef;
    fixturedef.shape=&shape;
    fixturedef.density=density;


    bodySphere->CreateFixture(&fixturedef);
    bodySphere->SetUserData(resBall);
    //bodySphere->SetUserData(dataUser);
    return bodySphere;

}

b2Vec2 ViewGL::convertWorldToScreen(const b2Vec2 preal)
{
    b2Vec2 retVal(2*m_rateWorldScreen*preal.x+m_window->width()/2.,m_window->height()-preal.y*m_rateWorldScreen);

    return retVal;

    /*
            ppix.x=(ppix.x/m_rateWorldScreen);
            //float32 val=ppix.y*W2P;
            //qDebug() << "val:::::" << val;

            ppix.y=( m_window->height()-ppix.y)/m_rateWorldScreen;
            //pcon.Set(pcon.x*W2P,boundingRect().bottom()-pcon.y*W2P);
        */
}


void ViewGL::convertPixelToWorld( b2Vec2& ppix)
{
    ppix.x=((ppix.x-m_window->width()*0.5)/m_rateWorldScreen);
    //float32 val=ppix.y*W2P;
    //qDebug() << "val:::::" << val;

    ppix.y=( m_window->height()-ppix.y)/m_rateWorldScreen;
    //pcon.Set(pcon.x*W2P,boundingRect().bottom()-pcon.y*W2P);

}
/*
        void ViewGL::convertWorldToUVMap( b2Vec2& preal)
        {
            float32 w=m_world_dimension.upperBound.x-m_world_dimension.lowerBound.x;
            float32 h=m_world_dimension.upperBound.y-m_world_dimension.lowerBound.y;
            //qDebug() << w<<h<<m_world_dimension.upperBound.y<<m_world_dimension.lowerBound.y;
            preal.Set(preal.x/w+0.5,preal.y/h+m_world_dimension.lowerBound.y/h);


        }
        */
bool ViewGL::getIsDestroying() const
{
    return m_isDestroying;
}

bool ViewGL::isDrawing() const
{
    return m_isDrawing;
}


b2Body* ViewGL::addRect(float32 x,float32 y,float32 w,float32 h,bool dyn=true)
{

    //  destroysphere1=true;



    // bodySphere->SetLinearVelocity(b2Vec2(0.0f,3.0f));
    b2Vec2 pcon=b2Vec2(x,y);
    convertPixelToWorld(pcon);

    b2BodyDef bodydef;
    bodydef.position.Set(pcon.x,pcon.y);
    if(dyn)
        bodydef.type=b2_dynamicBody;
    b2Body* body=m_world->CreateBody(&bodydef);
    bodydef.angularDamping = 1.3f;
    b2PolygonShape shape;
    shape.SetAsBox(w/2,h/2);

    b2FixtureDef fixturedef;

    fixturedef.restitution=.1;
    fixturedef.shape=&shape;
    fixturedef.density=1.0;
    body->CreateFixture(&fixturedef);
    return body;

}
void ViewGL::pressRight()
{

    if (rightFlipper!=nullptr)
    {
        if (!m_isStop)
        {
            rightFlipper->SetMotorSpeed(-20);
            sound1.play();
            m_countPinRight++;
        }
        if (!m_isPinRightDestroyed)
            if (m_countPinRight>10)
                if (numRand(0,1000)<10)
                {
                    m_isPinRightDestroyed=true;
                }
    }
}
int ViewGL::numRand(int low, int high) {

    return rand() % (high - low + 1) + low;
}

b2World *ViewGL::getWorld() const
{
    return m_world;
}
void ViewGL::pressLeft()
{

    if (leftFlipper!=nullptr)
    {
        if (!m_isStop)
        {
            leftFlipper->SetMotorSpeed(20);
            sound1.play();
            m_countPinLeft++;
        }

        if (!m_isPinLeftDestroyed)
            if (m_countPinLeft>10)
                if (numRand(0,1000)<10)
                {

                    m_isPinLeftDestroyed=true;
                }
        //
    }


    //body1->SetAngularDamping(20);

}

void ViewGL::releaseRight()
{
    if (rightFlipper!=nullptr)
    {
        if (!m_isStop)
        {
            rightFlipper->SetMotorSpeed(10);
            sound2.play();
        }
    }

    //

}
void ViewGL::releaseLeft()
{
    if (leftFlipper!=nullptr)
    {
        if (!m_isStop)
        {
            leftFlipper->SetMotorSpeed(-10);
            sound2.play();
        }
    }
}
void ViewGL::pause()
{

    // QCoreApplication::exit(0);
    if (mTimer.isActive())
    {
        qDebug() << "Stop timer pause"<<mTimer.isActive()<<mTimer.timerId();
        mTimer.stop() ;
        m_isStop=true;
        m_wasStop=true;
    }
    else
    {
        qDebug() << "Start timer pause"<<mTimer.isActive()<<mTimer.timerId();
        mTimer.start(1000.f / 60.0f , this);

        m_wasStop=false;
        m_isStop=false;
    }

}

QImage  ViewGL::getImage1()
{
    QImage image;
    /*
              image=QImage(64,32,QImage::Format_RGB32);

              if (image.load(url.path().append("/images/land_ocean_ice_350.jpg"),"jpg"))
                  qDebug() << "Image OK";
              */
    int i,j;
    image=QImage(64,32,QImage::Format_RGB32);

    QRgb redvalue,greenvalue,whitevalue,orangevalue;
    redvalue = qRgb(255, 0, 0);
    greenvalue = qRgb(0, 255, 0);
    whitevalue =qRgb(255, 255, 255);
    orangevalue = qRgb(255, 127, 0);

    for (i=0;i<16;i++)
    {
        for (j=0;j<=15;j++)
        {
            image.setPixel(i, j, orangevalue);
        }
        for (j=16;j<32;j++)
        {
            image.setPixel(i, j, whitevalue);
        }

    }
    for (i=16;i<32;i++)
    {


        for (j=0;j<=15;j++)
        {
            image.setPixel(i, j, whitevalue);
        }
        for (j=16;j<32;j++)
        {
            image.setPixel(i, j, orangevalue);
        }

    }

    for (i=32;i<48;i++)
    {


        for (j=0;j<=15;j++)
        {
            image.setPixel(i, j, orangevalue);
        }
        for (j=16;j<32;j++)
        {
            image.setPixel(i, j, whitevalue);
        }

    }
    for (i=48;i<64;i++)
    {
        for (j=0;j<=15;j++)
        {
            image.setPixel(i, j, whitevalue);
        }
        for (j=16;j<32;j++)
        {
            image.setPixel(i, j, orangevalue);
        }
    }

    /*
                  for (i=0;i<64;i++)
                  {
                      for (j=0;j<=10;j++)
                      {
                      image.setPixel(i, j, greenvalue);
                      }
                      for (j=11;j<=20;j++)
                      {
                      image.setPixel(i, j, whitevalue);
                      }
                      for (j=21;j<32;j++)
                      {
                      image.setPixel(i, j, redvalue);
                      }
                  }


                  for (i=0;i<32;i++)
                  {
                      for (j=0;j<=20;j++)
                      {
                      image.setPixel(j, i, greenvalue);
                      }
                      for (j=21;j<=40;j++)
                      {
                      image.setPixel(j, i, whitevalue);
                      }
                      for (j=41;j<64;j++)
                      {
                      image.setPixel(j, i, redvalue);
                      }
                  }

          */
    return image;
}

QImage  ViewGL::getImageName(const QString nameImage)
{

    QImage image;
    if (nameImage=="alex")
    {

        image=  getImage3();
    }
    else if (nameImage=="mokomaze")
    {
        image = getImage1();

    }
    else
    {

        image=QImage(64,32,QImage::Format_RGB32);

        if (image.load(dataUrl.path().append("/images/").append(nameImage)))
            qDebug() << "Image OK";

    }
    return image;
}
QImage  ViewGL::getImage3()
{
    QImage image;

    int i,j;
    image=QImage(64,32,QImage::Format_RGB32);

    QRgb redvalue,greenvalue,whitevalue,orangevalue;
    redvalue = qRgb(255, 0, 0);
    greenvalue = qRgb(0, 255, 0);
    whitevalue =qRgb(255, 255, 255);
    orangevalue = qRgb(255, 127, 0);



    for (i=0;i<64;i++)
    {
        for (j=0;j<=10;j++)
        {
            image.setPixel(i, j, greenvalue);
        }
        for (j=11;j<=20;j++)
        {
            image.setPixel(i, j, whitevalue);
        }
        for (j=21;j<32;j++)
        {
            image.setPixel(i, j, redvalue);
        }
    }


    for (i=0;i<32;i++)
    {
        for (j=0;j<=20;j++)
        {
            image.setPixel(j, i, greenvalue);
        }
        for (j=21;j<=40;j++)
        {
            image.setPixel(j, i, whitevalue);
        }
        for (j=41;j<64;j++)
        {
            image.setPixel(j, i, redvalue);
        }
    }


    return image;
}
QQuickWindow* ViewGL::grabScreen()
{

    QImage img;
    QString imgtmp;
    if (mTimer.isActive())
        pause();
    else
        pause();
    img=m_window->grabWindow();

    QDir dir(m_pathCache);
    if (!dir.exists()){
        if (dir.mkpath("."))
            qDebug() << "dir ok->"<< dir;
        else
            qDebug() << "dir no->"<< dir;


    }else
        qDebug() << "dir esiste->"<< dir;
    imgtmp=m_pathCache;
    imgtmp=imgtmp.append("/temp.png");

    //  url= SailfishApp::pathTo("data");
    if (img.save(imgtmp,"png"))
        qDebug() << "save ok->"<< imgtmp;
    else
        qDebug() << "error path->"<< imgtmp;
    m_window->update();
    return m_window;
    //img.save(path);

}
void ViewGL::setSphereRadius(float radius)
{
    qDebug() << radius;

}

void ViewGL::setLightOrientation(float horiz, float vert)
{
    m_horizonthal_light=qDegreesToRadians(horiz);
    m_vertical_light=qDegreesToRadians(vert);

}
void ViewGL::playSound(short tsound)
{

    if (!m_isStop)
    {
        if (tsound==1)
            sound3.play();
        else if (tsound==2)
            sound4.play();
    }

}
void ViewGL::openPinfold()
{




    /*
            QQmlEngine engine;
            QQmlComponent component(&engine, strurl);
           m_object = component.create();

           qDebug() <<"objectName:" <<  m_object->objectName();

           qDebug() << "Property value Y:" << QQmlProperty::read(m_object, "myval").toInt();

        */
    //    if (bodySphere1->GetLinearVelocity()==b2Vec2(0,0))
    //        bodySphere1->SetLinearVelocity(b2Vec2(1,1));
    //    else
    //        bodySphere1->SetLinearVelocity(b2Vec2(0,0));

    if(m_pinFold!=nullptr)
        if (!m_isStop)
        {
            sound5.play();

            m_pinFold->EnableMotor(false);
        }



    /*
            b2Fixture* ballfixture=bodySphere3->GetFixtureList();
            float32 radius= ballfixture->GetShape()->m_radius;
            float32 density=ballfixture->GetDensity();

            float32 volume=4/3.*M_PI*radius*radius*radius;
            float32 sphereMass=density*volume;

            b2Vec2 force=10*sphereMass*b2Vec2(-12.37991, 52.5464);
            qDebug() << "force"<<force.x<<force.y;
            bodySphere1->ApplyForceToCenter(force, true);

        */
    //openGLVisible=false;

    /*
            b2Fixture* fixture = bodySphere3->GetFixtureList();
            while (fixture != nullptr)
            {
                fixture->GetShape()->m_radius=5;


                fixture = fixture->GetNext();
            }
            */


    // openGLVisible=true;

}
void ViewGL::closePinfold()
{
    if(m_pinFold!=nullptr)
        if (!m_isStop)
        {
            sound4.stop();
            m_pinFold->EnableMotor(true);
        }

}

void ViewGL::setNeedModifyOn(short ind)
{
    qDebug() << "need do modify"<<ind;
    m_needModScene[ind]=true;
}

void  ViewGL::scheduledForModify(short ind)
{
    int i;
    //   qDebug()<< "Fixture update";


    b2Body* bodyToModify;
    // qDebug()<<"Punti da modificare:" <<lisBodyToModify.length();
    for (i=0;i<lisBodyToModify.length();i++)
    {


        //  lisBodyToModify[i].ballBody->SetLinearVelocity(b2Vec2(0,0));
        bodyToModify=lisBodyToModify[i].oldFixture->GetBody();

        bodyToModify->DestroyFixture(lisBodyToModify[i].oldFixture);


        for (int j=0;j<2;j++)
        {


            if (lisBodyToModify[i].newFixtureDef[j].shape!=nullptr)
            {

                bodyToModify->CreateFixture(&lisBodyToModify[i].newFixtureDef[j]);
            }


        }

    }


    lisBodyToModify.clear();
    m_needModScene[ind]=false;
    needTriangulate[ind] = true;
    m_nVertTexture[ind]=0;



}
void  ViewGL::addBodyBreak(b2Body*  body)
{
    m_isDestroying=true;
    bodyToBreak=body;

    //listBodyToBreak.append(body);
}

void  ViewGL::addFixModModify(fixtureToModify&  fixtMod)
{

    lisBodyToModify.append(fixtMod);
}
void ViewGL::setAccSpheres(const b2Vec2 acc)
{

 //fireBall1->SetTransform(b2Vec2(0,5), 0);


    if (bodySphere1!=nullptr)
    {
        /*
        ballfixture1=bodySphere1->GetFixtureList();
        radius= ballfixture1->GetShape()->m_radius;
        density=ballfixture1->GetDensity();
        volume=radius;
        sphereMass=density*volume;
        invsphereMass=1.0/sphereMass;
        force=invsphereMass*acc;

        qDebug() << "force1"<<force.x<<force.y;
        */

        if (  bodySphere1->GetPosition().y<52)
            bodySphere1->ApplyForce(acc,bodySphere1->GetWorldCenter(), true);

    }
    if (bodySphere2!=nullptr)
    {
        /*
        ballfixture2=bodySphere2->GetFixtureList();
        radius= ballfixture2->GetShape()->m_radius;
        density=ballfixture2->GetDensity();
        volume=radius;
        sphereMass=density*volume;
        invsphereMass=1.0/sphereMass;
        force=invsphereMass*acc;
        qDebug() << "force2"<<force.x<<force.y;
        */
        if (  bodySphere2->GetPosition().y<52)
            bodySphere2->ApplyForce(acc, bodySphere2->GetWorldCenter(),true);
    }
    if (bodySphere3!=nullptr)
    {
        /*
        ballfixture3=bodySphere3->GetFixtureList();
        radius= ballfixture3->GetShape()->m_radius;
        density=ballfixture3->GetDensity();
        volume=radius;
        sphereMass=density*volume;
        invsphereMass=1.0/sphereMass;
        force=invsphereMass*acc;
        qDebug() << "force3"<<force.x<<force.y;
        */
        if (  bodySphere3->GetPosition().y<52)
            bodySphere3->ApplyForce(acc, bodySphere3->GetWorldCenter(),true);

    }



}
//break body into fixutre
void ViewGL::breakBody(b2Body* b)
{


    b2Fixture* fixture = b->GetFixtureList();
    b2Transform t;
    b2Body *bodyNew;
    /*
            b2BodyDef myBodyDef;
                myBodyDef.type = b2_dynamicBody; //this will be a dynamic body
                myBodyDef.position.Set(0, 20); //a little to the left

                b2Body* dynamicBody1 = m_world->CreateBody(&myBodyDef);

            b2CircleShape circleShape;
              circleShape.m_p.Set(0, 0); //position, relative to body position
              circleShape.m_radius = 1; //radius

              b2FixtureDef myFixtureDef;
                myFixtureDef.shape = &circleShape; //this is a pointer to the shape above
                dynamicBody1->CreateFixture(&myFixtureDef); //add a fixture to the body

        */



    while (fixture != nullptr)
    {





        const b2Shape *shape = fixture->GetShape();
        const int childCount = shape->GetChildCount();
        for (int32 child = 0; child < childCount; ++child) {

            if (shape->GetType()==b2Shape::e_polygon)
            {
                Resource* resDynamic = new Resource;
                resDynamic->data=(float32*) ((Resource*)b->GetUserData())->data;
                resDynamic->index_iron=0;
                resDynamic->type=glass_dynamic_body_unbreakable;


                //  qDebug() << fixture->GetShape()->m_type;

                // create body from fixture
                b2BodyDef bodydef;
                bodydef.angle=b->GetAngle();
                bodydef.angularVelocity=b->GetAngularVelocity();
                bodydef.angularDamping = b->GetAngularDamping();
                bodydef.linearDamping = b->GetLinearDamping();
                //   bodydef.linearVelocity = b2Vec2(0.0f,-17.0f);
                bodydef.linearVelocity=b->GetLinearVelocity();

                bodydef.bullet=true;
                bodydef.position.Set(b->GetPosition().x,b->GetPosition().y);
                bodydef.type=b2_dynamicBody;

                int32 nvert=((b2PolygonShape*)shape)->GetVertexCount();
                qDebug() << "vert num:" <<nvert;
                b2PolygonShape polygonShape;
                //poly.SetAsBox(1,1);
                //b2Vec2* points=new malloc();
                b2Vec2 *point= &((b2PolygonShape*) shape)->m_vertices[0];
                //for (int32 i=0;i<nvert;i++)
                // {
                //    b2Vec2* vet=&((b2PolygonShape*) shape)->m_vertices[i];
                polygonShape.Set(point,nvert);
                // }

                qDebug() << "step3" ;
                //

                //bodydef.position.Set(5,10);
                /*
                        point->Set(-5,10);
                        b2Vec2 vertices[3];
                          vertices[0].Set(point[0].x,point[0].y);
                          vertices[1].Set(point[0].x+2,point[0].y);
                          vertices[2].Set( point[0].x+1,point[0].y-3);

                          b2PolygonShape polygonShape1;
                           b2BodyDef myBodyDef;
                            myBodyDef.type = b2_dynamicBody;
                          polygonShape1.Set(vertices, 3); //pass array to the shape

                            b2FixtureDef myFixtureDef;
                          myFixtureDef.shape = &polygonShape1; //change the shape of the fixture

                           myBodyDef.position.Set(b->GetPosition().x,b->GetPosition().y);
                          b2Body* dynamicBody2 = m_world->CreateBody(&myBodyDef);
                          dynamicBody2->CreateFixture(&myFixtureDef); //add a fixture to the body
                        //


                        */

                //                qDebug() << "point1" <<point[0].x<<point[0].y;
                //                qDebug() << "point2" <<point[1].x<<point[2].y;
                //                qDebug() << "point3" <<point[2].x<<point[2].y;

                //                qDebug() << "centro:"<<b->GetPosition().x<<b->GetPosition().y;
                b2FixtureDef fixturedef;
                fixturedef.shape=&polygonShape;
                fixturedef.density=fixture->GetDensity(); // iron

                bodyNew=m_world->CreateBody(&bodydef);
                bodyNew->SetUserData(resDynamic);

                bodyNew->CreateFixture(&fixturedef);


            }
        }
        fixture = fixture->GetNext();
    }

}
void ViewGL::Tilt()
{
    isStatusTilt=true;
    if (!m_isPinLeftDestroyed)
    {

        m_isPinLeftDestroyed=true;
    }
    if (!m_isPinRightDestroyed)
    {

        m_isPinRightDestroyed=true;
    }



    sound9.play();


}
void ViewGL::setOffsetY(int offsetY)
{

    m_offsetY=offsetY;
    // qDebug() << m_offsetY<<m_rateWorldScreen<<m_offsetY/m_rateWorldScreen<<"--"<<(m_offsetY+m_window->height())/m_rateWorldScreen;
    //if (m_offsetY/m_rateWorldScreen<-30 && (m_offsetY+m_window->height())/m_rateWorldScreen>-30)

    //{
    //}
    //qDebug() << "dentro!!";
    //else
    // qDebug() << "fuori!!";
    // qDebug() << "("<<m_world_dimension.lowerBound.x<<","<<m_world_dimension.lowerBound.y << ")"<<"("<<m_world_dimension.upperBound.x<<","<<m_world_dimension.upperBound.y << ")";

}
void ViewGL::addCage(float32 xPos,float32 yPos)
{

    //body with circle fixture
    // pin static
    b2BodyDef bodyDef;
    b2FixtureDef fixtureDef;
    b2CircleShape circleShape;
    circleShape.m_radius = 0.5;


    fixtureDef.shape = &circleShape;
    bodyDef.position.Set(xPos,yPos);
    b2Body* chainBase = m_world->CreateBody( &bodyDef );
    chainBase->SetType(b2_staticBody);
    chainBase->CreateFixture( &fixtureDef );
    //*******

    b2Vec2 attack;
    b2Body* lastBody ;
    b2RevoluteJointDef jdPin;
    b2RopeJointDef m_ropeDef;


    //CHAIN
    {
        b2PolygonShape shape;
        shape.SetAsBox( 0.125f,0.5f);

        b2FixtureDef fd;
        fd.shape = &shape;
        fd.density = 20.0f;
        fd.friction = 0.2f;
        fd.filter.categoryBits = 0x0001;
        fd.filter.maskBits = 0xFFFF & ~0x0002;


        jdPin.collideConnected = false;

        const int32 N = 10;
        const float32 y = yPos;
        m_ropeDef.localAnchorA.Set(xPos, y);

        b2Body* prevBody = chainBase;
        b2Body* body;
        for (int32 i = 0; i < N-1; ++i)
        {
            b2BodyDef bd;
            bd.type = b2_dynamicBody;
            bd.position.Set(xPos, y-1.0*i-0.125);
            body = m_world->CreateBody(&bd);

            body->CreateFixture(&fd);





            b2Vec2 anchor(xPos, y-float32(i));
            jdPin.Initialize(prevBody, body, anchor);
            m_world->CreateJoint(&jdPin);


            prevBody = body;
        }

        lastBody=body;

        attack.Set(xPos, y-1.0f * (N-1));
        m_ropeDef.localAnchorB.SetZero();

        float32 extraLength = 0.01f;
        m_ropeDef.maxLength = N - 1.0f + extraLength;
        m_ropeDef.bodyB = prevBody;
    }

    // {
    //    m_ropeDef.bodyA = ground;
    //    m_rope = m_world->CreateJoint(&m_ropeDef);
    // }
    //*******

    // CAGE (create with R.U.B.E)

    b2AABB aabb;
    b2Body** bodies = (b2Body**)b2Alloc(2 * sizeof(b2Body*));
    b2Joint** joints = (b2Joint**)b2Alloc(1 * sizeof(b2Joint*));




    // beam opening
    {
        b2BodyDef bd;
        bd.type = b2BodyType(2);
        bd.position=attack;
        bd.angle = 0.000000000000000e+00f;
        bd.linearVelocity.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
        bd.angularVelocity = 0.000000000000000e+00f;
        bd.linearDamping = 0.000000000000000e+00f;
        bd.angularDamping = 0.000000000000000e+00f;
        bd.allowSleep = bool(4);
        bd.awake = bool(2);
        bd.fixedRotation = bool(0);
        bd.bullet = bool(0);
        bd.active = bool(32);
        bd.gravityScale = 1.000000000000000e+00f;
        bodies[0] = m_world->CreateBody(&bd);

        {
            b2FixtureDef fd;
            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 1.000000000000000e+00f;
            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2PolygonShape shape;
            b2Vec2 vs[8];
            vs[0].Set(-1.066207885742188e-03f, -1.610147953033447e-01f);
            vs[1].Set(-1.066207885742188e-03f, 1.639821529388428e-01f);
            vs[2].Set(-2.341766357421875e+00f, 1.639981269836426e-01f);
            vs[3].Set(-2.341766357421875e+00f, -1.611177921295166e-01f);
            shape.Set(vs, 4);

            fd.shape = &shape;

            bodies[0]->CreateFixture(&fd);
        }
    }
    // body Cage

    {
        b2BodyDef bd;
        bd.type = b2BodyType(2);
        bd.position=attack;
        bd.angle = 0.000000000000000e+00f;
        bd.linearVelocity.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
        bd.angularVelocity = 0.000000000000000e+00f;
        bd.linearDamping = 0.000000000000000e+00f;
        bd.angularDamping = 0.000000000000000e+00f;
        bd.allowSleep = bool(4);
        bd.awake = bool(2);
        bd.fixedRotation = bool(0);
        bd.bullet = bool(0);
        bd.active = bool(32);
        bd.gravityScale = 1.000000000000000e+00f;
        bodies[1] = m_world->CreateBody(&bd);

        //Pin beam opening
        {
            b2FixtureDef fd;
            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 1.000000000000000e+00f;
            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2CircleShape shape;
            shape.m_radius = 0.2;
            shape.m_p.Set(1.007178664207458e+00f, -3.257267236709595e+00f);

            fd.shape = &shape;

            bodies[1]->CreateFixture(&fd);
        }
        //Part 1
        {
            b2FixtureDef fd;
            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 1.000000000000000e+00f;
            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2PolygonShape shape;
            b2Vec2 vs[8];
            vs[0].Set(2.032855987548828e+00f, -1.125827789306641e+00f);
            vs[1].Set(3.552436828613281e-05f, -2.336502075195312e-05f);
            vs[2].Set(-1.709583401679993e+00f, -9.341659545898438e-01f);
            vs[3].Set(-1.448612928390503e+00f, -1.086727142333984e+00f);
            vs[4].Set(1.298900246620178e+00f, -1.125827789306641e+00f);
            shape.Set(vs, 5);

            fd.shape = &shape;

            bodies[1]->CreateFixture(&fd);
        }
        //Part2
        {

            b2FixtureDef fd;
            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 1.000000000000000e+00f;
            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2PolygonShape shape;
            b2Vec2 vs[8];
            vs[0].Set(2.032855987548828e+00f, -1.125827789306641e+00f);
            vs[1].Set(1.298900246620178e+00f, -1.125827789306641e+00f);
            vs[2].Set(1.298900246620178e+00f, -3.460426330566406e+00f);
            vs[3].Set(1.995939016342163e+00f, -3.460426330566406e+00f);
            shape.Set(vs, 4);

            fd.shape = &shape;

            bodies[1]->CreateFixture(&fd);

        }
        //part3
        {
            b2FixtureDef fd;
            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 1.000000000000000e+00f;
            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2PolygonShape shape;
            b2Vec2 vs[8];
            vs[0].Set(-1.448612928390503e+00f, -1.086727142333984e+00f);
            vs[1].Set(-1.709583401679993e+00f, -9.341659545898438e-01f);
            vs[2].Set(-1.988792896270752e+00f, -1.086727142333984e+00f);
            vs[3].Set(-1.988792896270752e+00f, -3.460426330566406e+00f);
            vs[4].Set(-1.484741926193237e+00f, -3.462027072906494e+00f);
            shape.Set(vs, 5);

            fd.shape = &shape;

            bodies[1]->CreateFixture(&fd);
        }
        //***
        // set texture
        getBodyAABB(aabb,bodies[1],false);
        b2Vec2 *uvRef=new b2Vec2(aabb.lowerBound.x, aabb.lowerBound.y);
        Resource* resDynamic = new Resource;
        resDynamic->data=(float32*)uvRef;
        resDynamic->index_iron=0;

        resDynamic->type=wood_dynamic_body;


        bodies[1]->SetUserData(resDynamic);


        // pin attack chain

        //***
        {
            b2FixtureDef fd;

            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 0.1;

            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2CircleShape shape;
            shape.m_radius = 0.3f;
            shape.m_p.Set(0, 0);

            fd.shape = &shape;

            bodies[1]->CreateFixture(&fd);
        }
    }

    //joint Cage<->Beam
    {
        b2RevoluteJointDef jd;
        jd.bodyA = bodies[1];
        jd.bodyB = bodies[0];
        jd.collideConnected = bool(0);
        jd.localAnchorA.Set(1.013136386871338e+00f, -3.257251501083374e+00f);
        jd.localAnchorB.Set(-1.062751282006502e-03f, 1.869238447397947e-03f);
        jd.referenceAngle = 0.000000000000000e+00f;
        jd.enableLimit = bool(1);
        jd.lowerAngle = 1.745329238474369e-02f;
        jd.upperAngle = 1.570796370506287e+00f;
        jd.enableMotor = bool(1);
        jd.motorSpeed = -1.047197532653809e+01f;
        jd.maxMotorTorque = 1.800000000000000e+03f;
        joints[0] = m_world->CreateJoint(&jd);
    }


    //    jdPin.maxMotorTorque =1;
    //    jdPin.motorSpeed = 0.0f;
    //    jdPin.enableMotor = true;


    jdPin.Initialize(lastBody, bodies[1], attack);
    m_world->CreateJoint(&jdPin);



    b2Free(joints);
    b2Free(bodies);
    joints = NULL;
    bodies = NULL;



}
void ViewGL::addChooseBox(float32 xPos,float32 yPos)
{
    b2Body** bodies = (b2Body**)b2Alloc(2 * sizeof(b2Body*));
    b2Joint** joints = (b2Joint**)b2Alloc(1 * sizeof(b2Joint*));
    {
        b2BodyDef bd;
        bd.type = b2BodyType(0);
        bd.position.Set(xPos, yPos);
        bd.angle = 0.000000000000000e+00f;
        bd.linearVelocity.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
        bd.angularVelocity = 0.000000000000000e+00f;
        bd.linearDamping = 0.000000000000000e+00f;
        bd.angularDamping = 0.000000000000000e+00f;
        bd.allowSleep = bool(4);
        bd.awake = bool(2);
        bd.fixedRotation = bool(16);
        bd.bullet = bool(0);
        bd.active = bool(32);
        bd.gravityScale = 1.000000000000000e+00f;
        bodies[0] = m_world->CreateBody(&bd);

        {
            b2FixtureDef fd;
            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 1.000000000000000e+00f;
            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2CircleShape shape;
            shape.m_radius = 2.212429940700531e-01f;
            shape.m_p.Set(0.000000000000000e+00f, 0.000000000000000e+00f);

            fd.shape = &shape;

            bodies[0]->CreateFixture(&fd);
        }
    }
    {
        b2BodyDef bd;
        bd.type = b2BodyType(2);
        bd.position.Set(xPos, yPos-3);
        bd.angle = 0.000000000000000e+00f;
        bd.linearVelocity.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
        bd.angularVelocity = 0.000000000000000e+00f;
        bd.linearDamping = 0.000000000000000e+00f;
        bd.angularDamping = 0.000000000000000e+00f;
        bd.allowSleep = bool(4);
        bd.awake = bool(2);
        bd.fixedRotation = bool(0);
        bd.bullet = bool(0);
        bd.active = bool(32);
        bd.gravityScale = 1.000000000000000e+00f;
        bodies[1] = m_world->CreateBody(&bd);

        {
            b2FixtureDef fd;
            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 1.000000000000000e+00f;
            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2PolygonShape shape;
            b2Vec2 vs[8];
            vs[0].Set(2.314823627471924e+00f, -2.118247032165527e+00f);
            vs[1].Set(2.314339160919189e+00f, -1.818227291107178e+00f);
            vs[2].Set(-2.477427959442139e+00f, -1.823640346527100e+00f);
            vs[3].Set(-2.476943492889404e+00f, -2.123659849166870e+00f);
            shape.Set(vs, 4);

            fd.shape = &shape;

            bodies[1]->CreateFixture(&fd);
        }
        {
            b2FixtureDef fd;
            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 1.000000000000000e+00f;
            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2PolygonShape shape;
            b2Vec2 vs[8];
            vs[0].Set(-1.832341194152832e+00f, -1.811600923538208e+00f);
            vs[1].Set(-1.832341194152832e+00f, 1.715185880661011e+00f);
            vs[2].Set(-2.132361173629761e+00f, 1.715185880661011e+00f);
            vs[3].Set(-2.132361173629761e+00f, -1.811600923538208e+00f);
            shape.Set(vs, 4);

            fd.shape = &shape;

            bodies[1]->CreateFixture(&fd);
        }
        {
            b2FixtureDef fd;
            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 1.000000000000000e+00f;
            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2PolygonShape shape;
            b2Vec2 vs[8];
            vs[0].Set(2.326209545135498e+00f, 1.719559788703918e+00f);
            vs[1].Set(2.325725078582764e+00f, 2.019579410552979e+00f);
            vs[2].Set(-2.466042041778564e+00f, 2.014166355133057e+00f);
            vs[3].Set(-2.465557575225830e+00f, 1.714146852493286e+00f);
            shape.Set(vs, 4);

            fd.shape = &shape;

            bodies[1]->CreateFixture(&fd);
        }
        {
            b2FixtureDef fd;
            fd.friction = 2.000000029802322e-01f;
            fd.restitution = 0.000000000000000e+00f;
            fd.density = 1.000000000000000e+00f;
            fd.isSensor = bool(0);
            fd.filter.categoryBits = uint16(1);
            fd.filter.maskBits = uint16(65535);
            fd.filter.groupIndex = int16(0);
            b2PolygonShape shape;
            b2Vec2 vs[8];
            vs[0].Set(1.923377513885498e+00f, -1.810076951980591e+00f);
            vs[1].Set(1.923377513885498e+00f, 1.716709852218628e+00f);
            vs[2].Set(1.623357534408569e+00f, 1.716709852218628e+00f);
            vs[3].Set(1.623357534408569e+00f, -1.810076951980591e+00f);
            shape.Set(vs, 4);

            fd.shape = &shape;

            bodies[1]->CreateFixture(&fd);
        }
    }
    {
        b2RevoluteJointDef jd;
        jd.bodyA = bodies[0];
        jd.bodyB = bodies[1];
        jd.collideConnected = bool(0);
        jd.localAnchorA.Set(-4.425048828125000e-03f, -1.176675796508789e+00f);
        jd.localAnchorB.Set(3.949134610593319e-03f, 1.879271745681763e+00f);
        jd.referenceAngle = 0.000000000000000e+00f;
        jd.enableLimit = bool(0);
        jd.lowerAngle = 0.000000000000000e+00f;
        jd.upperAngle = 0.000000000000000e+00f;
        jd.enableMotor = bool(0);
        jd.motorSpeed = 0.000000000000000e+00f;
        jd.maxMotorTorque = 0.000000000000000e+00f;
        joints[0] = m_world->CreateJoint(&jd);
    }
    b2Free(joints);
    b2Free(bodies);
    joints = NULL;
    bodies = NULL;
}
void ViewGL::createTeam(int indball1,int indball2,int indball3)
{
    QBall *ball1 = reinterpret_cast<QBall *>(m_dataList.at(indball1));
    QBall *ball2 = reinterpret_cast<QBall *>(m_dataList.at(indball2));
    QBall *ball3 = reinterpret_cast<QBall *>(m_dataList.at(indball3));

    qDebug() << ball1->name();
    qDebug() << ball2->name();
    qDebug() << ball3->name();
}
// must optimize but how?
void ViewGL::checkTeam( )
{


    m_needRecreate1=true;

}

// must optimize but how?
int ViewGL::countBall()
{
    int count=0;
    int i;
    for ( i = 0; i < m_dataList.count(); i++)
    {
        if (reinterpret_cast<QBall *>(m_dataList.at(i))->pickedtoplay())
            count++;

    }
    return count;

}
