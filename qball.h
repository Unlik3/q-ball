#ifndef QBALL_H
#define QBALL_H

#include <QObject>

class QBall : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString nationalityCode READ nationalityCode WRITE setNationalityCode NOTIFY nationalityCodeChanged)
    Q_PROPERTY(QString material READ material WRITE setMaterial NOTIFY materialChanged)
    Q_PROPERTY(QString texture READ texture WRITE setTexture NOTIFY textureChanged)
    Q_PROPERTY(QString whenbuild READ whenbuild WRITE setWhenbuild NOTIFY whenbuildChanged)
    Q_PROPERTY(QString img_preview READ img_preview WRITE setImg_preview NOTIFY img_previewChanged)

    Q_PROPERTY(float radius READ radius WRITE setRadius NOTIFY radiusChanged)
    Q_PROPERTY(float density READ density WRITE setDensity NOTIFY densityChanged)
    Q_PROPERTY(float restitution READ restitution WRITE setRestitution NOTIFY restitutionChanged)

    Q_PROPERTY(bool pickedtoplay READ pickedtoplay WRITE setPickedtoplay NOTIFY pickedtoplayChanged)
public:
    QBall();
    QBall(const QString name,QString natCode,const QString material,const float radius,const float density,const float restitution,const QString texture,const QString whenbuild,const  QString img_preview);
    QString m_name;
    QString m_nationalityCode;
    QString m_material;
    float m_radius;
    float m_density;
    float m_restitution;
    QString m_texture;
    QString m_whenbuild;
    QString m_img_preview;
    bool m_pickedtoplay;



    QString name() const;
    void setName(const QString &name);
    QString nationalityCode() const;
    void setNationalityCode(const QString &nationalityCode);

    QString material() const;
    void setMaterial(const QString &material);

    float radius() const;
    void setRadius(float radius);

    float density() const;
    void setDensity(float density);


    float restitution() const;
    void setRestitution(float restitution);



    QString texture() const;
    void setTexture(const QString &texture);

    QString whenbuild() const;
    void setWhenbuild(const QString &whenbuild);

    QString img_preview() const;
    void setImg_preview(const QString &img_preview);


    bool pickedtoplay() const;
    void setPickedtoplay(const bool &pickedtoplay);


signals:
    void nameChanged();
    void nationalityCodeChanged();
    void materialChanged();
    void textureChanged();
    void whenbuildChanged();
    void img_previewChanged();

    void radiusChanged();
    void densityChanged();
    void restitutionChanged();
    void pickedtoplayChanged();
};

#endif // QBALL_H
