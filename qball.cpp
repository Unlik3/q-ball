#include "qball.h"
#include <QDebug>
QBall::QBall()
{


}

QBall::QBall(const QString name, QString natCode, const QString material, const float radius, const float density,const float restitution, const QString texture, const QString whenbuild, const QString img_preview)
{
    m_name=name;
    m_nationalityCode=natCode;

    m_material=material;
    m_radius=radius;
    m_density=density;
    m_restitution=restitution;
    m_texture=texture;
    m_whenbuild=whenbuild;
    m_img_preview=img_preview;
    m_pickedtoplay=false;
}

QString QBall::img_preview() const
{
    return m_img_preview;
}

void QBall::setImg_preview(const QString &img_preview)
{
    if (img_preview != m_img_preview) {
        m_img_preview = img_preview;

        emit img_previewChanged();
    }
}

bool QBall::pickedtoplay() const
{
    return m_pickedtoplay;
}

void QBall::setPickedtoplay(const bool &pickedtoplay)
{
    if (pickedtoplay != m_pickedtoplay) {
    //    qDebug() << "Piched to ply"<<pickedtoplay;
        m_pickedtoplay = pickedtoplay;

        emit pickedtoplayChanged();
    }

}

QString QBall::whenbuild() const
{
    return m_whenbuild;
}

void QBall::setWhenbuild(const QString &whenbuild)
{

    if (whenbuild != m_whenbuild) {
        m_whenbuild = whenbuild;

        emit whenbuildChanged();
    }
}

QString QBall::texture() const
{
    return m_texture;
}

void QBall::setTexture(const QString &texture)
{
    if (texture != m_texture) {
        m_texture = texture;

        emit textureChanged();
    }

}

float QBall::density() const
{
    return m_density;
}

void QBall::setDensity(float density)
{

    if (density != m_density) {
        m_density = density;

        emit densityChanged();
    }

}

float QBall::restitution() const
{
    return m_restitution;
}

void QBall::setRestitution(float restitution)
{

    if (restitution != m_restitution) {
        m_restitution = restitution;

        emit restitutionChanged();
    }

}


float QBall::radius() const
{
    return m_radius;
}

void QBall::setRadius(float radius)
{
    if (radius != m_radius) {
        m_radius = radius;

        emit radiusChanged();
    }

}

QString QBall::material() const
{
    return m_material;
}

void QBall::setMaterial(const QString &material)
{

    if (material != m_material) {
        m_material = material;

        emit materialChanged();
    }

}

QString QBall::nationalityCode() const
{
    return m_nationalityCode;
}

void QBall::setNationalityCode(const QString &nationalityCode)
{
    if (nationalityCode != m_nationalityCode) {
        m_nationalityCode = nationalityCode;

        emit nationalityCodeChanged();
    }

}

QString QBall::name() const
{
    return m_name;
}

void QBall::setName(const QString &name)
{
    if (name != m_name) {
        m_name = name;

        emit nameChanged();
    }
}

