#include "glrendersphere.h"

GLRenderSphere::GLRenderSphere(QObject *parent) :
    QObject(parent)
{
 m_program=nullptr;
}
void GLRenderSphere::Create(const int width,const int height,const QImage image,float32 scale,float32 traslx)
{
    // float32 radiusWorld;
    m_rateWorldScreen=scale;
    QImage image1=image.mirrored(false,true);
    //   radiusWorld=1.1;
    // QOpenGLBuffer m_vertex;
    m_heightScreen=height;
    GLfloat radius=m_rateWorldScreen;
    qDebug()<< "Raggio :"<< radius;
    m_status=normal;

    GLfloat g_vertex_buffer_data[] = {
        // X        Y         S     T
        -radius, radius,   -1.0f, 1.0f,
        radius,    radius,    1.0f, 1.0f,
        radius,    -radius, 1.0f,-1.0f,
        -radius ,-radius,-1.0f,-1.0f,

    };


    /*
        if  (window->openglContext()->hasExtension(QByteArrayLiteral("GL_OES_vertex_array_object")))
            qDebug() << "VAO presente!!!!";
        else
             qFatal( "GL_OES_vertex_array_object is not supported" );

    */

    m_program = new QOpenGLShaderProgram();
    //  printVersionInformation();
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex,
                                       "#version 300 es\n"
                                       "in vec4 vtxcoord;\n"
                                       "out vec2 texCoord;\n"
                                       "//out float alfaout;\n"
                                       "//uniform float alfa;\n"
                                       "uniform mat4 projectionMatrix;\n"
                                       "uniform mat4 modelMatrix;\n"
                                       "void main() {\n"
                                       "//alfaout=alfa;\n"
                                       "      texCoord=vtxcoord.zw;\n"
                                       "    gl_Position = projectionMatrix*modelMatrix*vec4(vtxcoord.xy,-0.3, 1.0f);\n"
                                       "}\n"
                                       );
    qDebug() << "addShaderFromSourceCode Vertex" << m_program->log();
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       "#version 300 es\n"
                                       "precision mediump float;\n"
                                       "out vec4 my_fragcolor;\n"
                                       "uniform float alfa;\n"
                                       "in vec2 texCoord;\n"
                                       "uniform mat4 rotationMatrix;\n"
                                       "uniform vec4 lightPos;\n"
                                       "uniform sampler2D tex;\n"
                                       "#define PIP2    1.5707963       // PI/2\n"
                                       "#define PI      3.1415629\n"
                                       "#define TWOPI   6.2831853       // 2PI\n"
                                       "\n"
                                       "void main() {\n"
                                       "\n"
                                       "//float longitude=0.0f;\n"
                                       "//float latitude=0.0f;\n"
                                       "//float lx = sin(longitude) * cos(latitude);\n"
                                       "//float ly = sin(latitude);\n"
                                       "//float lz = cos(longitude) * cos(latitude);\n"
                                       "vec4 color=vec4(1.0f,1.0f,1.0f,1.0f);\n"
                                       "//vec4 lightPos=vec4(lx,ly,lz,0.0f);\n"
                                       "float minLight=0.05f;\n"
                                       "float d = (texCoord.x) * (texCoord.x) + (texCoord.y) * (texCoord.y);\n"

                                       "//if (texCoord.y<0.0f)\n"
                                       "if (d>1.0f)\n"
                                       "discard;\n"
                                       "float z= sqrt(1.0f - d);\n"
                                       "vec4 point = vec4(texCoord.xy, z, 1.0);\n"
                                       "float l = clamp(dot(point, lightPos), minLight, 1.0);\n"
                                       "point *=rotationMatrix;\n"
                                       "float x = 0.5f+atan(point.x, point.z)/TWOPI;\n"
                                       "float y = 0.5f-2.0f*asin(point.y)/TWOPI;\n"
                                       "vec4 color1;\n"
                                       "color1=texture(tex, vec2(x, y));\n"
                                       "if (color1.w<0.1f)"
                                       "{\n"
                                       "   z= -sqrt(1.0f - d);\n"
                                       "   point = vec4(texCoord.xy, z, 1.0);\n"
                                       "   l = clamp(dot(point, lightPos), minLight, 1.0);\n"
                                       "   point *=rotationMatrix;\n"
                                       "   x = 0.5f+atan(point.x, point.z)/TWOPI;\n"
                                       "   y = 0.5f-2.0f*asin(point.y)/TWOPI;\n"
                                       "}\n"
                                       "my_fragcolor = texture(tex, vec2(x, y)) * vec4(l, l, l, 1.0) * color;\n"

                                       " my_fragcolor = alfa*my_fragcolor;\n"


                                       "}\n"
                                       );
    qDebug() << "addShaderFromSourceCode Fragment" << m_program->log();
    m_projection_matrix.setToIdentity();
    m_projection_matrix.ortho(0.0,  width, height, 0.0f, -1.0f, 1.0f);
    m_projection_matrix.translate(traslx/scale+width/2.0f,0,0);


    m_program->bindAttributeLocation("vtxcoord",0);
    if (m_program->link())
        qDebug()<< "Link OK";

    if(m_VAOEXT.initializeOpenGLFunctions())
        qDebug()<< "initializeOpenGL OK";
    m_VAOEXT.glGenVertexArraysOES(1,  &VertexArrayID);
    m_VAOEXT.glBindVertexArrayOES(VertexArrayID);
    // genera il VBO
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
    glVertexAttribPointer(
                0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                4,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );

    glEnableVertexAttribArray(0);

    m_texture  =  new QOpenGLTexture(image1);

    m_texture->setMinificationFilter(QOpenGLTexture::Nearest);
    m_texture->setMagnificationFilter(QOpenGLTexture::Nearest);

    // set the wrap mode
    m_texture->setWrapMode(QOpenGLTexture::Repeat);


    glBindBuffer(GL_ARRAY_BUFFER, 0);
    m_VAOEXT.glBindVertexArrayOES(0);

    if (m_program->bind())
    {

        m_program->setUniformValue("projectionMatrix", m_projection_matrix);
    }
    qDebug() << "Program link result:" << m_program->log();

}

QOpenGLTexture *GLRenderSphere::texture() const
{
    return m_texture;
}

void GLRenderSphere::Flush(float32* userData,const float32 coordX,const float32 coordY,float32 radius,QVector4D lightPos,const GLfloat alfa,float32 offsetY)
{
    if (m_program->bind())
    {

        QQuaternion qRot;

        qRot.setScalar(userData[3]);
        qRot.setVector(userData[4],userData[5],userData[6]);
        m_rotationMatrix.setToIdentity();
        m_rotationMatrix.rotate(qRot);


        m_modelMatrix.setToIdentity();

        m_modelMatrix.translate(coordX*m_rateWorldScreen,m_heightScreen-coordY*m_rateWorldScreen+offsetY);
        m_modelMatrix.scale(radius);
        m_program->setUniformValue("rotationMatrix", m_rotationMatrix);
        m_program->setUniformValue("modelMatrix", m_modelMatrix);
        m_program->setUniformValue("lightPos", lightPos);

        m_program->setUniformValue("alfa", alfa);

        m_VAOEXT.glBindVertexArrayOES(VertexArrayID);

        m_texture->bind();

        //    m_program->enableAttributeArray("vtxcoord");
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        //  m_program->disableAttributeArray("vtxcoord");


        m_texture->release();
        m_VAOEXT.glBindVertexArrayOES(0);
        m_program->release();


    }



}


GLRenderSphere::~GLRenderSphere()
{
 //   delete m_texture;
    if (m_program!=nullptr)
    {
    delete m_program;
    glDeleteBuffers(1, &vertexbuffer);
    m_VAOEXT.glDeleteVertexArraysOES(1, &VertexArrayID);
    }
    qDebug() << "sphere delete";
}
