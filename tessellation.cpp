
#include "Box2D.h"
#include "renderitemraw.h"
#include "tesselation.h"
#include <assert.h>
#include <QDebug>

//extern ViewGL* pMainWin;
void* stdAlloc(void* userData, unsigned int size)
{
    int* allocated = ( int*)userData;
    TESS_NOTUSED(userData);
    *allocated += (int)size;
    return malloc(size);
}

void stdFree(void* userData, void* ptr)
{
    TESS_NOTUSED(userData);
    free(ptr);
}
/*
void convertLoopToPolygon(const b2Fixture* chain, GLdouble* data,const GLint lenght)
{


    int32 i;

   // lenght = chain->GetShape()->GetChildCount();

    b2EdgeShape edge;
    b2Vec2 point;
    const b2Transform& xf = chain->GetBody()->GetTransform();




    for ( i = 0; i <lenght; i++) {


        ((b2ChainShape*)chain->GetShape())->GetChildEdge(&edge, i);
        //qDebug() << "point" << edge.m_vertex1.x << edge.m_vertex1.y;
        point= b2Mul(xf, edge.m_vertex1);

        //  qDebug() << "pointT" << point.x << point.y;
        data[i * 3]=point.x;
        data[1+i * 3]=point.y;
        data[2+i * 3]=0;


    }


}
*/
