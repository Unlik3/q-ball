#ifndef MYCONTACTLISTENER_H
#define MYCONTACTLISTENER_H
#include "Box2D/Box2D.h"
#include <QUrl>
#include <QSoundEffect>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "renderitemraw.h"
#include "typeglobal.h"
class ViewGL;
class MyContactListener : public b2ContactListener
{
public:
    MyContactListener(const QUrl url, ViewGL* mainWin);
        void BeginContact(b2Contact* contact);
        void EndContact(b2Contact* contact);
        void initHapticEffect();

protected:
 int vibra_evdev_file_search(bool *supportsRumble, bool *supportsPeriodic);
     QSoundEffect m_sound[10];
 struct ff_effect m_effect;
 ViewGL* m_mainWin;
 int m_fd;
 void playHapticEffect(const unsigned int intensity, const unsigned int duration);
 void ballIron(b2Contact *contact, Resource *bodyUserData1);
};



#endif // MYCONTACTLISTENER_H
