#ifndef GLRENDERSPHERE_H
#define GLRENDERSPHERE_H

#include <QObject>
#include <QtGui/QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <Box2D/Box2D.h>
//#include "box2dworld.h"
#include <QOpenGLTexture>
#include <QtOpenGLExtensions>

class GLRenderSphere : public QObject
{
    Q_OBJECT
public:
    explicit GLRenderSphere(QObject *parent = 0);
    ~GLRenderSphere();
    void Create(const int width,const int height,const QImage image,float32 scale,float32 traslx);


    QOpenGLShaderProgram *m_program;
    QOpenGLTexture *m_texture;
    QOpenGLExtension_OES_vertex_array_object m_VAOEXT;
    GLuint VertexArrayID, vertexbuffer;
    QMatrix4x4                  m_projection_matrix;
    QMatrix4x4                  m_modelMatrix;
    QMatrix4x4                  m_rotationMatrix;
    QVector2D                   m_scale_tran;

    float m_rateWorldScreen;
    int m_heightScreen; // screen height

    enum Status{
       normal=0,
       splash=1,
       underwater=2
    } m_status;   
    void Flush(float32 *userData, const float32 coordX, const float32 coordY, float32 radius, QVector4D lightPos, const GLfloat alfa, float32 offsetY);
    QOpenGLTexture *texture() const;

private:

signals:

public slots:

};

#endif // GLRENDERSPHERE_H
