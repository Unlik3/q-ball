#ifndef GLRENDERPOINTS_H
#define GLRENDERPOINTS_H

#include <QObject>
#include <QtGui/QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLTexture>
#include <Box2D/Box2D.h>
//#include "box2dworld.h"

class GLRenderPoints : public QObject
{
    Q_OBJECT
public:
    explicit GLRenderPoints(QObject *parent = 0);
    ~GLRenderPoints();


    void Flush(float32 offsetY);
    void Vertex(const b2Vec2 &v, const b2Color &c, b2Vec2 size);
    void Create(int width, int height, float32 scale, float32 traslx, const QUrl dataUrl);
    inline int getNumVertex(){return m_count;}
private:
   int32 m_count;
   enum { e_maxVertices = 26 };
   b2Vec2 m_vertices[e_maxVertices];
   b2Color m_colors[e_maxVertices];
   b2Vec2 m_sizeAngle[e_maxVertices];
    bool m_firstFlush;
   QOpenGLShaderProgram *m_program;
   QOpenGLBuffer m_vertexBuffer;
   QOpenGLBuffer m_colorBuffer;
   QOpenGLBuffer m_sizeBuffer;
   QOpenGLTexture *m_texture;
   QMatrix4x4                  m_projection_matrix;
   QVector3D                   m_param;




signals:

public slots:

};

#endif // GLRENDERPOINTS_H
