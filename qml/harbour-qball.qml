/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.qball.controller 1.0
import Nemo.Configuration 1.0
import QtSensors 5.2
import "pages"


ApplicationWindow
{


     Component.onCompleted:
     {
        //  console.log(dataPath);
         console.log("App Running!")
         var types = QmlSensors.sensorTypes();
                   console.log(types.join(", "));




         controller.play1=setting.chooseball1
         controller.play2=setting.chooseball2
         controller.play3=setting.chooseball3

         myModel[controller.play1].pickedtoplay=true
         myModel[controller.play2].pickedtoplay=true
         myModel[controller.play3].pickedtoplay=true


         console.log("start:",setting.chooseball1,setting.chooseball2,setting.chooseball3)





     }

    //start app
    initialPage: Component { StartPage { } }
  cover: Qt.resolvedUrl("cover/CoverPage.qml")

  ConfigurationValue {
           id: bestTimesArray
           key: StandardPaths.data+"/arrLevel"
           defaultValue: ["","","",""]
       }

  ConfigurationValue {
           id: failedattemptsArray
           key: StandardPaths.data+"/arrAttempt"
           defaultValue: [0,0,0]
       }

  ConfigurationGroup {
      id: setting
      synchronous : true
      path: StandardPaths.data+"/config1"

      property int chooseball1:controller.play1

      property int chooseball2:controller.play2

      property int  chooseball3:controller.play3
      property int level:1
      property string nameTeam:"Magicians"

      onValueChanged: {

          console.log("onValueChanged!!!",path)


      }
      onValuesChanged:
      {
            console.log("onValuesChanged!!!")
      }
  }

  //cover: null
}


