import QtQuick 2.0
import Sailfish.Silica 1.0



Page {
    id: aboutPage
    SilicaListView {

        VerticalScrollDecorator {}
        width: aboutPage.width
        spacing: Theme.paddingLarge

        flickableDirection: Flickable.VerticalFlick

        anchors.fill: parent

            PageHeader {
                id: pagHeader
                title: "About"
            }
            Label {
                id:titleSail
                text: qsTr("q-BALL");
                anchors.top: pagHeader.bottom
                color:  Theme.secondaryColor
                anchors.horizontalCenter: parent.horizontalCenter;
                font.pixelSize: Theme.fontSizeLarge ;
                font.bold: true;
            }
            Label {
                id:byLabel
                text: qsTr("by");font.pixelSize:Theme.fontSizeSmall ;
                 anchors.top:titleSail.bottom
                color: Theme.secondaryColor ;
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                id:myname;
                 anchors.top:byLabel.bottom
                text: qsTr("Daniele Cavallini");
                font.pixelSize:Theme.fontSizeSmall ;
                color: Theme.secondaryColor ;
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                id:version
                 anchors.top:myname.bottom
                text: qsTr("Version: Beta 2");
                font.pixelSize:Theme.fontSizeSmall ;
                color: Theme.secondaryColor ;
                anchors.horizontalCenter: parent.horizontalCenter
            }
            //Label { text: qsTr("e-mail: <a href=\"cavaldan2@gmail.com\">cavaldan2@gmail.com</a>")  ;font.pixelSize:Theme.fontSizeSmall;anchors.horizontalCenter: parent.horizontalCenter}
            TextArea {

                anchors { left: parent.left; right: parent.right }
                readOnly:true;
                id: textRequest
                text: qsTr("If you like this app, you can show your support with a donation! Thank you");
                font.pixelSize:Theme.fontSizeSmall ;
                color: Theme.secondaryColor ;
                anchors.top:version.bottom;
                anchors.margins: 20

                width:Screen.width

            }

            Button {
               id:button_donation
               anchors.top:textRequest.bottom
               anchors.margins: 20
               anchors.horizontalCenter: parent.horizontalCenter;
               text: "Donate!"
               onClicked: Qt.openUrlExternally("http://www.paypal.me/sailfishos");
            }
            Label {

               color:Theme.secondaryColor
               anchors.top:button_donation.bottom
               anchors.margins: 20
               anchors.horizontalCenter: parent.horizontalCenter;
               text: "paypal.me/sailfishos"

            }
            Label {

               color:Theme.secondaryColor
               anchors.bottom: parent.bottom
               anchors.margins: 20
               anchors.horizontalCenter: parent.horizontalCenter;
               text: "e-mail:cavaldan2@gmail.com"


            }
        }

}
