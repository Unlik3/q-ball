/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0


Dialog {
    id: page

    property int initY:0

    Component.onDestruction:
    {
        console.log("Destruction Beginning!")
        controller.stopGame();
    }
    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {

        id:flickItem
        interactive:true
        anchors.fill: parent
        contentWidth: ret.width;
        contentHeight: ret.height

        function updateGL() {
            controller.setOffsetY(-contentY)
        }


        Timer {

            id: scrolltimer
            interval: 20
            repeat: true
            running: true
            triggeredOnStart: true

            onTriggered:
            {


                parent.updateGL()
            }
        }
        onFlickEnded:
        {
            scrolltimer.stop()
            console.log("onFlickEnded:")
        }

        onMovementEnded:
        {
            console.log("onMovementEnded:")
        }
        onFlickStarted:
        {

            console.log("onFlickStarted:")
        }
        onMovementStarted:
        {




            scrolltimer.start()
          //  console.log("movementStarted:",contentY)
        }
        onVerticalVelocityChanged:
        {
            console.debug(verticalVelocity)
            if (verticalVelocity<-200)
                 controller.setAcc(0,-1*flickItem.verticalVelocity);
            }
        Item
        {


            id:ret;
            visible: true
            //   color: "red"
            width: Screen.width
            // anchors.top: lab.bottom
            height: 5000
            anchors.top: parent.top
            anchors.topMargin: 100
            Column {

                id:col
              //  property alias posY: ret.y
                Repeater {


                    id: rep
                    onItemAdded: {
                     console.log("val y",rep.get(index).columnItem.y)

                    }
                    model: myModel
                    Column {
                        TextSwitch {
                            id: origin

                            width: Screen.width
                            checked: false
                            text: "Name"
                            description: "Activates the Doomsday device"
                            onCheckedChanged: {


                                console.log("Value", rep.itemAt(index).y)
                            }

                        }

                        Row {

                            id: row
                            x: Theme.horizontalPageMargin

                            spacing: Theme.paddingMedium

                            Rectangle {
                                id: colorIndicator
                                x:250

                                width: 80
                                height: 80
                                color: "#e60003"
                            }
                            Column
                            {
                                Label {
                                    text: "Color:"
                                    color:  Theme.highlightColor

                                }
                                Label {
                                    text: "Material:"
                                    color:  Theme.highlightColor

                                }
                                Label {
                                    text: "dateTime"
                                    color:  Theme.highlightColor

                                }
                            }
                        }
                    }
                }
            }

        }
    }
    Component.onCompleted: {

        console.log("ScrollPage Running!")


        /*

        for (var i = 0; i <10; ++i) {

            myModel.append({ 'dateTime': "dateTimexx",
                             'section': "Make",
                              'index1':i+1 ,
                               'posy':0})



        }

           for (var i = 0; i <10; ++i) {
              console.log("val:", rep.itemAt(i).y)
           }

           */
    }


    // Tell SilicaFlickable the height of its content.
    //  contentHeight: 5000


    // Place our content in a Column.  The PageHeader is always placed at the top
    // of the page, followed by our content.

    ListModel {
           id: myModel

       }


}




