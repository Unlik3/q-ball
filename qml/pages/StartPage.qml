import QtQuick 2.0
import Sailfish.Silica 1.0


Page {


    Component.onCompleted: console.log("Start Running!")

    onStatusChanged: {

        if (status==PageStatus.Active)
        {
            console.log("Attiva")
            bal1Anim.start();
            bal2Anim.start();
            bal3Anim.start();
        }
    }

    anchors.fill: parent

    SilicaFlickable {
        id: flick
        interactive:true
        visible:true
        anchors.fill: parent


        Label {

            id:qball
            text: "q-Ball"

            font.pixelSize: Theme.fontSizeHuge
            color:  Theme.highlightBackgroundColor
            anchors.top: parent.top
            anchors.topMargin: 100

            anchors.horizontalCenter:  parent.horizontalCenter

        }


        Label {
            id:team
            anchors.top: qball.bottom
            text:"Team: "+setting.nameTeam
            anchors.horizontalCenter:  parent.horizontalCenter
            anchors.topMargin: 20

        }

        Row{
            id: row
            spacing: 60
            anchors.top: team.bottom
            anchors.topMargin: 40
            anchors.horizontalCenter:  parent.horizontalCenter
            Image {
                id: colorIndicator


                RotationAnimation on rotation {
                    id:bal1Anim
                    // loops: Animation.Infinite
                    from: 0
                    to: 360
                    duration: 4000
                }
                scale: myModel[controller.play1].radius
                width: 80
                height: 80
                // color: "#e60003"

                source: dataURL+"/icon/"+myModel[controller.play1].img_preview
                MouseArea {
                    anchors.fill: parent
                    onClicked: bal1Anim.restart()

                }

            }
            Image {

                RotationAnimation on rotation {
                    id:bal2Anim
                    // loops: Animation.Infinite
                    from: 360
                    to: 0
                    duration: 2500
                }


                scale:  myModel[controller.play1].radius
                width: 80
                height: 80
                // color: "#e60003"
                source: dataURL+"/icon/"+myModel[controller.play2].img_preview
                MouseArea {
                    anchors.fill: parent
                    onClicked: bal2Anim.restart()

                }
            }
            Image {

                RotationAnimation on rotation {
                    id:bal3Anim
                    // loops: Animation.Infinite
                    from: 0
                    to: 360
                    duration: 2000
                }


                scale: myModel[controller.play3].radius
                width: 80
                height: 80
                // color: "#e60003"
                source: dataURL+"/icon/"+myModel[controller.play3].img_preview
                MouseArea {
                    anchors.fill: parent
                    onClicked: bal3Anim.restart()

                }
            }
        }

        Column {

            id:col
            //anchors.centerIn: parent
            anchors.top: row.bottom
            anchors.topMargin: 40
            anchors.horizontalCenter:  parent.horizontalCenter
            spacing: 10



            Button {
                text:"Practice"
                onClicked: {

                    pageStack.push(Qt.resolvedUrl("PlayPage.qml"), {},PageStackAction.Immediate)
                    controller.startGame("Pratice.json")

                    //setting.clear()

                    //  pageStack.push(Qt.resolvedUrl("PlayPage.qml"), {},PageStackAction.Immediate)
                    // controller.startGame("start1.json")
                }
            }
            Row
            {
                Button {
                    id:button_1
                    text:"Just to start"
                    enabled:setting.level>0

                    onClicked: {


                        pageStack.push(Qt.resolvedUrl("PlayPage.qml"), {nameScheme:"Scheme 1"},PageStackAction.Immediate)
                        controller.startGame("Schema_1.json")

                    }
                }
                Item
                {

                    visible:setting.level>1
                    height: button_1.height
                    width: 200
                    Label
                    {


                        color:Theme.secondaryColor
                        anchors.centerIn: parent
                        text:bestTimesArray.value[0]+" sec"+"("+failedattemptsArray.value[0]+")"

                    }

                }
            }
            Row
            {
                Button {

                    text:"Indian camp"
                    enabled:setting.level>1

                    onClicked: {


                        pageStack.push(Qt.resolvedUrl("PlayPage.qml"), {nameScheme:"Scheme 2"},PageStackAction.Immediate)
                        controller.startGame("Schema_2.json")

                    }
                }
                Item
                {

                    visible:setting.level>2
                    height: button_1.height
                    width: 200
                    Label
                    {


                        color:Theme.secondaryColor
                        anchors.centerIn: parent
                        text:bestTimesArray.value[1]+" sec"+"("+failedattemptsArray.value[1]+")"

                    }

                }
            }
            Row
            {
                Button {
                    enabled:  setting.level>2
                    text:"Monkeys' lair"
                    onClicked: {


                        pageStack.push(Qt.resolvedUrl("PlayPage.qml"), {nameScheme:"Scheme 3"},PageStackAction.Immediate)
                      //  controller.startGame("Schema_3.json")
                                     controller.startGame("Schema_3.json")



                    }
                }
                Item
                {
                    visible:setting.level>3
                    height: button_1.height
                    width: 200

                    Label
                    {

                        color:Theme.secondaryColor

                        anchors.centerIn: parent
                        text:bestTimesArray.value[2]+" sec"+"("+failedattemptsArray.value[2]+")"

                    }

                }
            }
            Row
            {
                Button {
                    enabled:  setting.level>3
                    text:"Glass hell"
                    onClicked: {

                        pageStack.push(Qt.resolvedUrl("PlayPage.qml"), {nameScheme:"Scheme 4"},PageStackAction.Immediate)
                        //controller.startGame("test.json")
                                    controller.startGame("Schema_4.json")


                    }
                }
                Item
                {
                    visible:setting.level>4
                    height: button_1.height
                    width: 200
                    Label
                    {

                        color:Theme.secondaryColor

                        anchors.centerIn: parent
                        text:bestTimesArray.value[3]+" sec"+"("+failedattemptsArray.value[3]+")"

                    }

                }
            }
        }

        PullDownMenu {
            id: about
            visible:true
            MenuItem {

                text: qsTr("About")
                onClicked:
                {


                    //controller.setVisibleOpenGL(false);
                    pageStack.push(Qt.resolvedUrl("AboutPage.qml"))

                }
            }

            MenuItem {

                text: qsTr("Controls")
                onClicked:
                {


                    //controller.setVisibleOpenGL(false);
                    pageStack.push(Qt.resolvedUrl("Controls.qml"))

                }
            }
            MenuItem {

                text: qsTr("Choose Team")
                onClicked:
                {

                    pageStack.push(Qt.resolvedUrl("ChoosePage.qml"), {},PageStackAction.Animated)
                }
            }
        }

        Label {

            visible:setting.level>4
            id:overallTime
            text: qsTr("Overall time:")+sumTime()+" sec"

            font.pixelSize: Theme.fontSizeMedium
            color:  Theme.primaryColor
            anchors.top: col.bottom
            anchors.topMargin: 50

            anchors.horizontalCenter:  parent.horizontalCenter

        }
    }


    function sumTime()
    {

        return Number(bestTimesArray.value[0])+Number(bestTimesArray.value[1])+Number(bestTimesArray.value[2]+Number(bestTimesArray.value[3]))
    }


}
