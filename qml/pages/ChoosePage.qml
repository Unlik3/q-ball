/****************************************************************************************
**
** Copyright (C) 2013 Jolla Ltd.
** Contact: Matt Vogt <matthew.vogt@jollamobile.com>
** All rights reserved.
**
** This file is part of Sailfish Silica UI component package.
**
** You may use this file under the terms of BSD license as follows:
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Jolla Ltd nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
** ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************************/

import QtQuick 2.2
import Sailfish.Silica 1.0


Page {


    property int totChoose:controller.countBallCtrl()

    property string newname:setting.nameTeam



    //  property variant arrayChooseTeam: []



    id:choose


    Component {


        // property alias ischecked: origin.checked


        id:dele

        Column {

           // property string name
            TextSwitch {
                id: origin

                width: Screen.width
                checked: model.modelData.pickedtoplay
                text: ("Name :")+model.modelData.name
                description: qsTr("Made in:") + nationalityCode+"\n"+qsTr("Date of construction:")+whenbuild
                onCheckedChanged: {



                    model.modelData.pickedtoplay=checked
                    totChoose=controller.countBallCtrl()





                    if(totChoose===3)
                    {
                        acceptTeam.color=Theme.highlightColor
                        acceptTeam.text=qsTr("Accept")
                    }
                    else
                    {
                        acceptTeam.color="red"
                        acceptTeam.text=qsTr("Reject ("+totChoose+qsTr(") Need 3"))
                    }
                }

            }

            Row {

                id: row
                x: Theme.horizontalPageMargin

                spacing: Theme.paddingMedium

                Image {
                    id: colorIndicator
                    x:250

                    scale: radius
                    width: 80
                    height: 80
                    // color: "#e60003"
                    source: dataURL+"/icon/"+model.modelData.img_preview
                }
                Column
                {
                    Label {
                        id:material
                        font.pixelSize: Theme.fontSizeSmall
                        text: qsTr("Material: ")+ model.modelData.material
                        color:  Theme.highlightColor


                    }
                    Label {

                        font.pixelSize: Theme.fontSizeSmall
                        text: qsTr("Radius: ")+model.modelData.radius.toFixed(2)+ " cm";
                        color:  Theme.highlightColor

                        wrapMode: Label.WordWrap

                    }
                    Label {
                        font.pixelSize: Theme.fontSizeSmall
                        text: qsTr("Mass: ")+(model.modelData.density*4/3.*Math.PI *radius*radius*radius).toFixed(2)+" g";
                        color:  Theme.highlightColor

                    }
                    Label {
                        font.pixelSize: Theme.fontSizeSmall
                        text: qsTr("Density: ")+(model.modelData.density).toFixed(2)+" g/cm3";
                        color:  Theme.highlightColor

                    }
                }
            }

        }


    }




    //    ListModel {
    //        id:myModel

    //    }
    SilicaListView {
        id:slv
        interactive:true
        visible:true
        anchors.fill: parent

        /*
        height: 1600


            anchors.top:parent.top
            anchors.left:parent.left
            anchors.right:parent.right

           // anchors.bottom:acceptTeam.top


       // anchors.bottomMargin: acceptTeam.height
*/

        spacing: Theme.paddingMedium

        header: PageHeader {
            title: "Make up Team"
        }


        delegate: dele
        model:myModel


        VerticalScrollDecorator {}



        footer: Label {
            width: parent.width
            height: Screen.height/2

        }
        PullDownMenu {
            id: about
            visible:true

            MenuItem {

                text: qsTr("Change name")
                onClicked:
                {

                    pageStack.push(pageChangeName)

                }
            }


        }
    }


    /*
    onAccepted: {








    }

*/
    Component {
        id: pageChangeName

        Dialog {
            id:dial

            anchors.fill: parent
            canAccept:true
            DialogHeader {
                           id: header
                           title: qsTr("Team's name")
                       }
            TextField {

                id: name
                anchors.centerIn: parent
                focus: true
                text: setting.nameTeam
                EnterKey.onClicked:dial.accept()


            }
            onAccepted:{


                newname=name.text

            }
        }

    }
    Component.onCompleted:
    {
        console.log("choose Page Running!")


    }


    Rectangle {
        anchors.bottom: choose.bottom
        height: 100;
        width: choose.width
        color:Theme.highlightDimmerColor
        opacity: 0.9
        radius: 10
        Label {
            id: acceptTeam
            anchors.centerIn: parent
            text:qsTr("Accept")

        }

        MouseArea {
            anchors.fill: parent


            onPressed:{
                // parent.color= Theme.secondaryColor
                //acceptTeam.opacity=0.8
                // parent.opacity=0.8
                parent.color=Qt.lighter(parent.color,1.5)
                //parent.opacity=Qt.lighter()
            }

            onReleased: {

                parent.color=Theme.highlightDimmerColor
                     setting.nameTeam=newname

                // acceptTeam.opacity=0.9
                //  parent.color=Theme.highlightDimmerColor
                console.log("clic",totChoose)
                if (totChoose===3)
                {



                    var count=0

                    for (var i = 0; i < myModel.length; i++)
                    {

                        console.log("scelto",myModel[i].pickedtoplay)
                        if (myModel[i].pickedtoplay)
                        {


                            if (count===0)
                            {
                                controller.play1=i
                                count++
                            }
                            else if (count===1)
                            {
                                controller.play2=i
                                count++
                            }
                            else if (count===2)
                            {
                                controller.play3=i
                                count++
                            }


                        }
                        console.log(controller.play1,controller.play2,controller.play3)
                    }
                    // reset

                  //  setting.clear()
                    bestTimesArray.value=["","","",""]

                    setting.level=1
                    failedattemptsArray.value=["","","",""]

                     controller.checkTeamCTRL();
                    console.log(controller.play1,controller.play2,controller.play3)
                    if (newname=="ForzaLazio")
                        setting.level=4
                    pageStack.pop()
                }


            }



        }
    }

    /*
   Button {




       visible:true
       id: acceptTeam
       height: 100;
       width: choose.width
       anchors.bottom: choose.bottom
       text:qsTr("Accept")

       color: Theme.highlightColor
      // color:Theme.highlightDimmerColor
     //  opacity:1
       onClicked: {
           if (totChoose===3)
           {
            controller.checkTeamCTRL();
           pageStack.pop()
           }

       }

   }
   */

}
