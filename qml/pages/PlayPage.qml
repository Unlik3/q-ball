
import QtQuick 2.0
import Sailfish.Silica 1.0
//import QtFeedback 5.0
import QtSensors 5.2

Page {

    property int timePassed: 0
    property string nameScheme

    id: page1
    backNavigation:false
    forwardNavigation:false
    showNavigationIndicator:false
    property double inclination:0
    state:"gaming"
    Component.onDestruction:
    {
        console.log("Destruction Beginning!",state)

        var listfail1 = failedattemptsArray.value
        if (page1.nameScheme=="Scheme 1" )
            listfail1[0]++
        else if  (page1.nameScheme=="Scheme 2" )
            listfail1[1]++
        else if  (page1.nameScheme=="Scheme 3" )
            listfail1[2]++
        else if  (page1.nameScheme=="Scheme 4" )
            listfail1[3]++

        failedattemptsArray.value = listfail1

        controller.stopGame();
    }
    Component.onCompleted: console.log("First Page Running!")
    Connections {
        target: controller
        onStatusChanged: {
            console.log("Received in QML from C++: ",page1.nameScheme,controller.status )
            state=  controller.status
            if (state!="gaming")
                timer.stop()
            else
                timer.start()

            /*
            if (state=="tilt" || state=="failed" || state=="levelok")
            {

                var listfail1 = failedattemptsArray.value
                if (page1.nameScheme=="Scheme 1" )
                {

                    listfail1[0]++

                }
                else if  (page1.nameScheme=="Scheme 2" )
                {

                    listfail1[1]++


                }
                else if  (page1.nameScheme=="Scheme 3" )
                {

                    listfail1[2]++


                }
                failedattemptsArray.value = listfail1
            }
*/




            if (state=="levelok" )
            {

                var list1 = bestTimesArray.value
                if(page1.nameScheme=="Scheme 1")
                {


                    list1[0]=timePassed

                    setting.level=Math.max(setting.level,2);


                }
                else if (page1.nameScheme=="Scheme 2")
                {
                    list1[1]=timePassed
                    setting.level=Math.max(setting.level,3);
                }

                else if (page1.nameScheme=="Scheme 3")
                {
                    list1[2]=timePassed

                    setting.level=Math.max(setting.level,4);
                }

                else if (page1.nameScheme=="Scheme 4")
                {
                    list1[3]=timePassed

                    setting.level=Math.max(setting.level,5);
                }

                console.log("level:",setting.level)

                bestTimesArray.value = list1

            }
        }


    }



//        Rectangle
//        {
//            visible:true
//            anchors.fill: parent
//            color:  "white"
//         }


    Accelerometer {
        /*
        function calcPitch(x,y,z)
        {
            return -(Math.atan(y / Math.sqrt(x * x + z * z)) * 57.2957795);
        }

        */

        function calcRoll(x,y,z)
        {
            return -(Math.atan(x / Math.sqrt(y * y + z * z)) * 57.2957795);
        }
        function getCurrentTime()
        {
            var data = new Date();

            return data.getTime();
        }

        property real threshold: 5 //default velocity threshold for shake to register
        property real timeout: 1000 //default interval between events
        property real lastX:0
        property real lastY:0
        property real lastZ:0
        property bool firstime:true
        property double lastTime;
        property double currentTime;

        skipDuplicates:true
        property double old1:0



        //  property int tilt:0



        id: accel
        dataRate: 100
        active:true
        accelerationMode: Accelerometer.Gravity


        onReadingChanged: {


         //   if (controller.tilt==0)
           // {
                // freeBall.color="green"
              //  freeBall.opacity=0.8
          //  }
            /*
            else
            {
                console.log("val:",1./controller.tilt)
                freeBall.opacity+=(1.-1./controller.tilt)
            }
            */

            if (state=="gaming" )
            {

                var angFix=0
                var current = accel.reading
                var ang1=calcRoll(accel.reading.x, accel.reading.y, accel.reading.z)
                // var ang2=calcPitch(accel.reading.x, accel.reading.y, accel.reading.z)
                var angtxt=ang1.toFixed(0);
                var oldangletext;


                inclination=old1
                old1=ang1

                if (angtxt!==oldangletext)
                {
                    //  angle.text=angtxt;

                    var lenbar=ang1*Screen.width/180.0;

                    if (ang1>=0)
                    {

                        anglerect.width= lenbar
                        anglerect.x=page1.width/2
                    }
                    else
                    {
                        anglerect.x=page1.width/2+lenbar
                        anglerect.width=-lenbar


                    }


                    oldangletext=angtxt;
                }

                var timeDifference;
                var deltaX = 0;
                var deltaY = 0;
                var deltaZ = 0;

                //  console.log("********riferimento:",ang2)
                // if (controller.tilt===0)
                //   rotation.active=false

                if (firstime) {
                    lastX = current.x;
                    lastY = current.y;
                    lastZ = current.z;

                    console.log("first time!!!")
                    lastTime = getCurrentTime();

                    firstime=false;
                    controller.tilt=0;
                    return;
                }


                deltaX = Math.abs(lastX - current.x);
                deltaY = Math.abs(lastY - current.y);
                deltaZ = Math.abs(lastZ - current.z);




                angFix=ang1;
                if (((deltaX > threshold) && (deltaY > threshold)) ||
                        ((deltaX > threshold) && (deltaZ > threshold)) ||
                        ((deltaY > threshold) && (deltaZ > threshold))) {
                    //calculate time in milliseconds since last shake registered

                    currentTime = getCurrentTime()

                    if (controller.tilt===0)
                    {

                        controller.preTilt();



                    }
                    controller.tilt++

                   // console.log("val:",1-1./(controller.tilt+1))
                   // freeBall.opacity=1.0-1./(controller.tilt+1)


                    timeDifference = currentTime - lastTime;

                    if (timeDifference > timeout) {

                        console.log("shake!!",controller.tilt,inclination)

                        lastTime = getCurrentTime();
                    }

                    if (controller.tilt===1 )

                    {



                        console.log("Angolo accelelerometro:",inclination)

                        // freeBall.color="red"
                        var forceapp
                        if (inclination>=0)
                        {
                            forceapp= inclination/90.0*20000+10000

                        }
                        else
                        {
                            forceapp= inclination/90.0*20000-10000

                        }

                        controller.setAcc(forceapp,getRandomArbitrary(0,5000));

                        controller.randomBreak();
                        /*
                        if (angFix<0) controller.setAcc(-50000*,0);
                        else
                            controller.setAcc(50000,0);
                            */

                    }





                    if (controller.tilt>=5 && state=="gaming")
                    {


                        //   timer.stop()
                        controller.tilt=0;
                        controller.statusTilt();



                        page1.state="tilt"




                        animation.start()

                        console.log("TILT Sorry!!")
                    }



                }



                lastX = current.x;
                lastY = current.y;
                lastZ = current.z;

            }

        }
    }


    SilicaFlickable {
        id: flick
        interactive:false
        visible:true
        anchors.fill: parent


        /*
        Label {
            x:5
            y:5
            id: angle
            color:"red"
            text:"0"

        }
*/
        Rectangle {
            id:anglerect
            color:Theme.highlightBackgroundColor
            anchors.top: parent.top
            x:page1.width/2
            width: 0
            height: 15
        }


        /*
        Rectangle {
            visible:false
            id:first
            color:"blue"
            x:0;
            y:0;
            opacity: .2
            width:parent.width;
            height: 80;
            MouseArea {
                anchors.fill: first
                onPressed: {
                    //pageStack.push(Qt.resolvedUrl("AboutPage.qml"))

                    //controller.pressClick(mouseX,mouseY)

                }
            }

        }
        */
        Item {

            //  color:"red"
            x:0
            width:parent.width/2
            // y:first.height
            height: parent.height
         //   opacity: .2
            MultiPointTouchArea {
                anchors.fill: parent
                touchPoints: [
                    TouchPoint {
                        id: touch1
                        onPressedChanged: {
                            if (pressed) {
                                controller.pressLeft(mouseX,mouseY)
                            }
                            else
                                controller.releaseLeft(mouseX,mouseY)

                        }

                    }

                ]
            }

        }
        Item {
            /*
            HapticsEffect {
                id: customBuzz
                intensity: 40
                duration: 20
            }
            */
            //  color:"green"
            x:parent.width/2
            width:parent.width/2
            // y:first.height
            height: parent.height
     //       opacity: .2
            MultiPointTouchArea {
                anchors.fill: parent
                touchPoints: [
                    TouchPoint {
                        id: touch2
                        onPressedChanged: {
                            if (pressed) {
                                //  customBuzz.start()
                                controller.pressRight(mouseX,mouseY)
                            }
                            else
                                controller.releaseRight(mouseX,mouseY)

                        }

                    }


                ]
            }

        }

        Rectangle {
            id:leftShake
            opacity: 0.2
            width: parent.width/4
            height: 140
            color:"blue"
            anchors.left: parent.left
            //anchors.bottom: parent.bottom
            y:Screen.height/2-height
            MultiPointTouchArea {
                anchors.fill: parent
                touchPoints: [
                    TouchPoint {

                        onPressedChanged: {
                            if (pressed) {

                                if (controller.canpush)
                                {
                                    controller.impossiblePush()
                                    controller.setAcc(getRandomArbitrary(1500,3000),getRandomArbitrary(1500,3000));
                                }

                            }
                        }

                    }

                ]
            }


        }
        Rectangle {
            id:rightShake
            opacity: 0.2
            width: parent.width/4
            height: 140
            color:"blue"
            anchors.right: parent.right
            y:Screen.height/2-height
            MultiPointTouchArea {
                anchors.fill: parent
                touchPoints: [
                    TouchPoint {

                        onPressedChanged: {
                            if (pressed) {

                                if (controller.canpush)
                                {
                                    controller.impossiblePush()
                                    controller.setAcc(getRandomArbitrary(-3000,-1500),getRandomArbitrary(1500,3000));
                                }




                            }
                        }

                    }

                ]
            }


        }
        Rectangle {
            id:freeBall
            opacity: 0.4
            width: 80
            height: 80

            radius: 40
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 60
            color:controller.tilt>0?"red":getColorFreeBall()


            MouseArea {
                anchors.fill: parent
                onPressed: {
                    //  console.log(dataPath);



                    /*

                    var forceapp;

                        if (inclination>=0)
                        {
                            forceapp= getRandomArbitrary(5000,10000)


                        }
                        else
                        {
                            forceapp= getRandomArbitrary(-10000,-5000)

                        }

                         controller.setAcc(forceapp,getRandomArbitrary(5000,10000));

                         */

                    //*****
                    /*
                    state="tilt"
                    //    timer.stop()
                    controller.tilt=0;
                    controller.statusTilt();







                    page1.state="tilt"

                    animation.start()
                    time.text ="TILT!!!! Game over"
                    console.log("TILT Sorry!!")
                    */
                    //*****

                    controller.openPinfoldCtrl()

                }
                onReleased:  {
                    controller.closePinfoldCtrl()

                }
            }


        }
    }

    IconButton {
        id:pauseButton
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top:parent.top
        anchors.topMargin:  25

        visible: true
        scale:0.8


        icon.source:dataURL+"/images/pause.png"
        onPressed:
        {
            opacity=0.3
        }


        onReleased:
        {

            opacity=1.0


            //controller.receiveFromQml(42);
            pause.text  = "Pause"
            flick.interactive=!flick.interactive
            //about.visible=!about.visible
            pause.visible=!pause.visible





            if (pause.visible)
            {
                opacity=0.8
                accel.active=false
                //   timer.stop()
                controller.status="paused"
                console.log("####@@@@@@")




            }
            else
            {
                accel.active=true
                controller.status="gaming"
                //   timer.start()
                console.log("####@@@@@@1")
            }


            controller.pause();
            animation.start()

        }


    }

    Timer {
        property int last:0
        id:timer
        interval: 1000; running: true; repeat: true
        onTriggered: {
            timePassed++
            time.text = "Time: "+ timePassed+" sec"
            /*
            if (controller.checkState())
            {
                flick.interactive=!flick.interactive
                about.visible=!about.visible
                pause.visible=!pause.visible
                timePassed=0;
                pauseButton.visible=false
                freeBall.visible = false
                pause.text="YOU WIN!!"
                timer.stop()
                //timer.last+=timePassed
            }
            */
        }
    }




    Label {
        id:time
        visible:true
        opacity: 0.5
        // text: "10 min 30 sec"
        anchors.horizontalCenter: parent.horizontalCenter
        // anchors.verticalCenter:  parent.verticalCenter
        anchors.top: parent.top
        anchors.topMargin: 150
        color: Theme.highlightColor
        font.family: Theme.fontFamilyHeading

    }

    Label {
        id:modeTilt
        visible:false
        text: qsTr("TILT!")
        anchors.horizontalCenter: parent.horizontalCenter
      //  anchors.verticalCenter:  parent.verticalCenter
        color: Theme.highlightColor
        font.pixelSize: Theme.fontSizeExtraLarge
        NumberAnimation {
            id:animation
            target: modeTilt
            property: "y"
            from: 0
            to: Screen.height
            duration: 6000
            loops: Animation.Infinite
            easing.type: Easing.InCubic

        }


    }

    Label {
        id:pause
        visible:false
        text: qsTr("Pause")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter:  parent.verticalCenter
        color: Theme.highlightColor
        //font.family: Theme.fontFamilyHeading

    }
    Label {
        id:failed_completed

        visible:false
        text: qsTr("LEVEL FAILED")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter:  parent.verticalCenter
        color: Theme.highlightColor
        font.family: Theme.fontFamilyHeading
    }
    /*
    Image {
        source: "/opt/sdk/harbour-qball/usr/share/harbour-qball/data/images/Testatlas.png"
        opacity: 0.5
    }
    */

    /*
AnimatedImage  {
    opacity: 0.5
    scale:1
    source: dataPath+"/images/waterSeamlessLoop.gif"
    anchors.horizontalCenter: parent.horizontalCenter
    y:960-380
   // width:180;
  //  height: 60;



}
*/
    ProximitySensor  {
        id: irproximity
        skipDuplicates:true
        property double reflectance: 0
        active: false
        onReadingChanged: {
            if (reading.near)
            {
                console.log("it is near")
                var forceapp
                if (inclination>=0)
                {
                    forceapp= inclination/90.0*10000+5000;

                }
                else
                {
                    forceapp= inclination/90.0*10000-5000;

                }

                controller.setAcc(forceapp,0);
            }
            else console.log("it is far")
        }

    }

    states: [
        State {
            name: "gaming"
            PropertyChanges {target: failed_completed; visible: false}
            PropertyChanges {target: pauseButton; visible: true}
            PropertyChanges {target: time; visible: true}
            PropertyChanges {target: accel; active: true}
            PropertyChanges {target: anglerect; visible: true}
            PropertyChanges {target: modeTilt; visible:false}

            PropertyChanges {target: freeBall; visible:true}
            PropertyChanges {target: page1; backNavigation: false}
            PropertyChanges {target: page1; showNavigationIndicator:false}

            PropertyChanges {target: time; color:Theme.highlightColor}
            PropertyChanges {target: time; opacity:0.5}
            PropertyChanges {target: pauseButton; opacity: 1}

        },
        State {
            name: "tilt"

            PropertyChanges {target: pauseButton; visible: false}
            PropertyChanges {target: time; visible: false}
            PropertyChanges {target: accel; active: false}
            PropertyChanges {target: modeTilt; visible:true}
            PropertyChanges {target: page1; backNavigation: true}
            PropertyChanges {target: page1; showNavigationIndicator:true}




        },
        State {
            name: "failed"

            PropertyChanges { target: failed_completed; visible: true }
            PropertyChanges { target: failed_completed; text:qsTr("LEVEL FAILED") }
            PropertyChanges {target: pauseButton; visible: false}
            PropertyChanges {target: time; visible: false}
            PropertyChanges {target: modeTilt; visible:false}
            PropertyChanges {target: anglerect; visible: false}
            PropertyChanges {target: freeBall; visible:false}


            PropertyChanges {target: page1; backNavigation: true}
            PropertyChanges {target: page1; showNavigationIndicator:true}
        },
        State {
            name: "paused"

            PropertyChanges { target: failed_completed; visible: false }
            PropertyChanges {target: pauseButton; visible: true}
            PropertyChanges {target: time; visible: true}
            PropertyChanges {target: modeTilt; visible:false}
            PropertyChanges {target: anglerect; visible: false}
            PropertyChanges {target: freeBall; visible:false}


            PropertyChanges {target: pauseButton; opacity: 0.6}


            PropertyChanges {target: page1; backNavigation: true}
            PropertyChanges {target: page1; showNavigationIndicator:true}
            PropertyChanges {target: time; color:Theme.highlightColor}

        },
        State {
            name: "levelok"

            PropertyChanges {target: time; visible: true}
            PropertyChanges { target: failed_completed; visible: true }
            PropertyChanges { target: failed_completed; text:qsTr("LEVEL COMPLETED") }
            PropertyChanges {target: pauseButton; visible: false}
            PropertyChanges {target: modeTilt; visible:false}
            PropertyChanges {target: anglerect; visible: false}
            PropertyChanges {target: freeBall; visible:false}
            PropertyChanges {target: page1; backNavigation: true}
            PropertyChanges {target: page1; showNavigationIndicator:true}



            PropertyChanges {target: time; color:Theme.highlightColor}
            PropertyChanges {target: time; opacity:1}
            // PropertyChanges {target: time; font.pixelSize:Theme.fontSizeLarge}



        }
    ]
    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min
    }

    function getColorFreeBall()
    {

       // console.log("controolo colore",controller.tilt)
      //  if (controller.tilt>1 )
        //    return "red"
       if (controller.canpush)

            return "green"
        else
            return "blue"
    }
}
