# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-qball

CONFIG += sailfishapp
#CONFIG += mobility
#PKGCONFIG += Qt0Feedback
#MOBILITY += qfeedbackeffect
QT +=  openglextensions multimedia



SOURCES += src/harbour-qball.cpp \
    renderitemraw.cpp \
    glrendersphere.cpp \
    b2dJson/b2dJson.cpp \
    b2dJson/b2dJsonImage.cpp \
    b2dJson/jsoncpp.cpp \
    glrenderlines.cpp \
    glrendertriangles.cpp \
    glrenderpoints.cpp \
    controller.cpp \
    mycontactlistener.cpp \
    glanimateball.cpp \
    glrenderwater.cpp \
    clipper.cpp \
    tessellation.cpp \
    src/libtess2/Source/bucketalloc.c \
    src/libtess2/Source/dict.c \
    src/libtess2/Source/geom.c \
    src/libtess2/Source/mesh.c \
    src/libtess2/Source/priorityq.c \
    src/libtess2/Source/sweep.c \
    src/libtess2/Source/tess.c \
    glrendertexturetriangles.cpp \
    qball.cpp

OTHER_FILES += qml/harbour-qball.qml \
    qml/cover/CoverPage.qml \
    rpm/harbour-qball.changes.in \
    rpm/harbour-qball.spec \
    rpm/harbour-qball.yaml \
    translations/*.ts \
    harbour-qball.desktop






data.path    = /usr/share/$$TARGET/data
data.files   += data/Schema_1.json
data.files   += data/Schema_2.json
data.files   += data/Schema_3.json
data.files   += data/Schema_4.json
data.files   += data/Pratice.json

INSTALLS     += data

#TEXTURE
images.path    = /usr/share/$$TARGET/data/images
images.files   += data/images/land_ocean_ice_350.jpg
images.files   += data/images/images.png
images.files   += data/images/pause.png
images.files   += data/images/eye.jpg
images.files   += data/images/arambula.jpg
images.files   += data/images/tajomaru.jpg
images.files   += data/images/fireball.png
images.files   += data/images/globe.png
images.files   += data/images/CausticsRender.png
images.files   += data/images/Iron_rust1.jpg
images.files   += data/images/iron.jpg
images.files   += data/images/cylinder.png
images.files   += data/images/aJGrA.jpg
images.files   += data/images/glass1.jpg
images.files   += data/images/wood.jpg
images.files   += data/images/jarmo.png
images.files   += data/images/biko.jpg
images.files   += data/images/test.jpg
images.files   += data/images/test.png
images.files   += data/images/0573.gif
images.files   += data/images/mihaela.png
images.files   += data/images/scheip63c.jpg
images.files   += data/images/bb.png
images.files   += data/images/paul.png
images.files   += data/images/controls.png
INSTALLS     += images
#ICON
icon.path    = /usr/share/$$TARGET/data/icon
icon.files   += data/icon/mokomaze.png
icon.files   += data/icon/jarmo.png
icon.files   += data/icon/alex.png
icon.files   += data/icon/miroslavova.png
icon.files   += data/icon/arambula.png
icon.files   += data/icon/tajomaru.png
icon.files   += data/icon/biko.png
icon.files   += data/icon/mihaela.png
icon.files   += data/icon/scheip63c.png
icon.files   += data/icon/bb.png
icon.files   += data/icon/0573.png
icon.files   += data/icon/paul.png
icon.files   += data/icon/globe.png
INSTALLS     += icon



sounds.path    = /usr/share/$$TARGET/data/sounds
sounds.files   += data/sounds/flip.wav
sounds.files   += data/sounds/tick.wav
sounds.files   += data/sounds/wall.wav
sounds.files   += data/sounds/bumper.wav
sounds.files   += data/sounds/door-open.wav
sounds.files   += data/sounds/water-splash.wav
sounds.files   += data/sounds/fuse.wav
sounds.files   += data/sounds/endlevel.wav
sounds.files   += data/sounds/iron-bar.wav
sounds.files   += data/sounds/glass-bottle.wav
sounds.files   += data/sounds/tilt.wav
sounds.files   += data/sounds/dead.wav
sounds.files   += data/sounds/impact_1.wav
sounds.files   += data/sounds/impact_2.wav
sounds.files   += data/sounds/impact_3.wav

INSTALLS     += sounds
#message($$TARGET)




# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.à
#TRANSLATIONS += translations/harbour-qball-de.ts

HEADERS += \
    renderitemraw.h \
    glrendersphere.h \
    glrendertriangles.h \
    glrenderlines.h \
    glrenderpoints.h \
    b2dJson/json/json-forwards.h \
    b2dJson/json/json.h \
    b2dJson/b2dJson.h \
    b2dJson/b2dJsonImage.h \
    b2dJson/bitmap.h \
    controller.h \
    typeglobal.h \
    mycontactlistener.h \
    glanimateball.h \
    glrenderwater.h \
    clipper.hpp \
    src/libtess2/Include/tesselator.h \
    src/libtess2/Source/bucketalloc.h \
    src/libtess2/Source/dict.h \
    src/libtess2/Source/geom.h \
    src/libtess2/Source/mesh.h \
    src/libtess2/Source/priorityq.h \
    src/libtess2/Source/sweep.h \
    src/libtess2/Source/tess.h \
    tesselation.h \
    glrendertexturetriangles.h \
    qball.h



lib.path    = /usr/share/$$TARGET/lib
CONFIG(ARMV,ARMV|i486):KIT = PLATFORM_ARMV7HL
CONFIG(i486,ARMV|i486):KIT = PLATFORM_i486

contains( KIT, PLATFORM_ARMV7HL ) {

lib.files   += libxarm/libBox2D.so
lib.files   += libxarm/libBox2D.so.2.3.2
LIBS += -L$$PWD/libxarm/ -lBox2D




message("PLATFORM_ARMV7HL")
}

contains( KIT, PLATFORM_i486 ) {

lib.files   += libx486/libBox2D.so
lib.files   += libx486/libBox2D.so.2.3.0
LIBS += -L$$PWD/libx486/ -lBox2D
message("PLATFORM_i486")
}



INSTALLS     += lib
INCLUDEPATH += $$PWD/../Compilazione/Box2D-master/Box2D/Box2D
DEPENDPATH += $$PWD/../Compilazione/Box2D-master/Box2D/Box2D

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

message ($$PWD)

DISTFILES += \
    qml/AboutPage.qml \
    qml/pages/AboutPage.qml \
    qml/pages/StartPage.qml \
    qml/pages/PlayPage.qml \
    qml/pages/ScrollPage.qml \
    qml/pages/ChoosePage.qml \
    data/icon/mokomaze.png \
    data/icon/jarmo.png \
    data/icon/arambula.png\
    data/icon/miroslavova.png \
    data/icon/tajomaru.png \
    data/icon/biko.png \
    data/icon/test.jpg \
    data/icon/mihaela.png \
    data/icon/scheip63c.png \
    data/icon/bb.png \
    data/icon/paul.png \
    data/icon/dinarnali.png \
    qml/pages/Controls.qml


