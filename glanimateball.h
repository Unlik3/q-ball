#ifndef GLANIMATEBALL_H
#define GLANIMATEBALL_H
#include <QObject>
#include <QtGui/QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <Box2D/Box2D.h>

#include <QOpenGLTexture>
#include <QtOpenGLExtensions>


class GLAnimateBall : public QObject
{
    Q_OBJECT
public:
    explicit GLAnimateBall(QObject *parent = 0);
     ~GLAnimateBall();
    void Create(const float32 radiusWorld,const int width,const int height,const QUrl dataUrl,float32 scale,float32 traslx);


    QOpenGLShaderProgram *m_program;
    QOpenGLTexture *m_texture;
    QOpenGLExtension_OES_vertex_array_object m_VAOEXT;
    GLuint VertexArrayID, vertexbuffer;
    QMatrix4x4                  m_projection_matrix;
    QMatrix4x4                  m_modelMatrix;
    QMatrix4x4                  m_rotationMatrix;

    enum Status{
       normal=0,
       splash=1,
       underwater=2,

    } m_status;
    bool fireOff;
    float m_rateWorldScreen;
    int m_heightScreen; // screen height
    short m_currentFrame;
    short m_TotFrame;
    void Flush(const float32 coordX,const float32 coordY,float32 offsetY);

signals:

public slots:
};

#endif // GLANIMATEBALL_H
