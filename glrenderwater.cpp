#include "glrenderwater.h"

GLRenderWater::GLRenderWater(b2Vec2 seg1p1,b2Vec2 seg1p2,b2Vec2 seg2p1,b2Vec2 seg2p2,QObject *parent) : QObject(parent)
{
    int i;
    m_count=0;
     m_program=nullptr;
    offset=0;
    waterColumn newPoint;
    NUM_POINTS = 40;
     qDebug() << "pseg";
 qDebug() << seg1p1.x<<seg1p1.y;
 qDebug() << seg1p2.x<<seg1p2.y;
  qDebug() << seg2p1.x<<seg2p1.y;
   qDebug() << seg2p2.x<<seg2p2.y;

    X_OFFSET = seg1p2.x;
    Y_OFFSET = seg1p1.y+(seg1p2.y-seg1p1.y)/2;
    WIDTH = seg2p2.x-seg1p2.x;
     qDebug() << "Calcolata: larghezza"<< WIDTH<< "Altezza"<<Y_OFFSET;
    Y_START=0;

m_firstFlush=true;

/*
=======
>>>>>>> dce72b9ef70134930cc7cb5589e430ceb4a125f5
    X_OFFSET = 270/17.55-64/17.55;
   Y_OFFSET = 20+128/17.55;
    WIDTH = 128/17.55;

<<<<<<< HEAD
    Y_START=25;
=======
    Y_START=20;
>>>>>>> dce72b9ef70134930cc7cb5589e430ceb4a125f5
  //  Y_OFFSET+=20;
*/
    SPRING_CONSTANT = 0.005;

    SPRING_CONSTANT_BASELINE = 0.005;



    DAMPING = 0.99;

    ITERATIONS = 2;
    BACKGROUND_WAVE_MAX_HEIGHT = 0.1f;
    BACKGROUND_WAVE_COMPRESSION = 1/4.;
  //  float32 i_x,i_y,i2_x,i2_y;





/*
      qDebug() << "seg1:" << seg1p1.x<< seg1p1.y<< seg1p2.x<<  seg1p2.y << "y: offset"<<Y_OFFSET;
    if (get_line_intersection(seg1p1.x, seg1p1.y, seg1p2.x,  seg1p2.y,
                              -15, Y_OFFSET, 15, Y_OFFSET ,
                              &i_x, &i_y))
        qDebug() << "Line Water Sx ..........OK";
    else
        qDebug() << "Line Water Sx ..........NOT!!";

    qDebug() << "seg2:" << seg2p1.x<< seg2p1.y<< seg2p2.x<<  seg2p2.y << "y: offset"<<Y_OFFSET;


    if(get_line_intersection(seg2p1.x, seg2p1.y, seg2p2.x,  seg2p2.y,
                             -15, Y_OFFSET, 15, Y_OFFSET ,
                             &i2_x, &i2_y))
        qDebug() << "Line Water Dx ..........OK";
    else
        qDebug() << "Line Water Dx ..........NOT!!";

    qDebug() << "intersezioni:" << i2_x << i2_y;

//X_OFFSET=i_x;

*/

    for ( i = 0; i <=NUM_POINTS; i++) {
        newPoint.x= i / (float32) NUM_POINTS * WIDTH;
        newPoint.y= Y_OFFSET;
        newPoint.spd=b2Vec2(0,0);
        newPoint.mass= 1;

        wavePoints.append(newPoint);
    }
    // for ( i = 0; i < NUM_POINTS; i++)
    // qDebug() <<  wavePoints[i].x <<wavePoints[i].y;
    //qsrand(5);


    for ( i = 0; i < NUM_BACKGROUND_WAVES; i++) {
        float32 sineOffset = -M_PI + 2 * M_PI * qrand()/(float32) RAND_MAX;
        sineOffsets[i]=sineOffset*0.001;
        float32 sineAmplitude = qrand()/(float32) RAND_MAX * BACKGROUND_WAVE_MAX_HEIGHT;
        sineAmplitudes[i]=sineAmplitude;
        float32 sineStretch = qrand()/(float32)RAND_MAX* 15*BACKGROUND_WAVE_COMPRESSION;
        sineStretches[i]=sineStretch;
        float32 offsetStretch = qrand()/(float32)RAND_MAX * BACKGROUND_WAVE_COMPRESSION*0.6;
        offsetStretches[i]=offsetStretch;
        //tableContent += "<tr><td>" + outputNum(sineOffset) + "</td><td>" + outputNum(sineAmplitude) +
        //                   "</td><td>" + outputNum(sineStretch) + "</td><td>" + outputNum(offsetStretch) + "</td></tr>";
    }
    //   Vertex(b2Vec2(16,25), b2Color(0,0,.1));
    //   Vertex(b2Vec2(18,25), b2Color(0,0,.1));
    //  Vertex(b2Vec2(17,30), b2Color(0,0,1));



}

void GLRenderWater::Create(int width,int height,float scale,const QUrl dataUrl,float32 traslx)
{
    QImage image;
qDebug() << "RenderWater";
m_firstTime=true;

image=QImage(1024,1024,QImage::Format_RGB32);
    // QUrl url;
    // url= SailfishApp::pathTo("data");
    if (image.load(dataUrl.path().append("/images/").append("CausticsRender.png")))
        qDebug() << "Image Water OK";

    qDebug() << "scale:"<< scale;


    //  QOpenGLBuffer m_vertex;
    m_program = new QOpenGLShaderProgram();
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex,
                                       "#version 300 es\n"
                                       "in vec2 vtxcoord;\n"
                                       "in vec4 v_color;\n"
                                       "in vec2 uvcoord;\n"
                                       "out vec2 texCoord;\n"
                                       "uniform mat4 projectionMatrix;\n"
                                       "uniform vec3 param;\n"
                                       "out vec4 f_color;\n"
                                       "void main() {\n"
                                       "  texCoord=uvcoord;\n"
                                       "	f_color = v_color;\n"
                                       "    gl_Position = projectionMatrix*vec4(param.x*vtxcoord.x,param.z+param.y-param.x*vtxcoord.y,0.0f, 1.0f);\n"

                                       "}\n"                                             );
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       "#version 300 es\n"
                                       "precision mediump float;\n"
                                       "out vec4 my_fragcolor;\n"
                                       "in vec4 f_color;\n"
                                       "in vec2 texCoord;\n"
                                       "uniform sampler2D tex;\n"
                                       "\n"
                                       "void main() {\n"
                                       "my_fragcolor=texture(tex, vec2(texCoord.x,texCoord.y));\n"
                                       "//my_fragcolor.a*=my_fragcolor.b;\n"
                                       "my_fragcolor*=f_color;\n"
                                       "my_fragcolor.a=f_color.a;\n"

                                       "}\n"
                                       );
    m_program->bindAttributeLocation("vtxcoord",0);
    m_program->bindAttributeLocation("v_color",1);
    m_program->bindAttributeLocation("uvcoord",2);

    m_program->link();


    // Create Buffer (Do not release until VAO is created)
    //   m_program->bind();
    //glBindAttribLocation(m_program->programId(), 0, "vtxcoord");
    // glBindAttribLocation(m_program->programId(), 1, "v_color");
    //   m_program->release();
    m_vertexBuffer.create();
    if(m_vertexBuffer.bind())
    {
        m_vertexBuffer.setUsagePattern(QOpenGLBuffer::DynamicDraw);
        m_vertexBuffer.allocate(m_vertices, sizeof(m_vertices));
        m_vertexBuffer.release();
    }
    m_colorBuffer.create();
    if(m_colorBuffer.bind())
    {
        m_colorBuffer.setUsagePattern(QOpenGLBuffer::DynamicDraw);
        m_colorBuffer.allocate(m_colors, sizeof(m_colors));
        m_colorBuffer.release();
        qDebug()<< "m_colorBuffer OK"<< "size:"<<sizeof(m_colors);
    }
    m_uvBuffer.create();
    if(m_uvBuffer.bind())
    {
        m_uvBuffer.setUsagePattern(QOpenGLBuffer::DynamicDraw);
        m_uvBuffer.allocate(m_uvmap, sizeof(m_uvmap));
        m_uvBuffer.release();
        qDebug()<< "m_uvBuffer OK"<< "size:"<<sizeof(m_uvmap);
    }

    m_texture  =  new QOpenGLTexture(image);

    m_texture->setMinificationFilter(QOpenGLTexture::Linear);
    m_texture->setMagnificationFilter(QOpenGLTexture::Linear);

    // set the wrap mode
  //  m_texture->setWrapMode(QOpenGLTexture::Repeat);

    m_texture->setWrapMode(QOpenGLTexture::DirectionS, QOpenGLTexture::Repeat);
    m_texture->setWrapMode(QOpenGLTexture::DirectionT, QOpenGLTexture::ClampToEdge);


    m_projection_matrix.setToIdentity();


    m_param.setX(scale);
    m_param.setY(height);
    m_param.setZ(0);

    m_projection_matrix.ortho(0, width, height, 0.0f, -1.0f, 1.0f);
    m_projection_matrix.translate(traslx/scale+width/2.0f,0,0);








    qDebug() << "Program1 link result:" << m_program->log();
}

void GLRenderWater::Vertex(const b2Vec2& v, const b2Color& c)
{

    if (m_count == e_maxVertices)
    {
        qDebug() << "##########GLRenderWater::Vertex###########";
      return;

    }

    m_vertices[m_count] =v;



    m_colors[m_count] = c;
    ++m_count;
}
void GLRenderWater::Flush(float32 offsetY)
{

    int n=0;
    offset ++;
//    glEnable(GL_BLEND);
//    glBlendFunc(GL_ONE, GL_ONE);
 //   glBlendEquation(GL_FUNC_ADD);
 float stepX,stepY;
 static short currentFrame=0;


short calcFrame=currentFrame/5;

 const short ncolonne=4;
stepX=calcFrame%ncolonne;
stepY=calcFrame/ncolonne;
float step,scaX=0.25,scaY=0.25*0.2,offset=0.25 ;
step=1.0*wavePoints.length();
step=1./step;
int indVertex=0;

//qDebug()<< "Framae:"<< calcFrame << stepX << stepY;
    // Update positions of points
    updateWavePoints();
     b2Color coldeep = b2Color(127/255.,255/255.,212/255.,0.0);
       // b2Color coldeep = b2Color(0.7,0.7,0.7,1.0);
    //   b2Color colsup  = b2Color(127/255.,255/255.,212/255.,1.0);

  //  b2Color coldeep = b2Color(.5,0.5,0.5,0.0);
//    b2Color colsup  = b2Color(127/255.,255/255.,212/255.,0.0);

   // b2Color colsup  = b2Color(0.,0.,0.5,1.0);

    // for ( n = 0; n < wavePoints.length()-1; n++)
    do
    {

        //  Vertex(b2Vec2(wavePoints[n].x+X_OFFSET,wavePoints[n].y+overlapSines(wavePoints[n].x)), colsup);


        Vertex(b2Vec2(wavePoints[n].x+X_OFFSET,wavePoints[n].y+overlapSines(wavePoints[n].x)), coldeep);
        Vertex(b2Vec2(wavePoints[n].x+X_OFFSET,Y_START), coldeep);
        Vertex(b2Vec2(wavePoints[n+1].x+X_OFFSET,wavePoints[n+1].y+overlapSines(wavePoints[n+1].x)), coldeep);
        Vertex(b2Vec2(wavePoints[n+1].x+X_OFFSET,Y_START), coldeep);

/*
        Vertex(b2Vec2(wavePoints[n].x+X_OFFSET,30), colsup);
        Vertex(b2Vec2(wavePoints[n].x+X_OFFSET,Y_START), colsup);
        Vertex(b2Vec2(wavePoints[n+1].x+X_OFFSET,30), colsup);
        Vertex(b2Vec2(wavePoints[n+1].x+X_OFFSET,Y_START), colsup);
*/



        m_uvmap[indVertex].Set(scaX*n*step+offset*stepX,offset*stepY);
   //     qDebug() << "Vertex:"<< indVertex << "("<<sca*n*step<<","<<0<<")";
        indVertex++;
        m_uvmap[indVertex].Set(scaX*n*step+offset*stepX,scaY+offset*stepY);
     //    qDebug() << "Vertex:"<< indVertex << "("<<sca*n*step<<","<<sca<<")";
        indVertex++;





            m_uvmap[indVertex].Set(scaX*(n+1)*step+offset*stepX,offset*stepY);
       //       qDebug() << "Vertex:"<< indVertex << "("<<(n+1)*step<<","<<0<<")";
            indVertex++;
            m_uvmap[indVertex].Set(scaX*(n+1)*step+offset*stepX,scaY+offset*stepY);
         //   qDebug() << "Vertex:"<< indVertex << "("<<(n+1)*step<<","<<sca<<")";
            indVertex++;

        n++;


        //  Vertex(b2Vec2(wavePoints[n].x+1,wavePoints[n].y+overlapSines(wavePoints[n].x)), b2Color(0,0,1));
        //  Vertex(b2Vec2(wavePoints[n].x,1+wavePoints[n].y+overlapSines(wavePoints[n].x)), b2Color(0,0,1));
        //if (offset==1)

        //    qDebug() << "over:"<<overlapSines(wavePoints[n].x);
    } while (n<wavePoints.length()-1);






//qDebug() << "tot vertex:"<< m_count;

    if (m_count == 0)
        return;
    if (m_firstFlush)
    {
        qDebug() << "GLRenderWater::Tot vertex:"<<m_count;
        m_firstFlush=false;
    }

    if ( m_program->bind())
    {


        if (m_vertexBuffer.bind())
        {

            m_program->enableAttributeArray("vtxcoord");
            m_program->setAttributeBuffer(0, GL_FLOAT, 0,2);
            m_vertexBuffer.write(0,m_vertices, m_count * sizeof(b2Vec2));
            m_vertexBuffer.release();
        }


        if(m_colorBuffer.bind() ) // il colore non cambia
        {

            m_program->enableAttributeArray("v_color");
            m_program->setAttributeBuffer(1, GL_FLOAT, 0,4);
        if (m_firstTime)
            {
                m_firstTime=false;
                m_colorBuffer.write(0,m_colors, m_count * sizeof(b2Color));
            }


            m_colorBuffer.release();

        }

        if (m_uvBuffer.bind())
        {

            m_program->enableAttributeArray("uvcoord");
            m_program->setAttributeBuffer(2, GL_FLOAT, 0,2);
            m_uvBuffer.write(0,m_uvmap, m_count * sizeof(b2Vec2));
            m_uvBuffer.release();
        }



        m_program->setUniformValue("projectionMatrix", m_projection_matrix);
        m_param.setZ(offsetY);
        m_program->setUniformValue("param", m_param);

        m_texture->bind();

        glDrawArrays(GL_TRIANGLE_STRIP, 0, m_count);

        m_texture->release();


        m_program->release();
     currentFrame = (currentFrame+1)%(16*5);
    }
    m_count = 0;
  //  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

float32 GLRenderWater::overlapSines(float32 x)
{
    float32 result=0;
    int i;
    //return 0;
    for ( i = 0;i < NUM_BACKGROUND_WAVES; i++) {
        result = result
                + sineOffsets[i]
                + sineAmplitudes[i]
                *sin(x * sineStretches[i] + offset * offsetStretches[i]);


    }

    return result;

}

// Update the positions of each wave point
void GLRenderWater::updateWavePoints() {
    int i,n;
    waterColumn p;
    float32 dy,forceToBaseline;

    //return;

    for (i = 0; i < ITERATIONS; i++) {

        for ( n = 0; n < wavePoints.length(); n++) {
            p = wavePoints[n];




            // force to apply to this point
            float32 force = 0;

            // forces caused by the point immediately to the left or the right
            float32 forceFromLeft, forceFromRight;

            if (n == 0) { // wrap to left-to-right
                dy = wavePoints[wavePoints.length() - 1].y - p.y;
                forceFromLeft = SPRING_CONSTANT * dy;
            } else { // normally
                dy = wavePoints[n - 1].y - p.y;
                forceFromLeft = SPRING_CONSTANT * dy;
            }
            if (n == wavePoints.length() - 1) { // wrap to right-to-left
                dy = wavePoints[0].y - p.y;
                forceFromRight = SPRING_CONSTANT * dy;
            } else { // normally
                dy = wavePoints[n + 1].y - p.y;
                forceFromRight = SPRING_CONSTANT * dy;
            }

            // Also apply force toward the baseline
            dy = Y_OFFSET - p.y;
            forceToBaseline = SPRING_CONSTANT_BASELINE * dy;



            // Sum up forces
            force = force + forceFromLeft;
            force = force + forceFromRight;
            force = force + forceToBaseline;




            // Calculate acceleration
            float32 acceleration = force / p.mass;

            // qDebug() << "acceleration: "<<acceleration;

            // Apply acceleration (with damping)
            p.spd.y = DAMPING * p.spd.y + acceleration;

            // Apply speed
            p.y = p.y + p.spd.y;
            wavePoints[n]=p;


        }
    }

}
GLRenderWater::~GLRenderWater()
{

    if (m_program!=nullptr)
    {
    m_vertexBuffer.destroy();
    m_colorBuffer.destroy();
    m_uvBuffer.destroy();
  // delete m_texture;

    delete m_program;
    }
   qDebug() << "RenderWater destroy";
}

void GLRenderWater::Splash(float32 xPosition, float32 velocity)
{
    int length=wavePoints.length();
    xPosition-= X_OFFSET;
    qDebug() << xPosition<< "Splashhhh"<<wavePoints[0].x<<wavePoints[length-1].x;

    if (length<1)
    {
        qDebug() << "Splash  problem!!!!";
        return;
    }
    //If the position is within the bounds of the water:
    if (xPosition >= wavePoints[0].x && xPosition <= wavePoints[length-1].x)
    {
        //Offset the x position to be the distance from the left side
        xPosition -= wavePoints[0].x;

        //Find which spring we're touching
        int index = qRound((length-1)*(xPosition / (wavePoints[length-1].x - wavePoints[0].x)));

        qDebug() << "Index"<< index<< wavePoints.count();
        if ((index+3)<wavePoints.count()  && (index-3)>=0)
        {
        //Add the velocity of the falling object to the spring
        wavePoints[index].spd.y  = wavePoints[index].spd.y + velocity;
        wavePoints[index+1].spd.y = wavePoints[index+1].spd.y+ velocity/2.;
        wavePoints[index+2].spd.y = wavePoints[index+2].spd.y+ velocity/3.;
        wavePoints[index+3].spd.y = wavePoints[index+3].spd.y+ velocity/4.;
        wavePoints[index-1].spd.y  =wavePoints[index-1].spd.y+ velocity/2;
        wavePoints[index-2].spd.y  =wavePoints[index-2].spd.y+ velocity/3.;
        wavePoints[index-3].spd.y  =wavePoints[index-3].spd.y+ velocity/4.;
        }
        //velocities[index] += velocity;
    }
    qDebug() << "splashh out";
}
// Returns 1 if the lines intersect, otherwise 0. In addition, if the lines
// intersect the intersection point may be stored in the floats i_x and i_y.
char GLRenderWater::get_line_intersection(float32 p0_x, float32 p0_y, float32 p1_x, float32 p1_y,
                                          float32 p2_x, float32 p2_y, float32 p3_x, float32 p3_y, float32 *i_x, float32 *i_y)
{
    float32 s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    float32 s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        // Collision detected
        if (i_x != NULL)
            *i_x = p0_x + (t * s1_x);
        if (i_y != NULL)
            *i_y = p0_y + (t * s1_y);
        return 1;
    }

    return 0; // No collision
}
float32 GLRenderWater::getLevelWater(float32 xPosition)
{
    int length=wavePoints.length();
    xPosition-= X_OFFSET;
    if (xPosition >= wavePoints[0].x && xPosition <= wavePoints[length-1].x)
    {

        //Offset the x position to be the distance from the left side
        xPosition -= wavePoints[0].x;

        //Find which spring we're touching
        int index = qRound((length-1)*(xPosition / (wavePoints[length-1].x - wavePoints[0].x)));


        return wavePoints[index].y+overlapSines(wavePoints[index].x);
    }
    else return 0;
}
