#ifndef RENDERITEMRAW_H
#define RENDERITEMRAW_H

#include <QQuickItem>
#include <QColor>
#include <Box2D/Box2D.h>

#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QOpenGLContext>
#include <QtOpenGLExtensions>
#include "renderitemraw.h"
#include <QTime>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLTexture>
#include <QtQuick/qquickwindow.h>
#include "b2dJson/b2dJson.h"
#include "glrendertriangles.h"
#include "glrenderlines.h"
#include "glrenderpoints.h"
#include "glrendersphere.h"
#include "glrenderwater.h"
#include "typeglobal.h"
#include "mycontactlistener.h"
#include "glanimateball.h"
#include "glrendertexturetriangles.h"
#include "controller.h"
#include "src/libtess2/Include/tesselator.h"

class MyContactListener;
class Controller;
class ViewGL : public QObject
{
    Q_OBJECT

public:



    ViewGL(QQuickWindow *window,Controller *controller);
    ~ViewGL();
    void timerEvent(QTimerEvent *event);

    void printVersionInformation();

    void DrawData(float32* userMyData1,float32* userMyData2,float32* userMyData3);


    void DrawTransform(const b2Transform& xf);
    void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
    void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
    void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);


    b2Body * addSphere(float32 x,float32 y,float32 r,float32 density);
    void drawSphere(b2Vec2 center,float32 angle,float32 radius,Resource* userData);

    void invertGravity();
    void pressRight();
    void pressLeft();
    void releaseRight();
    void releaseLeft();
    //void createProgramMoveSphere1();
    // void flushSphere1(float32 *userData);
    b2Body *addRect(float32 x, float32 y, float32 w, float32 h, bool dyn);
    void pause();
    void convertPixelToWorld(b2Vec2 &ppix);
    QImage getImage1();
    QImage getImageName(const QString nameImage);
    QImage getImage3();

    QBasicTimer mTimer;
    QQuickWindow* grabScreen();

    void setLightOrientation(float horiz, float vert);
    bool Initialize(QString nameScheme);
    void cleanUp();
    void setSphereRadius(float radius);
    void playSpund(short tsound);
    void playSound(short tsound);
    QString pathCache() const;
    void setPathCache(const QString &pathCache);


    //bool event(QEvent *ev);
    void openPinfold();
    void closePinfold();

    void setNeedModifyOn(short ind);
    void addFixModModify(fixtureToModify &fixtMod);


    void setAccSpheres(const b2Vec2 acc);
    b2Vec2 convertWorldToScreen(const b2Vec2 preal);
    void triangulate(b2ChainShape* chain,const b2Transform* xf,short indexTexture);
    bool isNeedModifyOn();

    QVector<QColor> rndColors(int count);
    bool isDrawing() const;


    void addBodyBreak(b2Body *body);
    bool getIsDestroying() const;

    void Tilt();
    bool getOpenGLVisible() const;
    void setOpenGLVisible(bool value);

    void setOffsetY(int offsetY);
    void start(const QString nameScheme);
    void stop();

    void createTeam(int indball1, int indball2, int indball3);
    void checkTeam();

    int countBall();
    int numRand(int low, int high);
    b2World *getWorld() const;

public slots:
    void renderGL();

    void onApplicationStateChanged(Qt::ApplicationState state);

    //  void deleteContext();
protected:

    void getWorldAABB(b2AABB &aabbWorld);

private slots:


private:
    QQuickWindow *m_window;
    //  QOpenGLContext* m_context;


    float angle;
    QString jsonpathFile;

    b2World* m_world;


    //
    GLRenderPoints* m_points;
    GLRenderLines* m_lines;
    GLRenderTriangles* m_triangles;
    GLRenderTextureTriangles* m_textureTriangles[3];
    GLRenderTextureTriangles* m_textureStaticTriangles;
    GLRenderTextureTriangles* m_textureDynamicTriangles[2];
    GLRenderSphere* m_sphere1;
    GLRenderSphere* m_sphere2;
    GLRenderSphere* m_sphere3;
    GLAnimateBall* m_fireball1;
    GLRenderWater* m_water;


    MyContactListener *myContactListenerInstance;

    QVector<fixtureToModify> lisBodyToModify;// lista body to modify
    b2Body* bodyToBreak;
    bool openGLVisible;

    //   QMediaPlayer  *player;
    //  QMediaPlaylist  *playList;

    QSoundEffect sound1;
    QSoundEffect sound2;
    QSoundEffect sound3;
    QSoundEffect sound4;
    QSoundEffect sound5;
    QSoundEffect sound6;
    QSoundEffect sound7;
    QSoundEffect sound8;
    QSoundEffect sound9;
    QSoundEffect sound10;

    b2Body* bodySphere1;
    b2Body* bodySphere2;
    b2Body* bodySphere3;

    QElapsedTimer timerdead;

    GLfloat m_sphere1Alfa;
    GLfloat m_sphere2Alfa;
    GLfloat m_sphere3Alfa;


    b2Body* fireBall1;
    float m_timeliveFireBall;

    b2RevoluteJoint *leftFlipper;
    b2RevoluteJoint *rightFlipper;
    b2RevoluteJoint *m_pinFold;


    float m_horizonthal_light;
    float m_vertical_light;

    float m_rateWorldScreen;
    float32 m_ratex,m_ratey;


    QString m_pathCache;
    QUrl dataUrl;
    bool m_isStop;
    bool m_wasStop;
    bool m_needModScene[3];

    void scheduledForModify(short ind);
    enum { e_maxVertices = 3 * 512 };
    b2Vec2 m_verticesTexture[e_maxVertices][3];
    int m_nVertTexture[3];

    bool needTriangulate[3];

    QVector<QColor> m_random_colors;

    short isIronRust(b2Body *b);
    short bodyType(b2Body* b);
    b2AABB m_world_dimension;
    //  void convertWorldToUVMap(b2Vec2 &preal);
    bool m_isDrawing;
    bool m_isDestroying;


    QVector4D lightPos;

    void DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color);
    void DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color);
    void DrawStaticSolidPolygon(const b2Vec2 *vertices, int32 vertexCount);
    void DrawShape(b2Fixture *fixture, const b2Transform &xf, const b2Color &color, bool isstatic, bool isfire);


    void getBodyAABB(b2AABB &aabb, b2Body *b, bool transformed);

    int m_countPinLeft,m_countPinRight;

    void breakBody(b2Body *b);
    bool m_isPinLeftDestroyed,m_isPinRightDestroyed;
    bool m_breakBody;
    bool isStatusTilt;
    int m_offsetY;

    Controller *m_controller;

    QElapsedTimer timer;


    void addCage(float32 xPos, float32 yPos);
    void initGL();
    void addChooseBox(float32 xPos, float32 yPos);
    QList<QObject*> m_dataList;

    bool m_needRecreate1;

    bool m_praticing;
    bool noWater;
    QSoundEffect *m_sound[10];
};



#endif // RENDERITEMRAW_H
