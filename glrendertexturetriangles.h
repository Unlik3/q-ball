#ifndef GLRENDERTEXTURETRIANGLES_H
#define GLRENDERTEXTURETRIANGLES_H

#include <QObject>
#include <QtGui/QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLTexture>
#include <QUrl>
#include <Box2D/Box2D.h>
class GLRenderTextureTriangles : public QObject
{
    Q_OBJECT

public:

    explicit GLRenderTextureTriangles(QObject *parent = 0);
    ~GLRenderTextureTriangles();
     void Create(int width, int height, float32 scale, float32 traslx, const QString pathImage,const float32 transp);
    void Vertex(const b2Vec2& v, const b2Vec2& uv);
    void Flush(float32 offestY);
    inline int getNumVertex(){return m_count;}
    inline QOpenGLShaderProgram * getShaderProgram() {return m_program;}

    void destroyTexture(QOpenGLContext *context);
private:
    int32 m_count;

    enum { e_maxVertices =  512};
    b2Vec2 m_vertices[e_maxVertices];
    b2Vec2 m_uv[e_maxVertices];

    QOpenGLShaderProgram *m_program;
    QMatrix4x4                  m_projection_matrix;
    QVector4D                   m_param;
    QOpenGLBuffer m_vertexBuffer;
    QOpenGLBuffer m_uvBuffer;
    QOpenGLTexture *m_texture;
    bool m_firstFlush;



signals:

public slots:
};

#endif // GLRENDERTEXTURETRIANGLES_H
