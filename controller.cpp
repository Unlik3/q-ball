#include "controller.h"
#include "qball.h"
#include "renderitemraw.h"

#include <QQuickView>

Controller::Controller(QObject *parent) : QObject(parent)
{
    m_status="gaming";
m_play1=0;
m_play2=1;
m_play3=2;
m_canpush=true;
}

void Controller::pressRight()
{
   m_viewGL->pressRight();
}
void Controller::pressLeft()
{
    m_viewGL->pressLeft();
}

void Controller::releaseRight()
{

    m_viewGL->releaseRight();
}
void Controller::releaseLeft()
{
    m_viewGL->releaseLeft();
}


void Controller::grabScreen()
{

 //  stop();

  m_viewGL->grabScreen();




}
void Controller::restart()
{
  m_viewGL->cleanUp();

}

void Controller::openPinfoldCtrl()
{
m_viewGL->openPinfold();
}
void Controller::closePinfoldCtrl()
{

m_viewGL->closePinfold();
}
void Controller::pause()
{

m_viewGL->pause();
}

void Controller::setAcc(const float accx,const float accy)
{

    qDebug() << accx<<accy<<"###################";
   m_viewGL->setAccSpheres(b2Vec2(accx,accy));


}
void Controller::statusTilt()
{
   m_status="tilt";
   m_viewGL->Tilt();
}

void Controller::setVisibleOpenGL(bool vis)
{
   m_viewGL->setOpenGLVisible(vis);
}

void Controller::setOffsetY(int offsetY)
{
   m_viewGL->setOffsetY(offsetY);
}
void Controller::preTilt()
{
    qDebug()<< "preTilt:"<<m_tilt;
     QTimer::singleShot(2000, this, SLOT(updateCaption()));

}
void Controller::updateCaption()
{
    qDebug()<< "#########tilt off:"<<m_tilt;
  setTilt(0);


}
void Controller::startGame(const QString nameScheme)
{

 m_status="gaming";
    m_viewGL->start(nameScheme);
    QObject::connect(qApp, SIGNAL(applicationStateChanged(Qt::ApplicationState)),m_viewGL, SLOT(onApplicationStateChanged(Qt::ApplicationState)));

}
void Controller::stopGame()
{

     m_viewGL->stop();


}


void Controller::setPaintGL(ViewGL *viewGL)
{
    m_viewGL=viewGL;
}

void Controller::createTeamCtrl(int indball1,int indball2,int indball3)
{
   m_viewGL->createTeam(indball1, indball2, indball3);

}
void Controller::checkTeamCTRL( )
{
    m_viewGL->checkTeam();
}

int Controller::countBallCtrl()
{
  return  m_viewGL->countBall();
}
void Controller::impossiblePush()
{
    qDebug() << "impossiblePush";
  setCanpush(false);
    QTimer::singleShot(5000, this, SLOT(canPushing()));
}
void Controller::canPushing()
{
    qDebug() << "canPushing";
    setCanpush(true);

}
void Controller::randomBreak()
{
    if ( m_viewGL->numRand(1,50)==3) // break gravity
     {
         m_viewGL->getWorld()->SetGravity(b2Vec2(m_viewGL->numRand(-10,10),m_viewGL->numRand(-10,10)));
       //  qDebug() <<"Gravity New" <<m_world->GetGravity().x<<m_world->GetGravity().y;

     }
}
