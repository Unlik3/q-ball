#ifndef GLRENDERLINES_H
#define GLRENDERLINES_H

#include <QObject>
#include <QtGui/QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <Box2D/Box2D.h>
//#include "box2dworld.h"

class GLRenderLines : public QObject
{
    Q_OBJECT
public:
    explicit GLRenderLines(QObject *parent = 0);
    ~GLRenderLines();
    void Create(int width,int height,float32 scale,float32 traslx);
       void Vertex(const b2Vec2 &v,const  b2Color &c);
       void Flush(float32 offsetY);
       inline int getNumVertex(){return m_count;}
private:
    int32 m_count;
    enum { e_maxVertices =   512 };
    b2Vec2 m_vertices[e_maxVertices];
    b2Color m_colors[e_maxVertices];
    QOpenGLShaderProgram *m_program;
     bool m_firstFlush;
 QOpenGLBuffer m_vertexBuffer;
  QOpenGLBuffer m_colorBuffer;
 QMatrix4x4                  m_projection_matrix;
 QVector3D                   m_param;


signals:

public slots:

};

#endif // GLRENDERLINES_H
