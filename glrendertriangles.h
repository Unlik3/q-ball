#ifndef GLRENDERTRIANGLES_H
#define GLRENDERTRIANGLES_H

#include <QObject>
#include <QtGui/QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <Box2D/Box2D.h>
//#include "box2dworld.h"

class GLRenderTriangles : public QObject
{
    Q_OBJECT
public:
    explicit GLRenderTriangles(QObject *parent = 0);
     ~GLRenderTriangles();
    void Create(int width,int height,float32 scale,float32 traslx);
    void Vertex(const b2Vec2 &v,const  b2Color &c);
    void Flush(float32 offsetY);
    inline int getNumVertex(){return m_count;}
    inline QOpenGLShaderProgram * getShaderProgram() {return m_program;}

private:
    int32 m_count;
 bool m_firstFlush;
    enum { e_maxVertices = 1024 };
    b2Vec2 m_vertices[e_maxVertices];
    b2Color m_colors[e_maxVertices];
    QOpenGLShaderProgram *m_program;
    QMatrix4x4                  m_projection_matrix;
    QVector3D                   m_param;
    QOpenGLBuffer m_vertexBuffer;
    QOpenGLBuffer m_colorBuffer;
  //  int m_width;
  //  int m_height;
  //  float m_scale;




signals:

public slots:

};

#endif // GLRENDERTRIANGLES_H
